# language: ru

Функционал: Тестируем цех переводчиков

  Сценарий: Авторизация и добавление слов
    Допустим я прохожу авторизации
    И я добавляю слово "Яблоко" для перевода
    И я добавляю слово "Стол" для перевода
    И я добавляю слово "Батут" для перевода
    И я добавляю слово "Корона" для перевода
    И я добавляю слово "Собака" для перевода
    И я добавляю слово "Дом" для перевода
    И я добавляю слово "Стул" для перевода
    И я добавляю слово "Город" для перевода
    И я добавляю слово "Солнце" для перевода
    И я добавляю слово "Мама" для перевода

  Сценарий: Добавление переводов для слов
    Допустим я прохожу авторизации
    И я перехожу на страницу переводов слова "Яблоко"
    И я добавляю переводы слова
      | form_enWord | apple    |
      | form_frWord | la pomme |
      | form_spWord | manzana  |
      | form_grWord | μήλο     |
      | form_itWord | mela     |
    И я перехожу главную на страницу
    И я перехожу на страницу переводов слова "Стол"
    И я добавляю переводы слова
      | form_enWord | Table    |
      | form_frWord | Table    |
      | form_spWord | Tabla    |
      | form_grWord | Τραπέζι  |
      | form_itWord | Tavolo   |
    И я перехожу главную на страницу
    И я перехожу на страницу переводов слова "Батут"
    И я добавляю переводы слова
      | form_enWord | Trampoline  |
      | form_frWord | Trampoline  |
      | form_spWord | Trampoline  |
      | form_grWord | Τραμπολίνο  |
      | form_itWord | Trampolino  |
    И я перехожу главную на страницу
    И я перехожу на страницу переводов слова "Корона"
    И я добавляю переводы слова
      | form_enWord | Crown  |
      | form_frWord | Crown  |
      | form_spWord | Crown  |
      | form_grWord | Crown  |
      | form_itWord | Crown  |
    И я перехожу главную на страницу
    И я перехожу на страницу переводов слова "Собака"
    И я добавляю переводы слова
      | form_enWord | Dog     |
      | form_frWord | Dog     |
      | form_spWord | Dog     |
      | form_grWord | Σκύλος  |
      | form_itWord | Dog     |
    И я перехожу главную на страницу
    И я перехожу на страницу переводов слова "Дом"
    И я добавляю переводы слова
      | form_enWord | House   |
      | form_frWord | House   |
      | form_spWord | House   |
      | form_grWord | Καρέκλα |
      | form_itWord | House   |
    И я перехожу главную на страницу
    И я перехожу на страницу переводов слова "Стул"
    И я добавляю переводы слова
      | form_enWord |  Chair |
      | form_frWord | Chaise |
      | form_spWord | Chair  |
      | form_grWord | Σπίτι  |
      | form_itWord | Sedia  |
    И я перехожу главную на страницу
    И я перехожу на страницу переводов слова "Город"
    И я добавляю переводы слова
      | form_enWord | City  |
      | form_frWord | Ville |
      | form_spWord | City  |
      | form_grWord | Πόλη  |
      | form_itWord | City  |
    И я перехожу главную на страницу
    И я перехожу на страницу переводов слова "Солнце"
    И я добавляю переводы слова
      | form_enWord | Sun  |
      | form_frWord | Sun  |
      | form_spWord | Sun  |
      | form_grWord | Sun  |
      | form_itWord | Sun  |
    И я перехожу главную на страницу
    И я перехожу на страницу переводов слова "Мама"
    И я добавляю переводы слова
      | form_enWord | Mama  |
      | form_frWord | Mama  |
      | form_spWord | Mama  |
      | form_grWord | Mama  |
      | form_itWord | Mama  |



