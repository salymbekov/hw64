<?php

namespace AppBundle\Features\Context;

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;


/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements KernelAwareContext
{
    /** @var  KernelInterface */
    private $kernel;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @When /^я прохожу авторизации$/
     */
    public function яПрохожуАвторизацию()
    {
        $this->visit('/login');
        $this->fillField('_username', 'admin');
        $this->fillField('_password', '123');
        $this->pressButton('_submit');
    }



    /**
     * @When /^я добавляю слово "([^"]*)" для перевода$/
     * @param $word
     */
    public function яДобавляюСловоДляПеревода($word)
    {
        $this->fillField('form[ruWord]', $word);
        $this->pressButton('form[save]');

    }

    /**
     * @When /^я перехожу на страницу переводов слова "([^"]*)"$/
     * @param $link
     */
    public function яПерехожуНаСтраницуДляДобавленияПеревода($link)
    {
        $this->clickLink($link);

    }

    /**
     * @When /^я перехожу главную на страницу$/
     * @param $link
     */
    public function яПерехожуНаГлавнуюСтраницу()
    {
       $this->visit('/');
    }


    /**
     * Sets Kernel instance.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @When /^я добавляю переводы слова$/
     * @param TableNode $table
     */
    public function яДобавляюПереводыСлова(TableNode $table)
    {
        $this->fillFields($table);
        $this->pressButton('form[save]');

    }

}
