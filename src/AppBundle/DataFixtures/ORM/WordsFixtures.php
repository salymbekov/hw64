<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Words;
use AppBundle\Entity\WordsTranslation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class WordsFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $wordTranslate = new WordsTranslation();
        $word = new Words();
        $wordTranslate->setWord('дом');
           $wordTranslate->setLocale('ru');
           $wordTranslate->setTranslatable($word);
        $manager->persist($wordTranslate);
        $wordTranslate2 = new WordsTranslation();
        $wordTranslate2->setWord('house');
        $wordTranslate2->setLocale('en');
        $wordTranslate2->setTranslatable($word);
        $manager->persist($wordTranslate2);
        $manager->flush();
    }
}
