<?php

/* WebProfilerBundle:Profiler:header.html.twig */
class __TwigTemplate_bc1e52c68a84ad4912a639930a3de4707ec155658dde0ab19724bf3fd668bca5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cdddb2a6b32eef52f30db629e7677b0597d2aa2a7b034828f6469b52ca230d86 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cdddb2a6b32eef52f30db629e7677b0597d2aa2a7b034828f6469b52ca230d86->enter($__internal_cdddb2a6b32eef52f30db629e7677b0597d2aa2a7b034828f6469b52ca230d86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:header.html.twig"));

        $__internal_21d65b4a69594b3fa57633b23e01d510727aad95b2a18d22ee4a3bf451251c34 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_21d65b4a69594b3fa57633b23e01d510727aad95b2a18d22ee4a3bf451251c34->enter($__internal_21d65b4a69594b3fa57633b23e01d510727aad95b2a18d22ee4a3bf451251c34_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:header.html.twig"));

        // line 1
        echo "<div id=\"header\">
    <div class=\"container\">
        <h1>";
        // line 3
        echo twig_include($this->env, $context, "@WebProfiler/Icon/symfony.svg");
        echo " Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
";
        
        $__internal_cdddb2a6b32eef52f30db629e7677b0597d2aa2a7b034828f6469b52ca230d86->leave($__internal_cdddb2a6b32eef52f30db629e7677b0597d2aa2a7b034828f6469b52ca230d86_prof);

        
        $__internal_21d65b4a69594b3fa57633b23e01d510727aad95b2a18d22ee4a3bf451251c34->leave($__internal_21d65b4a69594b3fa57633b23e01d510727aad95b2a18d22ee4a3bf451251c34_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 3,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"header\">
    <div class=\"container\">
        <h1>{{ include('@WebProfiler/Icon/symfony.svg') }} Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
", "WebProfilerBundle:Profiler:header.html.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/header.html.twig");
    }
}
