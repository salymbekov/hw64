<?php

/* WebProfilerBundle:Profiler:open.html.twig */
class __TwigTemplate_a683b575689043bc09129dae443e138560c941e2b0ad39b78961401347f2d048 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "WebProfilerBundle:Profiler:open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2f8f69a5e1ae5adbb922290288b8f5ddf010182d4511f43bc34771146c7a4e46 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2f8f69a5e1ae5adbb922290288b8f5ddf010182d4511f43bc34771146c7a4e46->enter($__internal_2f8f69a5e1ae5adbb922290288b8f5ddf010182d4511f43bc34771146c7a4e46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $__internal_1464959b30928dc39f9790649d6a014acc41503b127f1f865df3d46c1e8b09ce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1464959b30928dc39f9790649d6a014acc41503b127f1f865df3d46c1e8b09ce->enter($__internal_1464959b30928dc39f9790649d6a014acc41503b127f1f865df3d46c1e8b09ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2f8f69a5e1ae5adbb922290288b8f5ddf010182d4511f43bc34771146c7a4e46->leave($__internal_2f8f69a5e1ae5adbb922290288b8f5ddf010182d4511f43bc34771146c7a4e46_prof);

        
        $__internal_1464959b30928dc39f9790649d6a014acc41503b127f1f865df3d46c1e8b09ce->leave($__internal_1464959b30928dc39f9790649d6a014acc41503b127f1f865df3d46c1e8b09ce_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_6d727abf62b9f267986f2067b070aaacd4e6c10ecabc6e4e54d00acd588d2af3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6d727abf62b9f267986f2067b070aaacd4e6c10ecabc6e4e54d00acd588d2af3->enter($__internal_6d727abf62b9f267986f2067b070aaacd4e6c10ecabc6e4e54d00acd588d2af3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_50e4d2338a5f9ab3c4b9aebf4735e3cf273ae55c71aea50dcf2c94b55b68b502 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_50e4d2338a5f9ab3c4b9aebf4735e3cf273ae55c71aea50dcf2c94b55b68b502->enter($__internal_50e4d2338a5f9ab3c4b9aebf4735e3cf273ae55c71aea50dcf2c94b55b68b502_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_50e4d2338a5f9ab3c4b9aebf4735e3cf273ae55c71aea50dcf2c94b55b68b502->leave($__internal_50e4d2338a5f9ab3c4b9aebf4735e3cf273ae55c71aea50dcf2c94b55b68b502_prof);

        
        $__internal_6d727abf62b9f267986f2067b070aaacd4e6c10ecabc6e4e54d00acd588d2af3->leave($__internal_6d727abf62b9f267986f2067b070aaacd4e6c10ecabc6e4e54d00acd588d2af3_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_3d60bab2a0baa500befbc6705d0828d2d4c01e5972321f01a063360149d9edb0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3d60bab2a0baa500befbc6705d0828d2d4c01e5972321f01a063360149d9edb0->enter($__internal_3d60bab2a0baa500befbc6705d0828d2d4c01e5972321f01a063360149d9edb0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_2fd1169df4c9e88db2f9259dfecaa0a678a2f2b0db78f4882072f6f0e5c6454e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2fd1169df4c9e88db2f9259dfecaa0a678a2f2b0db78f4882072f6f0e5c6454e->enter($__internal_2fd1169df4c9e88db2f9259dfecaa0a678a2f2b0db78f4882072f6f0e5c6454e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, ($context["file"] ?? $this->getContext($context, "file")), "html", null, true);
        if ((0 < ($context["line"] ?? $this->getContext($context, "line")))) {
            echo " <small>line ";
            echo twig_escape_filter($this->env, ($context["line"] ?? $this->getContext($context, "line")), "html", null, true);
            echo "</small>";
        }
        echo "</h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(($context["filename"] ?? $this->getContext($context, "filename")), ($context["line"] ?? $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_2fd1169df4c9e88db2f9259dfecaa0a678a2f2b0db78f4882072f6f0e5c6454e->leave($__internal_2fd1169df4c9e88db2f9259dfecaa0a678a2f2b0db78f4882072f6f0e5c6454e_prof);

        
        $__internal_3d60bab2a0baa500befbc6705d0828d2d4c01e5972321f01a063360149d9edb0->leave($__internal_3d60bab2a0baa500befbc6705d0828d2d4c01e5972321f01a063360149d9edb0_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 15,  87 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }}{% if 0 < line %} <small>line {{ line }}</small>{% endif %}</h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "WebProfilerBundle:Profiler:open.html.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}
