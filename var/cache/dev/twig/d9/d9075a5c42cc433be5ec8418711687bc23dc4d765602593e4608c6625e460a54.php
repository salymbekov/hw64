<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_250f300249428ede894cdffc7e234cb1345bb6dca4cec9493f18c64ae3fa0970 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_19003760ec8eeab65d1e6faa94a18db5e4d1c3ae9de79e1727b873db22aedf3d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_19003760ec8eeab65d1e6faa94a18db5e4d1c3ae9de79e1727b873db22aedf3d->enter($__internal_19003760ec8eeab65d1e6faa94a18db5e4d1c3ae9de79e1727b873db22aedf3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        $__internal_797d12e53a667ce28eaa54a960d14b6edad74897a9045a47799c7a9e6907b641 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_797d12e53a667ce28eaa54a960d14b6edad74897a9045a47799c7a9e6907b641->enter($__internal_797d12e53a667ce28eaa54a960d14b6edad74897a9045a47799c7a9e6907b641_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_19003760ec8eeab65d1e6faa94a18db5e4d1c3ae9de79e1727b873db22aedf3d->leave($__internal_19003760ec8eeab65d1e6faa94a18db5e4d1c3ae9de79e1727b873db22aedf3d_prof);

        
        $__internal_797d12e53a667ce28eaa54a960d14b6edad74897a9045a47799c7a9e6907b641->leave($__internal_797d12e53a667ce28eaa54a960d14b6edad74897a9045a47799c7a9e6907b641_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/container_attributes.html.php");
    }
}
