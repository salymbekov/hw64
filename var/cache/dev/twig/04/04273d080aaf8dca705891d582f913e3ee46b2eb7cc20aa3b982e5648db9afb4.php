<?php

/* TwigBundle:Exception:error.txt.twig */
class __TwigTemplate_fba6a945a5a86a4d025a1285ae8f405d22d5ddd2819a91daf6f48578b094e010 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b67e32455fb09fd8fe15a6cc9b6d52e0caeea135a88a077be8380db598752be3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b67e32455fb09fd8fe15a6cc9b6d52e0caeea135a88a077be8380db598752be3->enter($__internal_b67e32455fb09fd8fe15a6cc9b6d52e0caeea135a88a077be8380db598752be3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        $__internal_e79560efa2a21997ea5877188347fe20caf657ac3cec0a2b433c4e6cc0717d20 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e79560efa2a21997ea5877188347fe20caf657ac3cec0a2b433c4e6cc0717d20->enter($__internal_e79560efa2a21997ea5877188347fe20caf657ac3cec0a2b433c4e6cc0717d20_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo ($context["status_code"] ?? $this->getContext($context, "status_code"));
        echo " ";
        echo ($context["status_text"] ?? $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_b67e32455fb09fd8fe15a6cc9b6d52e0caeea135a88a077be8380db598752be3->leave($__internal_b67e32455fb09fd8fe15a6cc9b6d52e0caeea135a88a077be8380db598752be3_prof);

        
        $__internal_e79560efa2a21997ea5877188347fe20caf657ac3cec0a2b433c4e6cc0717d20->leave($__internal_e79560efa2a21997ea5877188347fe20caf657ac3cec0a2b433c4e6cc0717d20_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("Oops! An Error Occurred
=======================

The server returned a \"{{ status_code }} {{ status_text }}\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
", "TwigBundle:Exception:error.txt.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.txt.twig");
    }
}
