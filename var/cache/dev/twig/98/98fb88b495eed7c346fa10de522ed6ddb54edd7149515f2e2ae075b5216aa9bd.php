<?php

/* FOSUserBundle:Registration:email.txt.twig */
class __TwigTemplate_d762083be76568cfbacd4ca34c2ce03e3311aec1a0801420cca4fa3271b885e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b0d96a58f1307370734ec38258787d291188a1ef480687ee753d71845fe42204 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b0d96a58f1307370734ec38258787d291188a1ef480687ee753d71845fe42204->enter($__internal_b0d96a58f1307370734ec38258787d291188a1ef480687ee753d71845fe42204_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        $__internal_c76af514c431ad46b44f3770352081a917c839a3b650b4bf6987dd5204693fce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c76af514c431ad46b44f3770352081a917c839a3b650b4bf6987dd5204693fce->enter($__internal_c76af514c431ad46b44f3770352081a917c839a3b650b4bf6987dd5204693fce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_b0d96a58f1307370734ec38258787d291188a1ef480687ee753d71845fe42204->leave($__internal_b0d96a58f1307370734ec38258787d291188a1ef480687ee753d71845fe42204_prof);

        
        $__internal_c76af514c431ad46b44f3770352081a917c839a3b650b4bf6987dd5204693fce->leave($__internal_c76af514c431ad46b44f3770352081a917c839a3b650b4bf6987dd5204693fce_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_d7a7cb9009c54d53629d8957371cab515a3ce413a67f2cde89514987109b0309 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d7a7cb9009c54d53629d8957371cab515a3ce413a67f2cde89514987109b0309->enter($__internal_d7a7cb9009c54d53629d8957371cab515a3ce413a67f2cde89514987109b0309_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_ec1f90d70c80a96149179a0302644b24b5899e5da2c7fd3981bc668db28b9c6f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ec1f90d70c80a96149179a0302644b24b5899e5da2c7fd3981bc668db28b9c6f->enter($__internal_ec1f90d70c80a96149179a0302644b24b5899e5da2c7fd3981bc668db28b9c6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        
        $__internal_ec1f90d70c80a96149179a0302644b24b5899e5da2c7fd3981bc668db28b9c6f->leave($__internal_ec1f90d70c80a96149179a0302644b24b5899e5da2c7fd3981bc668db28b9c6f_prof);

        
        $__internal_d7a7cb9009c54d53629d8957371cab515a3ce413a67f2cde89514987109b0309->leave($__internal_d7a7cb9009c54d53629d8957371cab515a3ce413a67f2cde89514987109b0309_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_f26017ebe518e99e369487d58f184375688eaeed5fe72975f1e3e6d494732234 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f26017ebe518e99e369487d58f184375688eaeed5fe72975f1e3e6d494732234->enter($__internal_f26017ebe518e99e369487d58f184375688eaeed5fe72975f1e3e6d494732234_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_a571ecc38d140d32c861f0472e718afdf07734bfce50157a52fd4debf9ad632e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a571ecc38d140d32c861f0472e718afdf07734bfce50157a52fd4debf9ad632e->enter($__internal_a571ecc38d140d32c861f0472e718afdf07734bfce50157a52fd4debf9ad632e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_a571ecc38d140d32c861f0472e718afdf07734bfce50157a52fd4debf9ad632e->leave($__internal_a571ecc38d140d32c861f0472e718afdf07734bfce50157a52fd4debf9ad632e_prof);

        
        $__internal_f26017ebe518e99e369487d58f184375688eaeed5fe72975f1e3e6d494732234->leave($__internal_f26017ebe518e99e369487d58f184375688eaeed5fe72975f1e3e6d494732234_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_1c41993f76751c7d17fca58b93849e006605ddbd3312661ee39a0805c3c99b0f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1c41993f76751c7d17fca58b93849e006605ddbd3312661ee39a0805c3c99b0f->enter($__internal_1c41993f76751c7d17fca58b93849e006605ddbd3312661ee39a0805c3c99b0f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_69a749785e1bbe7b50395b1df2eb0a8f1ba13adf5af5de4d027d262aa6f49cd8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_69a749785e1bbe7b50395b1df2eb0a8f1ba13adf5af5de4d027d262aa6f49cd8->enter($__internal_69a749785e1bbe7b50395b1df2eb0a8f1ba13adf5af5de4d027d262aa6f49cd8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_69a749785e1bbe7b50395b1df2eb0a8f1ba13adf5af5de4d027d262aa6f49cd8->leave($__internal_69a749785e1bbe7b50395b1df2eb0a8f1ba13adf5af5de4d027d262aa6f49cd8_prof);

        
        $__internal_1c41993f76751c7d17fca58b93849e006605ddbd3312661ee39a0805c3c99b0f->leave($__internal_1c41993f76751c7d17fca58b93849e006605ddbd3312661ee39a0805c3c99b0f_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Registration:email.txt.twig", "/home/timur/http/hw/hw64/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/email.txt.twig");
    }
}
