<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_ada3f887848910813e07bddaec2cb63b39c2dd24d3a8fb645ea4542c7cff8813 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_123b2187b01e4a2cd8b2ac5f6386e2b239197d7b08b4e0092115a7a869d1e986 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_123b2187b01e4a2cd8b2ac5f6386e2b239197d7b08b4e0092115a7a869d1e986->enter($__internal_123b2187b01e4a2cd8b2ac5f6386e2b239197d7b08b4e0092115a7a869d1e986_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        $__internal_9ae48a3cbd7d510d322ae9cd42c4011ad412647360c67d5025d6657dbda68aae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9ae48a3cbd7d510d322ae9cd42c4011ad412647360c67d5025d6657dbda68aae->enter($__internal_9ae48a3cbd7d510d322ae9cd42c4011ad412647360c67d5025d6657dbda68aae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => ($context["status_code"] ?? $this->getContext($context, "status_code")), "message" => ($context["status_text"] ?? $this->getContext($context, "status_text")), "exception" => $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_123b2187b01e4a2cd8b2ac5f6386e2b239197d7b08b4e0092115a7a869d1e986->leave($__internal_123b2187b01e4a2cd8b2ac5f6386e2b239197d7b08b4e0092115a7a869d1e986_prof);

        
        $__internal_9ae48a3cbd7d510d322ae9cd42c4011ad412647360c67d5025d6657dbda68aae->leave($__internal_9ae48a3cbd7d510d322ae9cd42c4011ad412647360c67d5025d6657dbda68aae_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}
", "TwigBundle:Exception:exception.json.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.json.twig");
    }
}
