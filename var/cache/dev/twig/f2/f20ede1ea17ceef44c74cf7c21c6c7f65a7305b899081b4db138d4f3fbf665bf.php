<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_8421279ea8b6ae7f6b2d4775a41eeadfff28a4c7ceb6f389a9915703537af95c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b7d81665f5aead334f0184d4c03c97df7371310a7c4891fe44eb811e8be77d6d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b7d81665f5aead334f0184d4c03c97df7371310a7c4891fe44eb811e8be77d6d->enter($__internal_b7d81665f5aead334f0184d4c03c97df7371310a7c4891fe44eb811e8be77d6d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        $__internal_cb6ffd6b4cfc57ef0c7adf3e9944fe4ae9695909aa39a6320c2f98397dbcd0f7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb6ffd6b4cfc57ef0c7adf3e9944fe4ae9695909aa39a6320c2f98397dbcd0f7->enter($__internal_cb6ffd6b4cfc57ef0c7adf3e9944fe4ae9695909aa39a6320c2f98397dbcd0f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_b7d81665f5aead334f0184d4c03c97df7371310a7c4891fe44eb811e8be77d6d->leave($__internal_b7d81665f5aead334f0184d4c03c97df7371310a7c4891fe44eb811e8be77d6d_prof);

        
        $__internal_cb6ffd6b4cfc57ef0c7adf3e9944fe4ae9695909aa39a6320c2f98397dbcd0f7->leave($__internal_cb6ffd6b4cfc57ef0c7adf3e9944fe4ae9695909aa39a6320c2f98397dbcd0f7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
", "@Framework/Form/email_widget.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/email_widget.html.php");
    }
}
