<?php

/* AppBundle:Words:index.html.twig */
class __TwigTemplate_250cd1e8a109b901b5ba4e3410e047ce57c505eb807e5998ac17204c4def4fd4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "AppBundle:Words:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bfece8326f64591e13a05352acc9b8c06581188641a18f41aac3cfadc5c24ebe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bfece8326f64591e13a05352acc9b8c06581188641a18f41aac3cfadc5c24ebe->enter($__internal_bfece8326f64591e13a05352acc9b8c06581188641a18f41aac3cfadc5c24ebe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Words:index.html.twig"));

        $__internal_86c2a906cd286575e8e55e1736b28398d14441f0581f160b86dba8665fb65831 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_86c2a906cd286575e8e55e1736b28398d14441f0581f160b86dba8665fb65831->enter($__internal_86c2a906cd286575e8e55e1736b28398d14441f0581f160b86dba8665fb65831_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Words:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bfece8326f64591e13a05352acc9b8c06581188641a18f41aac3cfadc5c24ebe->leave($__internal_bfece8326f64591e13a05352acc9b8c06581188641a18f41aac3cfadc5c24ebe_prof);

        
        $__internal_86c2a906cd286575e8e55e1736b28398d14441f0581f160b86dba8665fb65831->leave($__internal_86c2a906cd286575e8e55e1736b28398d14441f0581f160b86dba8665fb65831_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_1dca75bd51ffbc91bb68c6d51a9fbabcca20d192c2d5f7d16eb07ff21b35fe2c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1dca75bd51ffbc91bb68c6d51a9fbabcca20d192c2d5f7d16eb07ff21b35fe2c->enter($__internal_1dca75bd51ffbc91bb68c6d51a9fbabcca20d192c2d5f7d16eb07ff21b35fe2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_b5b005e64979e7cab7611d82dbe9ed23e625d584f44fbed376ad951ec2708a36 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b5b005e64979e7cab7611d82dbe9ed23e625d584f44fbed376ad951ec2708a36->enter($__internal_b5b005e64979e7cab7611d82dbe9ed23e625d584f44fbed376ad951ec2708a36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Цех переводчиков";
        
        $__internal_b5b005e64979e7cab7611d82dbe9ed23e625d584f44fbed376ad951ec2708a36->leave($__internal_b5b005e64979e7cab7611d82dbe9ed23e625d584f44fbed376ad951ec2708a36_prof);

        
        $__internal_1dca75bd51ffbc91bb68c6d51a9fbabcca20d192c2d5f7d16eb07ff21b35fe2c->leave($__internal_1dca75bd51ffbc91bb68c6d51a9fbabcca20d192c2d5f7d16eb07ff21b35fe2c_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_4bc123492582394949de6e641711218ff11078f4708300734205560a9c000273 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4bc123492582394949de6e641711218ff11078f4708300734205560a9c000273->enter($__internal_4bc123492582394949de6e641711218ff11078f4708300734205560a9c000273_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_4c4a5fc356fcbde39a077bfada7329eafa8fca86fb99a5f87d4a7483e22b2e5e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c4a5fc356fcbde39a077bfada7329eafa8fca86fb99a5f87d4a7483e22b2e5e->enter($__internal_4c4a5fc356fcbde39a077bfada7329eafa8fca86fb99a5f87d4a7483e22b2e5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "    <h1> ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("добро пожаловать в цех переводчиков", array(), "messages");
        echo "</h1>

    ";
        // line 9
        if ($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array())) {
            // line 10
            echo "        ";
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form');
            echo "
    ";
        } else {
            // line 12
            echo "        <p>
            ";
            // line 13
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("Чтобы иметь возможность оставлять заказ на перевод", array(), "messages");
            // line 14
            echo "            <a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\">";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("авторизуйтесь", array(), "messages");
            echo "</a>, ";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("а если нет аккаунта, пройдите", array(), "messages");
            echo " <a
                    href=\"";
            // line 15
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register");
            echo "\">";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("регистрацию", array(), "messages");
            echo "</a> ";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("на сайте", array(), "messages");
            // line 16
            echo "        </p>
    ";
        }
        // line 18
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["words"] ?? $this->getContext($context, "words")));
        foreach ($context['_seq'] as $context["_key"] => $context["word"]) {
            // line 19
            echo "        <p><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_words_translate", array("id" => $this->getAttribute($context["word"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["word"], "translate", array(0 => "ru"), "method"), "word", array()), "html", null, true);
            echo "</a></p>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['word'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_4c4a5fc356fcbde39a077bfada7329eafa8fca86fb99a5f87d4a7483e22b2e5e->leave($__internal_4c4a5fc356fcbde39a077bfada7329eafa8fca86fb99a5f87d4a7483e22b2e5e_prof);

        
        $__internal_4bc123492582394949de6e641711218ff11078f4708300734205560a9c000273->leave($__internal_4bc123492582394949de6e641711218ff11078f4708300734205560a9c000273_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Words:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 19,  106 => 18,  102 => 16,  96 => 15,  87 => 14,  85 => 13,  82 => 12,  76 => 10,  74 => 9,  68 => 7,  59 => 6,  41 => 4,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base.html.twig\" %}


{% block title %}Цех переводчиков{% endblock %}

{% block body %}
    <h1> {% trans %}добро пожаловать в цех переводчиков{% endtrans %}</h1>

    {% if  app.user %}
        {{ form(form) }}
    {% else %}
        <p>
            {% trans %}Чтобы иметь возможность оставлять заказ на перевод{% endtrans %}
            <a href=\"{{ path('fos_user_security_login') }}\">{% trans %}авторизуйтесь{% endtrans %}</a>, {% trans %}а если нет аккаунта, пройдите{% endtrans %} <a
                    href=\"{{ path('fos_user_registration_register') }}\">{% trans %}регистрацию{% endtrans %}</a> {% trans %}на сайте{% endtrans %}
        </p>
    {% endif %}
    {% for word in words %}
        <p><a href=\"{{ path('app_words_translate', {'id': word.id}) }}\">{{ word.translate('ru').word }}</a></p>
    {% endfor %}
{% endblock %}
", "AppBundle:Words:index.html.twig", "/home/timur/http/hw/hw64/src/AppBundle/Resources/views/Words/index.html.twig");
    }
}
