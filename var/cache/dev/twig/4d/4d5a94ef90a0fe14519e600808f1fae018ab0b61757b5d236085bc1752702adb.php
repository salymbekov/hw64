<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_0b57d506e6dd60125c131a1ab6921150ff7e82f951f724f20e671eb146fc815f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_10a7a4a000370a554c2b841f252580a153ddc43f4fdf2c7f4f7b9762c97b163d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_10a7a4a000370a554c2b841f252580a153ddc43f4fdf2c7f4f7b9762c97b163d->enter($__internal_10a7a4a000370a554c2b841f252580a153ddc43f4fdf2c7f4f7b9762c97b163d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        $__internal_91369efdc6f3ea55688ecb503febc4006111c4ee51ba37d60c81481a393b6a43 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_91369efdc6f3ea55688ecb503febc4006111c4ee51ba37d60c81481a393b6a43->enter($__internal_91369efdc6f3ea55688ecb503febc4006111c4ee51ba37d60c81481a393b6a43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_10a7a4a000370a554c2b841f252580a153ddc43f4fdf2c7f4f7b9762c97b163d->leave($__internal_10a7a4a000370a554c2b841f252580a153ddc43f4fdf2c7f4f7b9762c97b163d_prof);

        
        $__internal_91369efdc6f3ea55688ecb503febc4006111c4ee51ba37d60c81481a393b6a43->leave($__internal_91369efdc6f3ea55688ecb503febc4006111c4ee51ba37d60c81481a393b6a43_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->widget(\$form) ?>
", "@Framework/Form/hidden_row.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_row.html.php");
    }
}
