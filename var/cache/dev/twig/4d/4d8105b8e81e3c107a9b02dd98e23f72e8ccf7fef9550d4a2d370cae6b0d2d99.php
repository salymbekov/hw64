<?php

/* WebProfilerBundle:Collector:ajax.html.twig */
class __TwigTemplate_c6050198902f51836ce41e2cd763b8f30a40d3a670a7c089f848e402ab24e8a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8e83eb08a6019afa2bfaa7724c2334bce856f4597b9f38c5369d1be0f36182a8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8e83eb08a6019afa2bfaa7724c2334bce856f4597b9f38c5369d1be0f36182a8->enter($__internal_8e83eb08a6019afa2bfaa7724c2334bce856f4597b9f38c5369d1be0f36182a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:ajax.html.twig"));

        $__internal_767c66ff5d8a22ba0ff4baa3e167d3ca8bd92259a7cebe6201ae1035dced50c3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_767c66ff5d8a22ba0ff4baa3e167d3ca8bd92259a7cebe6201ae1035dced50c3->enter($__internal_767c66ff5d8a22ba0ff4baa3e167d3ca8bd92259a7cebe6201ae1035dced50c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8e83eb08a6019afa2bfaa7724c2334bce856f4597b9f38c5369d1be0f36182a8->leave($__internal_8e83eb08a6019afa2bfaa7724c2334bce856f4597b9f38c5369d1be0f36182a8_prof);

        
        $__internal_767c66ff5d8a22ba0ff4baa3e167d3ca8bd92259a7cebe6201ae1035dced50c3->leave($__internal_767c66ff5d8a22ba0ff4baa3e167d3ca8bd92259a7cebe6201ae1035dced50c3_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_07619c1e211f19779630d7c8a6b76987a3f6690ff30f89204c238b62536e901b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_07619c1e211f19779630d7c8a6b76987a3f6690ff30f89204c238b62536e901b->enter($__internal_07619c1e211f19779630d7c8a6b76987a3f6690ff30f89204c238b62536e901b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_3ff06dac9a96c2dd398fbb7e62978944b0d576df85de91b63635a8853f68044a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3ff06dac9a96c2dd398fbb7e62978944b0d576df85de91b63635a8853f68044a->enter($__internal_3ff06dac9a96c2dd398fbb7e62978944b0d576df85de91b63635a8853f68044a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_3ff06dac9a96c2dd398fbb7e62978944b0d576df85de91b63635a8853f68044a->leave($__internal_3ff06dac9a96c2dd398fbb7e62978944b0d576df85de91b63635a8853f68044a_prof);

        
        $__internal_07619c1e211f19779630d7c8a6b76987a3f6690ff30f89204c238b62536e901b->leave($__internal_07619c1e211f19779630d7c8a6b76987a3f6690ff30f89204c238b62536e901b_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "WebProfilerBundle:Collector:ajax.html.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/ajax.html.twig");
    }
}
