<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_d0831051694ed470e785aeec9a4609433d16fcd78478c0c70c23e2e3357d7c46 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a37027c0513e9490f559252f927605c03ed79ab5ac5cdc263aafac9e91dfa610 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a37027c0513e9490f559252f927605c03ed79ab5ac5cdc263aafac9e91dfa610->enter($__internal_a37027c0513e9490f559252f927605c03ed79ab5ac5cdc263aafac9e91dfa610_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        $__internal_3c8a691472d4d973d5352796fc6d51f00f3c71360ccb518572b8d46840cffd1c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3c8a691472d4d973d5352796fc6d51f00f3c71360ccb518572b8d46840cffd1c->enter($__internal_3c8a691472d4d973d5352796fc6d51f00f3c71360ccb518572b8d46840cffd1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_a37027c0513e9490f559252f927605c03ed79ab5ac5cdc263aafac9e91dfa610->leave($__internal_a37027c0513e9490f559252f927605c03ed79ab5ac5cdc263aafac9e91dfa610_prof);

        
        $__internal_3c8a691472d4d973d5352796fc6d51f00f3c71360ccb518572b8d46840cffd1c->leave($__internal_3c8a691472d4d973d5352796fc6d51f00f3c71360ccb518572b8d46840cffd1c_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_83dbeb6fbc3856966199e9e767cbeb6b13d58476c91bc9144e5cb78c9ab29af9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_83dbeb6fbc3856966199e9e767cbeb6b13d58476c91bc9144e5cb78c9ab29af9->enter($__internal_83dbeb6fbc3856966199e9e767cbeb6b13d58476c91bc9144e5cb78c9ab29af9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_9e27fa70b291d7b259dd41825daf4a079523e27fdbd4bc1f88e7f654b334d94a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9e27fa70b291d7b259dd41825daf4a079523e27fdbd4bc1f88e7f654b334d94a->enter($__internal_9e27fa70b291d7b259dd41825daf4a079523e27fdbd4bc1f88e7f654b334d94a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        
        $__internal_9e27fa70b291d7b259dd41825daf4a079523e27fdbd4bc1f88e7f654b334d94a->leave($__internal_9e27fa70b291d7b259dd41825daf4a079523e27fdbd4bc1f88e7f654b334d94a_prof);

        
        $__internal_83dbeb6fbc3856966199e9e767cbeb6b13d58476c91bc9144e5cb78c9ab29af9->leave($__internal_83dbeb6fbc3856966199e9e767cbeb6b13d58476c91bc9144e5cb78c9ab29af9_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_869835e5b4eb4e8c3d9ed30ba518a1f741b00dbafbca2ad9af30bc586b13185a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_869835e5b4eb4e8c3d9ed30ba518a1f741b00dbafbca2ad9af30bc586b13185a->enter($__internal_869835e5b4eb4e8c3d9ed30ba518a1f741b00dbafbca2ad9af30bc586b13185a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_42c7e58dd39e33151929a1b091d816dca943d9bcda4ff731dc0fa942e9a1f483 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_42c7e58dd39e33151929a1b091d816dca943d9bcda4ff731dc0fa942e9a1f483->enter($__internal_42c7e58dd39e33151929a1b091d816dca943d9bcda4ff731dc0fa942e9a1f483_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_42c7e58dd39e33151929a1b091d816dca943d9bcda4ff731dc0fa942e9a1f483->leave($__internal_42c7e58dd39e33151929a1b091d816dca943d9bcda4ff731dc0fa942e9a1f483_prof);

        
        $__internal_869835e5b4eb4e8c3d9ed30ba518a1f741b00dbafbca2ad9af30bc586b13185a->leave($__internal_869835e5b4eb4e8c3d9ed30ba518a1f741b00dbafbca2ad9af30bc586b13185a_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_ed646126f0c99e81bbdac68b4625473ea78bc05ed182460efa3d6446b2b3692b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ed646126f0c99e81bbdac68b4625473ea78bc05ed182460efa3d6446b2b3692b->enter($__internal_ed646126f0c99e81bbdac68b4625473ea78bc05ed182460efa3d6446b2b3692b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_348ca7a8b34dccd01558f6013fdb47961bf7b673a315e2b93a8e912ea96ebf86 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_348ca7a8b34dccd01558f6013fdb47961bf7b673a315e2b93a8e912ea96ebf86->enter($__internal_348ca7a8b34dccd01558f6013fdb47961bf7b673a315e2b93a8e912ea96ebf86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_348ca7a8b34dccd01558f6013fdb47961bf7b673a315e2b93a8e912ea96ebf86->leave($__internal_348ca7a8b34dccd01558f6013fdb47961bf7b673a315e2b93a8e912ea96ebf86_prof);

        
        $__internal_ed646126f0c99e81bbdac68b4625473ea78bc05ed182460efa3d6446b2b3692b->leave($__internal_ed646126f0c99e81bbdac68b4625473ea78bc05ed182460efa3d6446b2b3692b_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'resetting.email.subject'|trans({'%username%': user.username}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Resetting:email.txt.twig", "/home/timur/http/hw/hw64/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/email.txt.twig");
    }
}
