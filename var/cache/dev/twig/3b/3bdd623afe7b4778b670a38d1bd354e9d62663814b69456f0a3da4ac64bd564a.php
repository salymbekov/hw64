<?php

/* FOSUserBundle:Resetting:check_email.html.twig */
class __TwigTemplate_9cd4ced977090cb8b7003ca12badc461f0e88890a58c30c912321620bf181a89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_452d7e66145fc5d11d31993a13b94e144f0850b1035905a369951c8657c43f68 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_452d7e66145fc5d11d31993a13b94e144f0850b1035905a369951c8657c43f68->enter($__internal_452d7e66145fc5d11d31993a13b94e144f0850b1035905a369951c8657c43f68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $__internal_8152109fd83f982fedd6c9491da6869c36fd7c5f8bf74b5b32bee1f19bbe6bf2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8152109fd83f982fedd6c9491da6869c36fd7c5f8bf74b5b32bee1f19bbe6bf2->enter($__internal_8152109fd83f982fedd6c9491da6869c36fd7c5f8bf74b5b32bee1f19bbe6bf2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_452d7e66145fc5d11d31993a13b94e144f0850b1035905a369951c8657c43f68->leave($__internal_452d7e66145fc5d11d31993a13b94e144f0850b1035905a369951c8657c43f68_prof);

        
        $__internal_8152109fd83f982fedd6c9491da6869c36fd7c5f8bf74b5b32bee1f19bbe6bf2->leave($__internal_8152109fd83f982fedd6c9491da6869c36fd7c5f8bf74b5b32bee1f19bbe6bf2_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_d252e5cd9fc6cc489ef67aea1c912ecaf4a6734cf1d3bb24a540a86f7709a871 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d252e5cd9fc6cc489ef67aea1c912ecaf4a6734cf1d3bb24a540a86f7709a871->enter($__internal_d252e5cd9fc6cc489ef67aea1c912ecaf4a6734cf1d3bb24a540a86f7709a871_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_3861a2fee5330d84ea091299a6fddddea4680d00d4d7423ce18bc911db813df9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3861a2fee5330d84ea091299a6fddddea4680d00d4d7423ce18bc911db813df9->enter($__internal_3861a2fee5330d84ea091299a6fddddea4680d00d4d7423ce18bc911db813df9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo nl2br(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.check_email", array("%tokenLifetime%" => ($context["tokenLifetime"] ?? $this->getContext($context, "tokenLifetime"))), "FOSUserBundle"), "html", null, true));
        echo "
</p>
";
        
        $__internal_3861a2fee5330d84ea091299a6fddddea4680d00d4d7423ce18bc911db813df9->leave($__internal_3861a2fee5330d84ea091299a6fddddea4680d00d4d7423ce18bc911db813df9_prof);

        
        $__internal_d252e5cd9fc6cc489ef67aea1c912ecaf4a6734cf1d3bb24a540a86f7709a871->leave($__internal_d252e5cd9fc6cc489ef67aea1c912ecaf4a6734cf1d3bb24a540a86f7709a871_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
<p>
{{ 'resetting.check_email'|trans({'%tokenLifetime%': tokenLifetime})|nl2br }}
</p>
{% endblock %}
", "FOSUserBundle:Resetting:check_email.html.twig", "/home/timur/http/hw/hw64/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/check_email.html.twig");
    }
}
