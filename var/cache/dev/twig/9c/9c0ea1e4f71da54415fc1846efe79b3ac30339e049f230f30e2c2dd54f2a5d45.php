<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_b2af6e3d9e1fb40f2125115218f241fcbbc3ace608a0858f61368204fece439f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_96756d560726e3c0145e13ffa554e4666fb539a39af10c969bf8ad2f96512f1a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_96756d560726e3c0145e13ffa554e4666fb539a39af10c969bf8ad2f96512f1a->enter($__internal_96756d560726e3c0145e13ffa554e4666fb539a39af10c969bf8ad2f96512f1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        $__internal_cd74b3940c2740575667ea31ef9084f543610e928a066d4c9214ea3a75208e13 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cd74b3940c2740575667ea31ef9084f543610e928a066d4c9214ea3a75208e13->enter($__internal_cd74b3940c2740575667ea31ef9084f543610e928a066d4c9214ea3a75208e13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_96756d560726e3c0145e13ffa554e4666fb539a39af10c969bf8ad2f96512f1a->leave($__internal_96756d560726e3c0145e13ffa554e4666fb539a39af10c969bf8ad2f96512f1a_prof);

        
        $__internal_cd74b3940c2740575667ea31ef9084f543610e928a066d4c9214ea3a75208e13->leave($__internal_cd74b3940c2740575667ea31ef9084f543610e928a066d4c9214ea3a75208e13_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "TwigBundle:Exception:error.js.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.js.twig");
    }
}
