<?php

/* TwigBundle:Exception:traces_text.html.twig */
class __TwigTemplate_cacfc3fb90faafd775762d301ab91d931ca8c5fd078ac8f7e8dc009148b878ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            '__internal_b8a24fdb19b91658ed8fcba593e8e4fd2640335715f392371e029e3eb62c0573' => array($this, 'block___internal_b8a24fdb19b91658ed8fcba593e8e4fd2640335715f392371e029e3eb62c0573'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_274b36c1f9613eeac0b1f0c8e49cab3215195a2e39f2c5b3351ceeefefefa096 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_274b36c1f9613eeac0b1f0c8e49cab3215195a2e39f2c5b3351ceeefefefa096->enter($__internal_274b36c1f9613eeac0b1f0c8e49cab3215195a2e39f2c5b3351ceeefefefa096_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:traces_text.html.twig"));

        $__internal_10490194d3bbaa92641e8270d2e746b68e4cf02e52e0b9edb807084644269b81 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_10490194d3bbaa92641e8270d2e746b68e4cf02e52e0b9edb807084644269b81->enter($__internal_10490194d3bbaa92641e8270d2e746b68e4cf02e52e0b9edb807084644269b81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:traces_text.html.twig"));

        // line 1
        echo "<table class=\"trace trace-as-text\">
    <thead class=\"trace-head\">
        <tr>
            <th class=\"sf-toggle\" data-toggle-selector=\"#trace-text-";
        // line 4
        echo twig_escape_filter($this->env, ($context["index"] ?? $this->getContext($context, "index")), "html", null, true);
        echo "\" data-toggle-initial=\"";
        echo (((1 == ($context["index"] ?? $this->getContext($context, "index")))) ? ("display") : (""));
        echo "\">
                <h3 class=\"trace-class\">
                    ";
        // line 6
        if ((($context["num_exceptions"] ?? $this->getContext($context, "num_exceptions")) > 1)) {
            // line 7
            echo "                        <span class=\"text-muted\">[";
            echo twig_escape_filter($this->env, ((($context["num_exceptions"] ?? $this->getContext($context, "num_exceptions")) - ($context["index"] ?? $this->getContext($context, "index"))) + 1), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, ($context["num_exceptions"] ?? $this->getContext($context, "num_exceptions")), "html", null, true);
            echo "]</span>
                    ";
        }
        // line 9
        echo "                    ";
        echo twig_escape_filter($this->env, twig_last($this->env, twig_split_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "class", array()), "\\")), "html", null, true);
        echo "
                    <span class=\"icon icon-close\">";
        // line 10
        echo twig_include($this->env, $context, "@Twig/images/icon-minus-square-o.svg");
        echo "</span>
                    <span class=\"icon icon-open\">";
        // line 11
        echo twig_include($this->env, $context, "@Twig/images/icon-plus-square-o.svg");
        echo "</span>
                </h3>
            </th>
        </tr>
    </thead>

    <tbody id=\"trace-text-";
        // line 17
        echo twig_escape_filter($this->env, ($context["index"] ?? $this->getContext($context, "index")), "html", null, true);
        echo "\">
        <tr>
            <td>
                ";
        // line 20
        if (twig_length_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "trace", array()))) {
            // line 21
            echo "                <pre class=\"stacktrace\">";
            // line 22
            echo twig_escape_filter($this->env,             $this->renderBlock("__internal_b8a24fdb19b91658ed8fcba593e8e4fd2640335715f392371e029e3eb62c0573", $context, $blocks), "html");
            // line 25
            echo "                </pre>
                ";
        }
        // line 27
        echo "            </td>
        </tr>
    </tbody>
</table>
";
        
        $__internal_274b36c1f9613eeac0b1f0c8e49cab3215195a2e39f2c5b3351ceeefefefa096->leave($__internal_274b36c1f9613eeac0b1f0c8e49cab3215195a2e39f2c5b3351ceeefefefa096_prof);

        
        $__internal_10490194d3bbaa92641e8270d2e746b68e4cf02e52e0b9edb807084644269b81->leave($__internal_10490194d3bbaa92641e8270d2e746b68e4cf02e52e0b9edb807084644269b81_prof);

    }

    // line 22
    public function block___internal_b8a24fdb19b91658ed8fcba593e8e4fd2640335715f392371e029e3eb62c0573($context, array $blocks = array())
    {
        $__internal_6975a908676eab5ba1d024bb47935b89b2a6fbbf6ee4a582f3661329b9d47a5a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6975a908676eab5ba1d024bb47935b89b2a6fbbf6ee4a582f3661329b9d47a5a->enter($__internal_6975a908676eab5ba1d024bb47935b89b2a6fbbf6ee4a582f3661329b9d47a5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "__internal_b8a24fdb19b91658ed8fcba593e8e4fd2640335715f392371e029e3eb62c0573"));

        $__internal_70ba08ad1c4524b9b4bda748cc8cfc2b7770f337af41a04e689ce3d4cea1d74c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_70ba08ad1c4524b9b4bda748cc8cfc2b7770f337af41a04e689ce3d4cea1d74c->enter($__internal_70ba08ad1c4524b9b4bda748cc8cfc2b7770f337af41a04e689ce3d4cea1d74c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "__internal_b8a24fdb19b91658ed8fcba593e8e4fd2640335715f392371e029e3eb62c0573"));

        // line 23
        echo twig_include($this->env, $context, "@Twig/Exception/traces.txt.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception")), "format" => "html"), false);
        echo "
                ";
        
        $__internal_70ba08ad1c4524b9b4bda748cc8cfc2b7770f337af41a04e689ce3d4cea1d74c->leave($__internal_70ba08ad1c4524b9b4bda748cc8cfc2b7770f337af41a04e689ce3d4cea1d74c_prof);

        
        $__internal_6975a908676eab5ba1d024bb47935b89b2a6fbbf6ee4a582f3661329b9d47a5a->leave($__internal_6975a908676eab5ba1d024bb47935b89b2a6fbbf6ee4a582f3661329b9d47a5a_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:traces_text.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 23,  96 => 22,  82 => 27,  78 => 25,  76 => 22,  74 => 21,  72 => 20,  66 => 17,  57 => 11,  53 => 10,  48 => 9,  40 => 7,  38 => 6,  31 => 4,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<table class=\"trace trace-as-text\">
    <thead class=\"trace-head\">
        <tr>
            <th class=\"sf-toggle\" data-toggle-selector=\"#trace-text-{{ index }}\" data-toggle-initial=\"{{ 1 == index ? 'display' }}\">
                <h3 class=\"trace-class\">
                    {% if num_exceptions > 1 %}
                        <span class=\"text-muted\">[{{ num_exceptions - index + 1 }}/{{ num_exceptions }}]</span>
                    {% endif %}
                    {{ exception.class|split('\\\\')|last }}
                    <span class=\"icon icon-close\">{{ include('@Twig/images/icon-minus-square-o.svg') }}</span>
                    <span class=\"icon icon-open\">{{ include('@Twig/images/icon-plus-square-o.svg') }}</span>
                </h3>
            </th>
        </tr>
    </thead>

    <tbody id=\"trace-text-{{ index }}\">
        <tr>
            <td>
                {% if exception.trace|length %}
                <pre class=\"stacktrace\">
                {%- filter escape('html') -%}
                    {{- include('@Twig/Exception/traces.txt.twig', { exception: exception, format: 'html' }, with_context = false) }}
                {% endfilter %}
                </pre>
                {% endif %}
            </td>
        </tr>
    </tbody>
</table>
", "TwigBundle:Exception:traces_text.html.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/traces_text.html.twig");
    }
}
