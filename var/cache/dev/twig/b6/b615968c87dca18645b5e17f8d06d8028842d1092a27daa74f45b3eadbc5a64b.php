<?php

/* FOSUserBundle:Group:list.html.twig */
class __TwigTemplate_e1b9881db95d796a5625a07b27689da09f88866324c9292827b0465f8d58b438 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_984a4fb85fbc76bd90b2927913ec6b9ad06f214c6daa0b9a42729d36c8201baa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_984a4fb85fbc76bd90b2927913ec6b9ad06f214c6daa0b9a42729d36c8201baa->enter($__internal_984a4fb85fbc76bd90b2927913ec6b9ad06f214c6daa0b9a42729d36c8201baa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $__internal_78b9cad0ce7d496f56824a1a6cc9990294de948d8a0004c5e339e274d299516b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_78b9cad0ce7d496f56824a1a6cc9990294de948d8a0004c5e339e274d299516b->enter($__internal_78b9cad0ce7d496f56824a1a6cc9990294de948d8a0004c5e339e274d299516b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_984a4fb85fbc76bd90b2927913ec6b9ad06f214c6daa0b9a42729d36c8201baa->leave($__internal_984a4fb85fbc76bd90b2927913ec6b9ad06f214c6daa0b9a42729d36c8201baa_prof);

        
        $__internal_78b9cad0ce7d496f56824a1a6cc9990294de948d8a0004c5e339e274d299516b->leave($__internal_78b9cad0ce7d496f56824a1a6cc9990294de948d8a0004c5e339e274d299516b_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_9879de8f8e1e47e338ccfff6b646cd311f3785869e78dbc633cf9a35f1460930 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9879de8f8e1e47e338ccfff6b646cd311f3785869e78dbc633cf9a35f1460930->enter($__internal_9879de8f8e1e47e338ccfff6b646cd311f3785869e78dbc633cf9a35f1460930_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_f71710cc0863baf6b92c917b1e935c2454850e2609dd083e4b4b60611dfcf8d7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f71710cc0863baf6b92c917b1e935c2454850e2609dd083e4b4b60611dfcf8d7->enter($__internal_f71710cc0863baf6b92c917b1e935c2454850e2609dd083e4b4b60611dfcf8d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/list_content.html.twig", "FOSUserBundle:Group:list.html.twig", 4)->display($context);
        
        $__internal_f71710cc0863baf6b92c917b1e935c2454850e2609dd083e4b4b60611dfcf8d7->leave($__internal_f71710cc0863baf6b92c917b1e935c2454850e2609dd083e4b4b60611dfcf8d7_prof);

        
        $__internal_9879de8f8e1e47e338ccfff6b646cd311f3785869e78dbc633cf9a35f1460930->leave($__internal_9879de8f8e1e47e338ccfff6b646cd311f3785869e78dbc633cf9a35f1460930_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/list_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:list.html.twig", "/home/timur/http/hw/hw64/vendor/friendsofsymfony/user-bundle/Resources/views/Group/list.html.twig");
    }
}
