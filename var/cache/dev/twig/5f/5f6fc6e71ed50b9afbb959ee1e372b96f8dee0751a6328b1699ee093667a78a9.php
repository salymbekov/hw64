<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_08db1a868eedeeaddb4962ac6f199d555adeafd0fd8c78eed7f1afe7288d2ac8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b3d13c3700dd17e06642d5272e47a8e5fb4024fc3b949987c5e539fbbb48588b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b3d13c3700dd17e06642d5272e47a8e5fb4024fc3b949987c5e539fbbb48588b->enter($__internal_b3d13c3700dd17e06642d5272e47a8e5fb4024fc3b949987c5e539fbbb48588b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        $__internal_95c1026be1fb96e7c9097faf281222645f907a711c93a1ed825dbe92273091cd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_95c1026be1fb96e7c9097faf281222645f907a711c93a1ed825dbe92273091cd->enter($__internal_95c1026be1fb96e7c9097faf281222645f907a711c93a1ed825dbe92273091cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_b3d13c3700dd17e06642d5272e47a8e5fb4024fc3b949987c5e539fbbb48588b->leave($__internal_b3d13c3700dd17e06642d5272e47a8e5fb4024fc3b949987c5e539fbbb48588b_prof);

        
        $__internal_95c1026be1fb96e7c9097faf281222645f907a711c93a1ed825dbe92273091cd->leave($__internal_95c1026be1fb96e7c9097faf281222645f907a711c93a1ed825dbe92273091cd_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/reset_widget.html.php");
    }
}
