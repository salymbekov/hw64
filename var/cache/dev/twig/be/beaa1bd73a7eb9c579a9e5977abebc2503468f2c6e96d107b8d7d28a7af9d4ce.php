<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_cca7d5871f891537cf95384c469a415d04fa82750633c68ed073ddfb6581eb43 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5c683538a7885314abe2f367fb6b82648c00ef02827ddd24213317d89e3fe614 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5c683538a7885314abe2f367fb6b82648c00ef02827ddd24213317d89e3fe614->enter($__internal_5c683538a7885314abe2f367fb6b82648c00ef02827ddd24213317d89e3fe614_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        $__internal_f41fbca9b7ab222810a226e3439558c5dd698655ee06075ecd6d89a4a170d52e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f41fbca9b7ab222810a226e3439558c5dd698655ee06075ecd6d89a4a170d52e->enter($__internal_f41fbca9b7ab222810a226e3439558c5dd698655ee06075ecd6d89a4a170d52e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_5c683538a7885314abe2f367fb6b82648c00ef02827ddd24213317d89e3fe614->leave($__internal_5c683538a7885314abe2f367fb6b82648c00ef02827ddd24213317d89e3fe614_prof);

        
        $__internal_f41fbca9b7ab222810a226e3439558c5dd698655ee06075ecd6d89a4a170d52e->leave($__internal_f41fbca9b7ab222810a226e3439558c5dd698655ee06075ecd6d89a4a170d52e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
", "@Framework/Form/collection_widget.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/collection_widget.html.php");
    }
}
