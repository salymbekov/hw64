<?php

/* FOSUserBundle:Registration:check_email.html.twig */
class __TwigTemplate_f54987f5000c074c27a56207ba4398413ee0e47d73e20dec6c821c4cbe5d928b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_48881e6d46a503504d7a27b466ef1bd414c2a510330057ba6b5ba9442dbb1b5a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_48881e6d46a503504d7a27b466ef1bd414c2a510330057ba6b5ba9442dbb1b5a->enter($__internal_48881e6d46a503504d7a27b466ef1bd414c2a510330057ba6b5ba9442dbb1b5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $__internal_bcff83af0f810baef47eaca705ae393672c88b70b2b1f69ce05a13b6988172d1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bcff83af0f810baef47eaca705ae393672c88b70b2b1f69ce05a13b6988172d1->enter($__internal_bcff83af0f810baef47eaca705ae393672c88b70b2b1f69ce05a13b6988172d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_48881e6d46a503504d7a27b466ef1bd414c2a510330057ba6b5ba9442dbb1b5a->leave($__internal_48881e6d46a503504d7a27b466ef1bd414c2a510330057ba6b5ba9442dbb1b5a_prof);

        
        $__internal_bcff83af0f810baef47eaca705ae393672c88b70b2b1f69ce05a13b6988172d1->leave($__internal_bcff83af0f810baef47eaca705ae393672c88b70b2b1f69ce05a13b6988172d1_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_24057dc258e124212d0702f6d177edbd41d8ec821d4f5856687388977c40b687 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_24057dc258e124212d0702f6d177edbd41d8ec821d4f5856687388977c40b687->enter($__internal_24057dc258e124212d0702f6d177edbd41d8ec821d4f5856687388977c40b687_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_579467c42b1d0d8216a3aabf317ecc94b799434bf7bed54a87b775de1f325b30 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_579467c42b1d0d8216a3aabf317ecc94b799434bf7bed54a87b775de1f325b30->enter($__internal_579467c42b1d0d8216a3aabf317ecc94b799434bf7bed54a87b775de1f325b30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.check_email", array("%email%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_579467c42b1d0d8216a3aabf317ecc94b799434bf7bed54a87b775de1f325b30->leave($__internal_579467c42b1d0d8216a3aabf317ecc94b799434bf7bed54a87b775de1f325b30_prof);

        
        $__internal_24057dc258e124212d0702f6d177edbd41d8ec821d4f5856687388977c40b687->leave($__internal_24057dc258e124212d0702f6d177edbd41d8ec821d4f5856687388977c40b687_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:check_email.html.twig", "/home/timur/http/hw/hw64/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/check_email.html.twig");
    }
}
