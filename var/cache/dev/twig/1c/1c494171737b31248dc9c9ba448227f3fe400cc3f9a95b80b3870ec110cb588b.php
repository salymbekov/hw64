<?php

/* base.html.twig */
class __TwigTemplate_b1cd8acd56267e8dc78034a7c64a8d118c952834e0ce91f29194d2c9c3e5257a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6034d04b0b2f273d618368aa7fe87e703943b35bf775d903644d090d07d65a4c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6034d04b0b2f273d618368aa7fe87e703943b35bf775d903644d090d07d65a4c->enter($__internal_6034d04b0b2f273d618368aa7fe87e703943b35bf775d903644d090d07d65a4c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_b12ca5f2da268ab0a1560e103a5fc6bb63c45d712d7d2e904e228de74370a694 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b12ca5f2da268ab0a1560e103a5fc6bb63c45d712d7d2e904e228de74370a694->enter($__internal_b12ca5f2da268ab0a1560e103a5fc6bb63c45d712d7d2e904e228de74370a694_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
    <div class=\"language\">
        <a href=\"";
        // line 11
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_words_index", array("_locale" => "en"));
        echo "\">en</a>
        <a href=\"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_words_index", array("_locale" => "ru"));
        echo "\">ru</a>
    </div>
        ";
        // line 14
        $this->displayBlock('body', $context, $blocks);
        // line 15
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 16
        echo "    </body>
</html>
";
        
        $__internal_6034d04b0b2f273d618368aa7fe87e703943b35bf775d903644d090d07d65a4c->leave($__internal_6034d04b0b2f273d618368aa7fe87e703943b35bf775d903644d090d07d65a4c_prof);

        
        $__internal_b12ca5f2da268ab0a1560e103a5fc6bb63c45d712d7d2e904e228de74370a694->leave($__internal_b12ca5f2da268ab0a1560e103a5fc6bb63c45d712d7d2e904e228de74370a694_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_92ab48dca15a5b12cb8a57adf7df9b66a39a47bda6c686c3da6f4bdaefdded11 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_92ab48dca15a5b12cb8a57adf7df9b66a39a47bda6c686c3da6f4bdaefdded11->enter($__internal_92ab48dca15a5b12cb8a57adf7df9b66a39a47bda6c686c3da6f4bdaefdded11_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_5da637d74869719f2443d7315e9d0ccb7ceee9da8eee5025277fdef2f244cd6e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5da637d74869719f2443d7315e9d0ccb7ceee9da8eee5025277fdef2f244cd6e->enter($__internal_5da637d74869719f2443d7315e9d0ccb7ceee9da8eee5025277fdef2f244cd6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_5da637d74869719f2443d7315e9d0ccb7ceee9da8eee5025277fdef2f244cd6e->leave($__internal_5da637d74869719f2443d7315e9d0ccb7ceee9da8eee5025277fdef2f244cd6e_prof);

        
        $__internal_92ab48dca15a5b12cb8a57adf7df9b66a39a47bda6c686c3da6f4bdaefdded11->leave($__internal_92ab48dca15a5b12cb8a57adf7df9b66a39a47bda6c686c3da6f4bdaefdded11_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_0d0cb7c5665b28f48ea24909c778586adaead4e270867ae09f0364687c51f5d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0d0cb7c5665b28f48ea24909c778586adaead4e270867ae09f0364687c51f5d4->enter($__internal_0d0cb7c5665b28f48ea24909c778586adaead4e270867ae09f0364687c51f5d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_d1f4e9dba44db5d791951b6a9ff748873c68d516ec7af6d8d78865dca1934434 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d1f4e9dba44db5d791951b6a9ff748873c68d516ec7af6d8d78865dca1934434->enter($__internal_d1f4e9dba44db5d791951b6a9ff748873c68d516ec7af6d8d78865dca1934434_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_d1f4e9dba44db5d791951b6a9ff748873c68d516ec7af6d8d78865dca1934434->leave($__internal_d1f4e9dba44db5d791951b6a9ff748873c68d516ec7af6d8d78865dca1934434_prof);

        
        $__internal_0d0cb7c5665b28f48ea24909c778586adaead4e270867ae09f0364687c51f5d4->leave($__internal_0d0cb7c5665b28f48ea24909c778586adaead4e270867ae09f0364687c51f5d4_prof);

    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        $__internal_ed384db0239ba97645e64459522d67ea65cc3836b85ac9c23b899b9326be8e63 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ed384db0239ba97645e64459522d67ea65cc3836b85ac9c23b899b9326be8e63->enter($__internal_ed384db0239ba97645e64459522d67ea65cc3836b85ac9c23b899b9326be8e63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_63ed354409884aace548812b86eaba1e44459bff89dd69f7a514f4b5039fdf0c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_63ed354409884aace548812b86eaba1e44459bff89dd69f7a514f4b5039fdf0c->enter($__internal_63ed354409884aace548812b86eaba1e44459bff89dd69f7a514f4b5039fdf0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_63ed354409884aace548812b86eaba1e44459bff89dd69f7a514f4b5039fdf0c->leave($__internal_63ed354409884aace548812b86eaba1e44459bff89dd69f7a514f4b5039fdf0c_prof);

        
        $__internal_ed384db0239ba97645e64459522d67ea65cc3836b85ac9c23b899b9326be8e63->leave($__internal_ed384db0239ba97645e64459522d67ea65cc3836b85ac9c23b899b9326be8e63_prof);

    }

    // line 15
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_fb6e52d512640b1db70e527bc0ac232ea58b399cce1a7b65bfca3fa30865fc0d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fb6e52d512640b1db70e527bc0ac232ea58b399cce1a7b65bfca3fa30865fc0d->enter($__internal_fb6e52d512640b1db70e527bc0ac232ea58b399cce1a7b65bfca3fa30865fc0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_3f32cfed3fe0af738623ea8869633d1be3c2a103cf1f7faaaad5d3bd2262727c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3f32cfed3fe0af738623ea8869633d1be3c2a103cf1f7faaaad5d3bd2262727c->enter($__internal_3f32cfed3fe0af738623ea8869633d1be3c2a103cf1f7faaaad5d3bd2262727c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_3f32cfed3fe0af738623ea8869633d1be3c2a103cf1f7faaaad5d3bd2262727c->leave($__internal_3f32cfed3fe0af738623ea8869633d1be3c2a103cf1f7faaaad5d3bd2262727c_prof);

        
        $__internal_fb6e52d512640b1db70e527bc0ac232ea58b399cce1a7b65bfca3fa30865fc0d->leave($__internal_fb6e52d512640b1db70e527bc0ac232ea58b399cce1a7b65bfca3fa30865fc0d_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 15,  110 => 14,  93 => 6,  75 => 5,  63 => 16,  60 => 15,  58 => 14,  53 => 12,  49 => 11,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
    <div class=\"language\">
        <a href=\"{{ path('app_words_index', {'_locale': 'en'}) }}\">en</a>
        <a href=\"{{ path('app_words_index', {'_locale': 'ru'}) }}\">ru</a>
    </div>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/home/timur/http/hw/hw64/app/Resources/views/base.html.twig");
    }
}
