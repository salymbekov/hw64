<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_4716ac0d1c0a031fae4eb22ba2d594e1ff4706d23b025ee94b448b3739a51de1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_30404826c3d45611d44a7256ae3e4f1286b9d21ecb81f42aaa2f12b5677251af = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_30404826c3d45611d44a7256ae3e4f1286b9d21ecb81f42aaa2f12b5677251af->enter($__internal_30404826c3d45611d44a7256ae3e4f1286b9d21ecb81f42aaa2f12b5677251af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $__internal_952dca01de421b1d2dc75ba11effea18ea79cc9030f2b418903208fcca1aece7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_952dca01de421b1d2dc75ba11effea18ea79cc9030f2b418903208fcca1aece7->enter($__internal_952dca01de421b1d2dc75ba11effea18ea79cc9030f2b418903208fcca1aece7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_30404826c3d45611d44a7256ae3e4f1286b9d21ecb81f42aaa2f12b5677251af->leave($__internal_30404826c3d45611d44a7256ae3e4f1286b9d21ecb81f42aaa2f12b5677251af_prof);

        
        $__internal_952dca01de421b1d2dc75ba11effea18ea79cc9030f2b418903208fcca1aece7->leave($__internal_952dca01de421b1d2dc75ba11effea18ea79cc9030f2b418903208fcca1aece7_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_bc0b3a7bf409d3847e2b97843112c11e0fba3c471652352d79b76de415307060 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bc0b3a7bf409d3847e2b97843112c11e0fba3c471652352d79b76de415307060->enter($__internal_bc0b3a7bf409d3847e2b97843112c11e0fba3c471652352d79b76de415307060_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_52dafbb2a7dcb1b4b3f8b1e6a108efa2386babc8949bd3b27dee61bbd0ddc4f3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_52dafbb2a7dcb1b4b3f8b1e6a108efa2386babc8949bd3b27dee61bbd0ddc4f3->enter($__internal_52dafbb2a7dcb1b4b3f8b1e6a108efa2386babc8949bd3b27dee61bbd0ddc4f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_52dafbb2a7dcb1b4b3f8b1e6a108efa2386babc8949bd3b27dee61bbd0ddc4f3->leave($__internal_52dafbb2a7dcb1b4b3f8b1e6a108efa2386babc8949bd3b27dee61bbd0ddc4f3_prof);

        
        $__internal_bc0b3a7bf409d3847e2b97843112c11e0fba3c471652352d79b76de415307060->leave($__internal_bc0b3a7bf409d3847e2b97843112c11e0fba3c471652352d79b76de415307060_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_a2373d7cb01d8fe0a64d7cdb1fdb3896a6105e572861608bd40916396d638e4d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a2373d7cb01d8fe0a64d7cdb1fdb3896a6105e572861608bd40916396d638e4d->enter($__internal_a2373d7cb01d8fe0a64d7cdb1fdb3896a6105e572861608bd40916396d638e4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d79ee976e03b266c9445ce70283d5b5745f6c225285d6c93bf26e520c825029a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d79ee976e03b266c9445ce70283d5b5745f6c225285d6c93bf26e520c825029a->enter($__internal_d79ee976e03b266c9445ce70283d5b5745f6c225285d6c93bf26e520c825029a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_d79ee976e03b266c9445ce70283d5b5745f6c225285d6c93bf26e520c825029a->leave($__internal_d79ee976e03b266c9445ce70283d5b5745f6c225285d6c93bf26e520c825029a_prof);

        
        $__internal_a2373d7cb01d8fe0a64d7cdb1fdb3896a6105e572861608bd40916396d638e4d->leave($__internal_a2373d7cb01d8fe0a64d7cdb1fdb3896a6105e572861608bd40916396d638e4d_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
