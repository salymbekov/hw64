<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_c18afe596d77ef1fd1d19a28930b20558ef4d3ea74853b99504c91a88de22d78 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3d73361452c4c390cafe4e343980276bcea4a99d8c6767f6ac3c5d222eedc25f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3d73361452c4c390cafe4e343980276bcea4a99d8c6767f6ac3c5d222eedc25f->enter($__internal_3d73361452c4c390cafe4e343980276bcea4a99d8c6767f6ac3c5d222eedc25f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        $__internal_9d30d660bf39c5553b43c2059e032507c9658a5925dd4a3404d80eb0e06cdc6c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9d30d660bf39c5553b43c2059e032507c9658a5925dd4a3404d80eb0e06cdc6c->enter($__internal_9d30d660bf39c5553b43c2059e032507c9658a5925dd4a3404d80eb0e06cdc6c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_3d73361452c4c390cafe4e343980276bcea4a99d8c6767f6ac3c5d222eedc25f->leave($__internal_3d73361452c4c390cafe4e343980276bcea4a99d8c6767f6ac3c5d222eedc25f_prof);

        
        $__internal_9d30d660bf39c5553b43c2059e032507c9658a5925dd4a3404d80eb0e06cdc6c->leave($__internal_9d30d660bf39c5553b43c2059e032507c9658a5925dd4a3404d80eb0e06cdc6c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
", "@Framework/Form/choice_widget.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget.html.php");
    }
}
