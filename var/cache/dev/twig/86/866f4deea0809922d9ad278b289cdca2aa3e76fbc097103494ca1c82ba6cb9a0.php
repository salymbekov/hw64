<?php

/* foundation_5_layout.html.twig */
class __TwigTemplate_62bd0210f4060d7e39cdcde069d02b1f55d343addfd6d3849e0aa96bfabdce7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("form_div_layout.html.twig", "foundation_5_layout.html.twig", 1);
        $this->blocks = array(
            'form_widget_simple' => array($this, 'block_form_widget_simple'),
            'textarea_widget' => array($this, 'block_textarea_widget'),
            'button_widget' => array($this, 'block_button_widget'),
            'money_widget' => array($this, 'block_money_widget'),
            'percent_widget' => array($this, 'block_percent_widget'),
            'datetime_widget' => array($this, 'block_datetime_widget'),
            'date_widget' => array($this, 'block_date_widget'),
            'time_widget' => array($this, 'block_time_widget'),
            'choice_widget_collapsed' => array($this, 'block_choice_widget_collapsed'),
            'choice_widget_expanded' => array($this, 'block_choice_widget_expanded'),
            'checkbox_widget' => array($this, 'block_checkbox_widget'),
            'radio_widget' => array($this, 'block_radio_widget'),
            'form_label' => array($this, 'block_form_label'),
            'choice_label' => array($this, 'block_choice_label'),
            'checkbox_label' => array($this, 'block_checkbox_label'),
            'radio_label' => array($this, 'block_radio_label'),
            'checkbox_radio_label' => array($this, 'block_checkbox_radio_label'),
            'form_row' => array($this, 'block_form_row'),
            'choice_row' => array($this, 'block_choice_row'),
            'date_row' => array($this, 'block_date_row'),
            'time_row' => array($this, 'block_time_row'),
            'datetime_row' => array($this, 'block_datetime_row'),
            'checkbox_row' => array($this, 'block_checkbox_row'),
            'radio_row' => array($this, 'block_radio_row'),
            'form_errors' => array($this, 'block_form_errors'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "form_div_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7e1405d1028c7e4f4e3629dc5ed93b00536faa99a57a844c934f28bf59f61f3b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7e1405d1028c7e4f4e3629dc5ed93b00536faa99a57a844c934f28bf59f61f3b->enter($__internal_7e1405d1028c7e4f4e3629dc5ed93b00536faa99a57a844c934f28bf59f61f3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "foundation_5_layout.html.twig"));

        $__internal_900310e1ec96c5a3b3dfef081686c8ccd378f1d71f78f8adc0191220d5161abb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_900310e1ec96c5a3b3dfef081686c8ccd378f1d71f78f8adc0191220d5161abb->enter($__internal_900310e1ec96c5a3b3dfef081686c8ccd378f1d71f78f8adc0191220d5161abb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "foundation_5_layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7e1405d1028c7e4f4e3629dc5ed93b00536faa99a57a844c934f28bf59f61f3b->leave($__internal_7e1405d1028c7e4f4e3629dc5ed93b00536faa99a57a844c934f28bf59f61f3b_prof);

        
        $__internal_900310e1ec96c5a3b3dfef081686c8ccd378f1d71f78f8adc0191220d5161abb->leave($__internal_900310e1ec96c5a3b3dfef081686c8ccd378f1d71f78f8adc0191220d5161abb_prof);

    }

    // line 6
    public function block_form_widget_simple($context, array $blocks = array())
    {
        $__internal_597f0a9d71ba60034cef5f38ba8236c7b977ef679a568b0f46a63fad1d314b6e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_597f0a9d71ba60034cef5f38ba8236c7b977ef679a568b0f46a63fad1d314b6e->enter($__internal_597f0a9d71ba60034cef5f38ba8236c7b977ef679a568b0f46a63fad1d314b6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        $__internal_9fa6b52c523a6778498854a304debb3b302471a26649cd1de71faf283e66fe9a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9fa6b52c523a6778498854a304debb3b302471a26649cd1de71faf283e66fe9a->enter($__internal_9fa6b52c523a6778498854a304debb3b302471a26649cd1de71faf283e66fe9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        // line 7
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 8
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
            // line 9
            echo "    ";
        }
        // line 10
        $this->displayParentBlock("form_widget_simple", $context, $blocks);
        
        $__internal_9fa6b52c523a6778498854a304debb3b302471a26649cd1de71faf283e66fe9a->leave($__internal_9fa6b52c523a6778498854a304debb3b302471a26649cd1de71faf283e66fe9a_prof);

        
        $__internal_597f0a9d71ba60034cef5f38ba8236c7b977ef679a568b0f46a63fad1d314b6e->leave($__internal_597f0a9d71ba60034cef5f38ba8236c7b977ef679a568b0f46a63fad1d314b6e_prof);

    }

    // line 13
    public function block_textarea_widget($context, array $blocks = array())
    {
        $__internal_26b537518b84c7c218fae9f437067040b402ac4f4eb70e72eb6dfff8be1a1465 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_26b537518b84c7c218fae9f437067040b402ac4f4eb70e72eb6dfff8be1a1465->enter($__internal_26b537518b84c7c218fae9f437067040b402ac4f4eb70e72eb6dfff8be1a1465_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "textarea_widget"));

        $__internal_356f85c4952bba165d8c9e4746a88c9a60cdc9b64e6516a5383ba9de393ca970 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_356f85c4952bba165d8c9e4746a88c9a60cdc9b64e6516a5383ba9de393ca970->enter($__internal_356f85c4952bba165d8c9e4746a88c9a60cdc9b64e6516a5383ba9de393ca970_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "textarea_widget"));

        // line 14
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 15
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
            // line 16
            echo "    ";
        }
        // line 17
        $this->displayParentBlock("textarea_widget", $context, $blocks);
        
        $__internal_356f85c4952bba165d8c9e4746a88c9a60cdc9b64e6516a5383ba9de393ca970->leave($__internal_356f85c4952bba165d8c9e4746a88c9a60cdc9b64e6516a5383ba9de393ca970_prof);

        
        $__internal_26b537518b84c7c218fae9f437067040b402ac4f4eb70e72eb6dfff8be1a1465->leave($__internal_26b537518b84c7c218fae9f437067040b402ac4f4eb70e72eb6dfff8be1a1465_prof);

    }

    // line 20
    public function block_button_widget($context, array $blocks = array())
    {
        $__internal_a958c32af722c6370cab7cb13b854c93c888b3f6b522dac8616024fdb1689cce = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a958c32af722c6370cab7cb13b854c93c888b3f6b522dac8616024fdb1689cce->enter($__internal_a958c32af722c6370cab7cb13b854c93c888b3f6b522dac8616024fdb1689cce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        $__internal_70785f5c591ccd56d05a028d303b17e28ba03ec2623b0a1053bbfc94a1e2e110 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_70785f5c591ccd56d05a028d303b17e28ba03ec2623b0a1053bbfc94a1e2e110->enter($__internal_70785f5c591ccd56d05a028d303b17e28ba03ec2623b0a1053bbfc94a1e2e110_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        // line 21
        $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " button"))));
        // line 22
        $this->displayParentBlock("button_widget", $context, $blocks);
        
        $__internal_70785f5c591ccd56d05a028d303b17e28ba03ec2623b0a1053bbfc94a1e2e110->leave($__internal_70785f5c591ccd56d05a028d303b17e28ba03ec2623b0a1053bbfc94a1e2e110_prof);

        
        $__internal_a958c32af722c6370cab7cb13b854c93c888b3f6b522dac8616024fdb1689cce->leave($__internal_a958c32af722c6370cab7cb13b854c93c888b3f6b522dac8616024fdb1689cce_prof);

    }

    // line 25
    public function block_money_widget($context, array $blocks = array())
    {
        $__internal_72087087f9359eece67079debcb857c970ca96f0c58d37a3953c36288b60dc72 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_72087087f9359eece67079debcb857c970ca96f0c58d37a3953c36288b60dc72->enter($__internal_72087087f9359eece67079debcb857c970ca96f0c58d37a3953c36288b60dc72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        $__internal_0c3fb473e45805c4ed86ec2de78f762db882bb00959e0235912e247fa5574b56 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0c3fb473e45805c4ed86ec2de78f762db882bb00959e0235912e247fa5574b56->enter($__internal_0c3fb473e45805c4ed86ec2de78f762db882bb00959e0235912e247fa5574b56_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        // line 26
        echo "<div class=\"row collapse\">
        ";
        // line 27
        $context["prepend"] = ("{{" == twig_slice($this->env, ($context["money_pattern"] ?? $this->getContext($context, "money_pattern")), 0, 2));
        // line 28
        echo "        ";
        if ( !($context["prepend"] ?? $this->getContext($context, "prepend"))) {
            // line 29
            echo "            <div class=\"small-3 large-2 columns\">
                <span class=\"prefix\">";
            // line 30
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->encodeCurrency($this->env, ($context["money_pattern"] ?? $this->getContext($context, "money_pattern")));
            echo "</span>
            </div>
        ";
        }
        // line 33
        echo "        <div class=\"small-9 large-10 columns\">";
        // line 34
        $this->displayBlock("form_widget_simple", $context, $blocks);
        // line 35
        echo "</div>
        ";
        // line 36
        if (($context["prepend"] ?? $this->getContext($context, "prepend"))) {
            // line 37
            echo "            <div class=\"small-3 large-2 columns\">
                <span class=\"postfix\">";
            // line 38
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->encodeCurrency($this->env, ($context["money_pattern"] ?? $this->getContext($context, "money_pattern")));
            echo "</span>
            </div>
        ";
        }
        // line 41
        echo "    </div>";
        
        $__internal_0c3fb473e45805c4ed86ec2de78f762db882bb00959e0235912e247fa5574b56->leave($__internal_0c3fb473e45805c4ed86ec2de78f762db882bb00959e0235912e247fa5574b56_prof);

        
        $__internal_72087087f9359eece67079debcb857c970ca96f0c58d37a3953c36288b60dc72->leave($__internal_72087087f9359eece67079debcb857c970ca96f0c58d37a3953c36288b60dc72_prof);

    }

    // line 44
    public function block_percent_widget($context, array $blocks = array())
    {
        $__internal_ab8a77a49baff884acc01fec5891f580f5601912c8fa4c6b305a115646dcb7e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ab8a77a49baff884acc01fec5891f580f5601912c8fa4c6b305a115646dcb7e4->enter($__internal_ab8a77a49baff884acc01fec5891f580f5601912c8fa4c6b305a115646dcb7e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        $__internal_4377655c9623aa9276acf9a2763da4429880fd65962f56624952a8f9c9de3a67 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4377655c9623aa9276acf9a2763da4429880fd65962f56624952a8f9c9de3a67->enter($__internal_4377655c9623aa9276acf9a2763da4429880fd65962f56624952a8f9c9de3a67_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        // line 45
        echo "<div class=\"row collapse\">
        <div class=\"small-9 large-10 columns\">";
        // line 47
        $this->displayBlock("form_widget_simple", $context, $blocks);
        // line 48
        echo "</div>
        <div class=\"small-3 large-2 columns\">
            <span class=\"postfix\">%</span>
        </div>
    </div>";
        
        $__internal_4377655c9623aa9276acf9a2763da4429880fd65962f56624952a8f9c9de3a67->leave($__internal_4377655c9623aa9276acf9a2763da4429880fd65962f56624952a8f9c9de3a67_prof);

        
        $__internal_ab8a77a49baff884acc01fec5891f580f5601912c8fa4c6b305a115646dcb7e4->leave($__internal_ab8a77a49baff884acc01fec5891f580f5601912c8fa4c6b305a115646dcb7e4_prof);

    }

    // line 55
    public function block_datetime_widget($context, array $blocks = array())
    {
        $__internal_de48a9d220271388512873116d1f0f0fff505f3615cc5e8f3031305f93f3411c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_de48a9d220271388512873116d1f0f0fff505f3615cc5e8f3031305f93f3411c->enter($__internal_de48a9d220271388512873116d1f0f0fff505f3615cc5e8f3031305f93f3411c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        $__internal_ed9a965408c1a77be7256fb1e2a2d214fcf313b903d06de37c6f7e9bc89b1a07 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ed9a965408c1a77be7256fb1e2a2d214fcf313b903d06de37c6f7e9bc89b1a07->enter($__internal_ed9a965408c1a77be7256fb1e2a2d214fcf313b903d06de37c6f7e9bc89b1a07_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        // line 56
        if ((($context["widget"] ?? $this->getContext($context, "widget")) == "single_text")) {
            // line 57
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 59
            echo "        ";
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " row"))));
            // line 60
            echo "        <div class=\"row\">
            <div class=\"large-7 columns\">";
            // line 61
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "date", array()), 'errors');
            echo "</div>
            <div class=\"large-5 columns\">";
            // line 62
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "time", array()), 'errors');
            echo "</div>
        </div>
        <div ";
            // line 64
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            <div class=\"large-7 columns\">";
            // line 65
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "date", array()), 'widget', array("datetime" => true));
            echo "</div>
            <div class=\"large-5 columns\">";
            // line 66
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "time", array()), 'widget', array("datetime" => true));
            echo "</div>
        </div>
    ";
        }
        
        $__internal_ed9a965408c1a77be7256fb1e2a2d214fcf313b903d06de37c6f7e9bc89b1a07->leave($__internal_ed9a965408c1a77be7256fb1e2a2d214fcf313b903d06de37c6f7e9bc89b1a07_prof);

        
        $__internal_de48a9d220271388512873116d1f0f0fff505f3615cc5e8f3031305f93f3411c->leave($__internal_de48a9d220271388512873116d1f0f0fff505f3615cc5e8f3031305f93f3411c_prof);

    }

    // line 71
    public function block_date_widget($context, array $blocks = array())
    {
        $__internal_8bf143a1be0cec9125ce8a5240d62f330fa4673ddbdf1059a021bb5f98084386 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8bf143a1be0cec9125ce8a5240d62f330fa4673ddbdf1059a021bb5f98084386->enter($__internal_8bf143a1be0cec9125ce8a5240d62f330fa4673ddbdf1059a021bb5f98084386_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        $__internal_4b60df9eff10ceead49d6285b91aed44572b1d8fa5478a2e2a25f66a47822b8d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4b60df9eff10ceead49d6285b91aed44572b1d8fa5478a2e2a25f66a47822b8d->enter($__internal_4b60df9eff10ceead49d6285b91aed44572b1d8fa5478a2e2a25f66a47822b8d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        // line 72
        if ((($context["widget"] ?? $this->getContext($context, "widget")) == "single_text")) {
            // line 73
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 75
            echo "        ";
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " row"))));
            // line 76
            echo "        ";
            if (( !array_key_exists("datetime", $context) ||  !($context["datetime"] ?? $this->getContext($context, "datetime")))) {
                // line 77
                echo "            <div ";
                $this->displayBlock("widget_container_attributes", $context, $blocks);
                echo ">
        ";
            }
            // line 79
            echo twig_replace_filter(($context["date_pattern"] ?? $this->getContext($context, "date_pattern")), array("{{ year }}" => (("<div class=\"large-4 columns\">" .             // line 80
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "year", array()), 'widget')) . "</div>"), "{{ month }}" => (("<div class=\"large-4 columns\">" .             // line 81
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "month", array()), 'widget')) . "</div>"), "{{ day }}" => (("<div class=\"large-4 columns\">" .             // line 82
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "day", array()), 'widget')) . "</div>")));
            // line 84
            if (( !array_key_exists("datetime", $context) ||  !($context["datetime"] ?? $this->getContext($context, "datetime")))) {
                // line 85
                echo "            </div>
        ";
            }
            // line 87
            echo "    ";
        }
        
        $__internal_4b60df9eff10ceead49d6285b91aed44572b1d8fa5478a2e2a25f66a47822b8d->leave($__internal_4b60df9eff10ceead49d6285b91aed44572b1d8fa5478a2e2a25f66a47822b8d_prof);

        
        $__internal_8bf143a1be0cec9125ce8a5240d62f330fa4673ddbdf1059a021bb5f98084386->leave($__internal_8bf143a1be0cec9125ce8a5240d62f330fa4673ddbdf1059a021bb5f98084386_prof);

    }

    // line 90
    public function block_time_widget($context, array $blocks = array())
    {
        $__internal_09634cc52491cc848ccbbb4b80f7862d78d54ed83f570625ac3a3ea95911d815 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_09634cc52491cc848ccbbb4b80f7862d78d54ed83f570625ac3a3ea95911d815->enter($__internal_09634cc52491cc848ccbbb4b80f7862d78d54ed83f570625ac3a3ea95911d815_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        $__internal_7b100a3547a7ca66916304f69ff275e0513c71f69a23be6fb3f890fe8c330152 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7b100a3547a7ca66916304f69ff275e0513c71f69a23be6fb3f890fe8c330152->enter($__internal_7b100a3547a7ca66916304f69ff275e0513c71f69a23be6fb3f890fe8c330152_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        // line 91
        if ((($context["widget"] ?? $this->getContext($context, "widget")) == "single_text")) {
            // line 92
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 94
            echo "        ";
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " row"))));
            // line 95
            echo "        ";
            if (( !array_key_exists("datetime", $context) || (false == ($context["datetime"] ?? $this->getContext($context, "datetime"))))) {
                // line 96
                echo "            <div ";
                $this->displayBlock("widget_container_attributes", $context, $blocks);
                echo ">
        ";
            }
            // line 98
            echo "        ";
            if (($context["with_seconds"] ?? $this->getContext($context, "with_seconds"))) {
                // line 99
                echo "            <div class=\"large-4 columns\">";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "hour", array()), 'widget');
                echo "</div>
            <div class=\"large-4 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        ";
                // line 106
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "minute", array()), 'widget');
                echo "
                    </div>
                </div>
            </div>
            <div class=\"large-4 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        ";
                // line 116
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "second", array()), 'widget');
                echo "
                    </div>
                </div>
            </div>
        ";
            } else {
                // line 121
                echo "            <div class=\"large-6 columns\">";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "hour", array()), 'widget');
                echo "</div>
            <div class=\"large-6 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        ";
                // line 128
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "minute", array()), 'widget');
                echo "
                    </div>
                </div>
            </div>
        ";
            }
            // line 133
            echo "        ";
            if (( !array_key_exists("datetime", $context) || (false == ($context["datetime"] ?? $this->getContext($context, "datetime"))))) {
                // line 134
                echo "            </div>
        ";
            }
            // line 136
            echo "    ";
        }
        
        $__internal_7b100a3547a7ca66916304f69ff275e0513c71f69a23be6fb3f890fe8c330152->leave($__internal_7b100a3547a7ca66916304f69ff275e0513c71f69a23be6fb3f890fe8c330152_prof);

        
        $__internal_09634cc52491cc848ccbbb4b80f7862d78d54ed83f570625ac3a3ea95911d815->leave($__internal_09634cc52491cc848ccbbb4b80f7862d78d54ed83f570625ac3a3ea95911d815_prof);

    }

    // line 139
    public function block_choice_widget_collapsed($context, array $blocks = array())
    {
        $__internal_9cef8c23eb2573470d0ea8c8fbe0b84bd2f263010fdd854a3a40249f4c1debd4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9cef8c23eb2573470d0ea8c8fbe0b84bd2f263010fdd854a3a40249f4c1debd4->enter($__internal_9cef8c23eb2573470d0ea8c8fbe0b84bd2f263010fdd854a3a40249f4c1debd4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_collapsed"));

        $__internal_11dd8e8479118ac5b3c68ab7c9d3d59e03c8c3e64b8a0f6ea15d9ecf29a6c0d5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_11dd8e8479118ac5b3c68ab7c9d3d59e03c8c3e64b8a0f6ea15d9ecf29a6c0d5->enter($__internal_11dd8e8479118ac5b3c68ab7c9d3d59e03c8c3e64b8a0f6ea15d9ecf29a6c0d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_collapsed"));

        // line 140
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 141
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
            // line 142
            echo "    ";
        }
        // line 143
        echo "
    ";
        // line 144
        if (($context["multiple"] ?? $this->getContext($context, "multiple"))) {
            // line 145
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("style" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "style", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "style", array()), "")) : ("")) . " height: auto; background-image: none;"))));
            // line 146
            echo "    ";
        }
        // line 147
        echo "
    ";
        // line 148
        if ((((($context["required"] ?? $this->getContext($context, "required")) && (null === ($context["placeholder"] ?? $this->getContext($context, "placeholder")))) &&  !($context["placeholder_in_choices"] ?? $this->getContext($context, "placeholder_in_choices"))) &&  !($context["multiple"] ?? $this->getContext($context, "multiple")))) {
            // line 149
            $context["required"] = false;
        }
        // line 151
        echo "<select ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if (($context["multiple"] ?? $this->getContext($context, "multiple"))) {
            echo " multiple=\"multiple\" data-customforms=\"disabled\"";
        }
        echo ">
        ";
        // line 152
        if ( !(null === ($context["placeholder"] ?? $this->getContext($context, "placeholder")))) {
            // line 153
            echo "<option value=\"\"";
            if ((($context["required"] ?? $this->getContext($context, "required")) && twig_test_empty(($context["value"] ?? $this->getContext($context, "value"))))) {
                echo " selected=\"selected\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, (((($context["translation_domain"] ?? $this->getContext($context, "translation_domain")) === false)) ? (($context["placeholder"] ?? $this->getContext($context, "placeholder"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(($context["placeholder"] ?? $this->getContext($context, "placeholder")), array(), ($context["translation_domain"] ?? $this->getContext($context, "translation_domain"))))), "html", null, true);
            echo "</option>";
        }
        // line 155
        if ((twig_length_filter($this->env, ($context["preferred_choices"] ?? $this->getContext($context, "preferred_choices"))) > 0)) {
            // line 156
            $context["options"] = ($context["preferred_choices"] ?? $this->getContext($context, "preferred_choices"));
            // line 157
            $this->displayBlock("choice_widget_options", $context, $blocks);
            // line 158
            if (((twig_length_filter($this->env, ($context["choices"] ?? $this->getContext($context, "choices"))) > 0) &&  !(null === ($context["separator"] ?? $this->getContext($context, "separator"))))) {
                // line 159
                echo "<option disabled=\"disabled\">";
                echo twig_escape_filter($this->env, ($context["separator"] ?? $this->getContext($context, "separator")), "html", null, true);
                echo "</option>";
            }
        }
        // line 162
        $context["options"] = ($context["choices"] ?? $this->getContext($context, "choices"));
        // line 163
        $this->displayBlock("choice_widget_options", $context, $blocks);
        // line 164
        echo "</select>";
        
        $__internal_11dd8e8479118ac5b3c68ab7c9d3d59e03c8c3e64b8a0f6ea15d9ecf29a6c0d5->leave($__internal_11dd8e8479118ac5b3c68ab7c9d3d59e03c8c3e64b8a0f6ea15d9ecf29a6c0d5_prof);

        
        $__internal_9cef8c23eb2573470d0ea8c8fbe0b84bd2f263010fdd854a3a40249f4c1debd4->leave($__internal_9cef8c23eb2573470d0ea8c8fbe0b84bd2f263010fdd854a3a40249f4c1debd4_prof);

    }

    // line 167
    public function block_choice_widget_expanded($context, array $blocks = array())
    {
        $__internal_03bb0e1ffc6193651493e6e8790617af319599633dbe67f9ba06f4f142f1840a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_03bb0e1ffc6193651493e6e8790617af319599633dbe67f9ba06f4f142f1840a->enter($__internal_03bb0e1ffc6193651493e6e8790617af319599633dbe67f9ba06f4f142f1840a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        $__internal_916bb14b4cbd01dd81fe8a7bfec1fa31a781d953994f04358704b4d05bf72b61 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_916bb14b4cbd01dd81fe8a7bfec1fa31a781d953994f04358704b4d05bf72b61->enter($__internal_916bb14b4cbd01dd81fe8a7bfec1fa31a781d953994f04358704b4d05bf72b61_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        // line 168
        if (twig_in_filter("-inline", (($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")))) {
            // line 169
            echo "        <ul class=\"inline-list\">
            ";
            // line 170
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 171
                echo "                <li>";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 172
($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : (""))));
                // line 173
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 175
            echo "        </ul>
    ";
        } else {
            // line 177
            echo "        <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            ";
            // line 178
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 179
                echo "                ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 180
($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : (""))));
                // line 181
                echo "
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 183
            echo "        </div>
    ";
        }
        
        $__internal_916bb14b4cbd01dd81fe8a7bfec1fa31a781d953994f04358704b4d05bf72b61->leave($__internal_916bb14b4cbd01dd81fe8a7bfec1fa31a781d953994f04358704b4d05bf72b61_prof);

        
        $__internal_03bb0e1ffc6193651493e6e8790617af319599633dbe67f9ba06f4f142f1840a->leave($__internal_03bb0e1ffc6193651493e6e8790617af319599633dbe67f9ba06f4f142f1840a_prof);

    }

    // line 187
    public function block_checkbox_widget($context, array $blocks = array())
    {
        $__internal_659a0d75830625a9116fd8b963b44bfa89abadf19be2e24906d563dad9ccf665 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_659a0d75830625a9116fd8b963b44bfa89abadf19be2e24906d563dad9ccf665->enter($__internal_659a0d75830625a9116fd8b963b44bfa89abadf19be2e24906d563dad9ccf665_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        $__internal_46af3a8872363d7d9a4ed09eae83aa05a17c52ea3602ea31a6a63a09b6ab2616 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_46af3a8872363d7d9a4ed09eae83aa05a17c52ea3602ea31a6a63a09b6ab2616->enter($__internal_46af3a8872363d7d9a4ed09eae83aa05a17c52ea3602ea31a6a63a09b6ab2616_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        // line 188
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter(($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")), "")) : (""));
        // line 189
        echo "    ";
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 190
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
            // line 191
            echo "    ";
        }
        // line 192
        echo "    ";
        if (twig_in_filter("checkbox-inline", ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")))) {
            // line 193
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
            echo "
    ";
        } else {
            // line 195
            echo "        <div class=\"checkbox\">
            ";
            // line 196
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
            echo "
        </div>
    ";
        }
        
        $__internal_46af3a8872363d7d9a4ed09eae83aa05a17c52ea3602ea31a6a63a09b6ab2616->leave($__internal_46af3a8872363d7d9a4ed09eae83aa05a17c52ea3602ea31a6a63a09b6ab2616_prof);

        
        $__internal_659a0d75830625a9116fd8b963b44bfa89abadf19be2e24906d563dad9ccf665->leave($__internal_659a0d75830625a9116fd8b963b44bfa89abadf19be2e24906d563dad9ccf665_prof);

    }

    // line 201
    public function block_radio_widget($context, array $blocks = array())
    {
        $__internal_5e2a9e2376f0e7f2f355385b968b1da0bf30cf7cb71c36480d4bd6a0c00133ed = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5e2a9e2376f0e7f2f355385b968b1da0bf30cf7cb71c36480d4bd6a0c00133ed->enter($__internal_5e2a9e2376f0e7f2f355385b968b1da0bf30cf7cb71c36480d4bd6a0c00133ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        $__internal_dc5fb72ba25de3906423ae2b47cec256526587f0029533ca95c2a17ee6eb5f38 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dc5fb72ba25de3906423ae2b47cec256526587f0029533ca95c2a17ee6eb5f38->enter($__internal_dc5fb72ba25de3906423ae2b47cec256526587f0029533ca95c2a17ee6eb5f38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        // line 202
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter(($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")), "")) : (""));
        // line 203
        echo "    ";
        if (twig_in_filter("radio-inline", ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")))) {
            // line 204
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
            echo "
    ";
        } else {
            // line 206
            echo "        ";
            if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
                // line 207
                $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
                // line 208
                echo "        ";
            }
            // line 209
            echo "        <div class=\"radio\">
            ";
            // line 210
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
            echo "
        </div>
    ";
        }
        
        $__internal_dc5fb72ba25de3906423ae2b47cec256526587f0029533ca95c2a17ee6eb5f38->leave($__internal_dc5fb72ba25de3906423ae2b47cec256526587f0029533ca95c2a17ee6eb5f38_prof);

        
        $__internal_5e2a9e2376f0e7f2f355385b968b1da0bf30cf7cb71c36480d4bd6a0c00133ed->leave($__internal_5e2a9e2376f0e7f2f355385b968b1da0bf30cf7cb71c36480d4bd6a0c00133ed_prof);

    }

    // line 217
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_3c88c3a9c888a52fb902b1fec9547e3b06dd519170d0feaa2d48bfd2fa63f699 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3c88c3a9c888a52fb902b1fec9547e3b06dd519170d0feaa2d48bfd2fa63f699->enter($__internal_3c88c3a9c888a52fb902b1fec9547e3b06dd519170d0feaa2d48bfd2fa63f699_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_b1f07756cd8049136cb0f693b992b3198c4ad1924f3884f2cabf39e405367f25 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b1f07756cd8049136cb0f693b992b3198c4ad1924f3884f2cabf39e405367f25->enter($__internal_b1f07756cd8049136cb0f693b992b3198c4ad1924f3884f2cabf39e405367f25_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 218
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 219
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
            // line 220
            echo "    ";
        }
        // line 221
        $this->displayParentBlock("form_label", $context, $blocks);
        
        $__internal_b1f07756cd8049136cb0f693b992b3198c4ad1924f3884f2cabf39e405367f25->leave($__internal_b1f07756cd8049136cb0f693b992b3198c4ad1924f3884f2cabf39e405367f25_prof);

        
        $__internal_3c88c3a9c888a52fb902b1fec9547e3b06dd519170d0feaa2d48bfd2fa63f699->leave($__internal_3c88c3a9c888a52fb902b1fec9547e3b06dd519170d0feaa2d48bfd2fa63f699_prof);

    }

    // line 224
    public function block_choice_label($context, array $blocks = array())
    {
        $__internal_0a143d034e87193d36649a5b9eb2fb7b9c789250bb97ebab3f6fb1784a15f284 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0a143d034e87193d36649a5b9eb2fb7b9c789250bb97ebab3f6fb1784a15f284->enter($__internal_0a143d034e87193d36649a5b9eb2fb7b9c789250bb97ebab3f6fb1784a15f284_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_label"));

        $__internal_a8c030910de9428a52d8549553336538b95f7137710c18650ca9ad7b24abb645 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a8c030910de9428a52d8549553336538b95f7137710c18650ca9ad7b24abb645->enter($__internal_a8c030910de9428a52d8549553336538b95f7137710c18650ca9ad7b24abb645_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_label"));

        // line 225
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 226
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
            // line 227
            echo "    ";
        }
        // line 228
        echo "    ";
        // line 229
        echo "    ";
        $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(twig_replace_filter((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")), array("checkbox-inline" => "", "radio-inline" => "")))));
        // line 230
        $this->displayBlock("form_label", $context, $blocks);
        
        $__internal_a8c030910de9428a52d8549553336538b95f7137710c18650ca9ad7b24abb645->leave($__internal_a8c030910de9428a52d8549553336538b95f7137710c18650ca9ad7b24abb645_prof);

        
        $__internal_0a143d034e87193d36649a5b9eb2fb7b9c789250bb97ebab3f6fb1784a15f284->leave($__internal_0a143d034e87193d36649a5b9eb2fb7b9c789250bb97ebab3f6fb1784a15f284_prof);

    }

    // line 233
    public function block_checkbox_label($context, array $blocks = array())
    {
        $__internal_a324bbee13e559824b003142402f82b1b50948715ee40c56f88b47a013551f2c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a324bbee13e559824b003142402f82b1b50948715ee40c56f88b47a013551f2c->enter($__internal_a324bbee13e559824b003142402f82b1b50948715ee40c56f88b47a013551f2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_label"));

        $__internal_1011ba4966610b419da64fd6e5b7c8211b073c62c9e79fecd8a653f9920bd373 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1011ba4966610b419da64fd6e5b7c8211b073c62c9e79fecd8a653f9920bd373->enter($__internal_1011ba4966610b419da64fd6e5b7c8211b073c62c9e79fecd8a653f9920bd373_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_label"));

        // line 234
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_1011ba4966610b419da64fd6e5b7c8211b073c62c9e79fecd8a653f9920bd373->leave($__internal_1011ba4966610b419da64fd6e5b7c8211b073c62c9e79fecd8a653f9920bd373_prof);

        
        $__internal_a324bbee13e559824b003142402f82b1b50948715ee40c56f88b47a013551f2c->leave($__internal_a324bbee13e559824b003142402f82b1b50948715ee40c56f88b47a013551f2c_prof);

    }

    // line 237
    public function block_radio_label($context, array $blocks = array())
    {
        $__internal_ad0ec692d25f428e85620c1ba034e65daa22daa3b56d5b277a3cf552dc34457b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ad0ec692d25f428e85620c1ba034e65daa22daa3b56d5b277a3cf552dc34457b->enter($__internal_ad0ec692d25f428e85620c1ba034e65daa22daa3b56d5b277a3cf552dc34457b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_label"));

        $__internal_2d043bf3cf2e980576c5560c02e5ffaa5181b7669488136f692931a1f10cbddb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2d043bf3cf2e980576c5560c02e5ffaa5181b7669488136f692931a1f10cbddb->enter($__internal_2d043bf3cf2e980576c5560c02e5ffaa5181b7669488136f692931a1f10cbddb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_label"));

        // line 238
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_2d043bf3cf2e980576c5560c02e5ffaa5181b7669488136f692931a1f10cbddb->leave($__internal_2d043bf3cf2e980576c5560c02e5ffaa5181b7669488136f692931a1f10cbddb_prof);

        
        $__internal_ad0ec692d25f428e85620c1ba034e65daa22daa3b56d5b277a3cf552dc34457b->leave($__internal_ad0ec692d25f428e85620c1ba034e65daa22daa3b56d5b277a3cf552dc34457b_prof);

    }

    // line 241
    public function block_checkbox_radio_label($context, array $blocks = array())
    {
        $__internal_d413284ecc072b8e601d00a0ac62c64ab1e93d4987e74526db6644ec0271fb21 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d413284ecc072b8e601d00a0ac62c64ab1e93d4987e74526db6644ec0271fb21->enter($__internal_d413284ecc072b8e601d00a0ac62c64ab1e93d4987e74526db6644ec0271fb21_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        $__internal_f12362845804cd4c1f28693d877e50de8ad7e3bb1bdc1e494378644c59d44168 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f12362845804cd4c1f28693d877e50de8ad7e3bb1bdc1e494378644c59d44168->enter($__internal_f12362845804cd4c1f28693d877e50de8ad7e3bb1bdc1e494378644c59d44168_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        // line 242
        if (($context["required"] ?? $this->getContext($context, "required"))) {
            // line 243
            echo "        ";
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " required"))));
            // line 244
            echo "    ";
        }
        // line 245
        echo "    ";
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 246
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
            // line 247
            echo "    ";
        }
        // line 248
        echo "    ";
        if (array_key_exists("parent_label_class", $context)) {
            // line 249
            echo "        ";
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class"))))));
            // line 250
            echo "    ";
        }
        // line 251
        echo "    ";
        if (twig_test_empty(($context["label"] ?? $this->getContext($context, "label")))) {
            // line 252
            if ( !twig_test_empty(($context["label_format"] ?? $this->getContext($context, "label_format")))) {
                // line 253
                $context["label"] = twig_replace_filter(($context["label_format"] ?? $this->getContext($context, "label_format")), array("%name%" =>                 // line 254
($context["name"] ?? $this->getContext($context, "name")), "%id%" =>                 // line 255
($context["id"] ?? $this->getContext($context, "id"))));
            } else {
                // line 258
                $context["label"] = $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->humanize(($context["name"] ?? $this->getContext($context, "name")));
            }
        }
        // line 261
        echo "    <label";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["label_attr"] ?? $this->getContext($context, "label_attr")));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo ">
        ";
        // line 262
        echo ($context["widget"] ?? $this->getContext($context, "widget"));
        echo "
        ";
        // line 263
        echo twig_escape_filter($this->env, (((($context["translation_domain"] ?? $this->getContext($context, "translation_domain")) === false)) ? (($context["label"] ?? $this->getContext($context, "label"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(($context["label"] ?? $this->getContext($context, "label")), array(), ($context["translation_domain"] ?? $this->getContext($context, "translation_domain"))))), "html", null, true);
        echo "
    </label>";
        
        $__internal_f12362845804cd4c1f28693d877e50de8ad7e3bb1bdc1e494378644c59d44168->leave($__internal_f12362845804cd4c1f28693d877e50de8ad7e3bb1bdc1e494378644c59d44168_prof);

        
        $__internal_d413284ecc072b8e601d00a0ac62c64ab1e93d4987e74526db6644ec0271fb21->leave($__internal_d413284ecc072b8e601d00a0ac62c64ab1e93d4987e74526db6644ec0271fb21_prof);

    }

    // line 269
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_acd66b020f40ac75c3b5ac48fa7dfad9d9119ea601a22e82ffa40882303b7313 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_acd66b020f40ac75c3b5ac48fa7dfad9d9119ea601a22e82ffa40882303b7313->enter($__internal_acd66b020f40ac75c3b5ac48fa7dfad9d9119ea601a22e82ffa40882303b7313_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_d56840000db85a8be023eed980c88072bf53413a424554f2b1ed1a8e3e06421e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d56840000db85a8be023eed980c88072bf53413a424554f2b1ed1a8e3e06421e->enter($__internal_d56840000db85a8be023eed980c88072bf53413a424554f2b1ed1a8e3e06421e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 270
        echo "<div class=\"row\">
        <div class=\"large-12 columns";
        // line 271
        if ((( !($context["compound"] ?? $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter(($context["force_error"] ?? $this->getContext($context, "force_error")), false)) : (false))) &&  !($context["valid"] ?? $this->getContext($context, "valid")))) {
            echo " error";
        }
        echo "\">
            ";
        // line 272
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label');
        echo "
            ";
        // line 273
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
            ";
        // line 274
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        echo "
        </div>
    </div>";
        
        $__internal_d56840000db85a8be023eed980c88072bf53413a424554f2b1ed1a8e3e06421e->leave($__internal_d56840000db85a8be023eed980c88072bf53413a424554f2b1ed1a8e3e06421e_prof);

        
        $__internal_acd66b020f40ac75c3b5ac48fa7dfad9d9119ea601a22e82ffa40882303b7313->leave($__internal_acd66b020f40ac75c3b5ac48fa7dfad9d9119ea601a22e82ffa40882303b7313_prof);

    }

    // line 279
    public function block_choice_row($context, array $blocks = array())
    {
        $__internal_fe7891e44c6df014064d8ded51bfb927d3c732bf14b20d4e988a7e48882d1478 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fe7891e44c6df014064d8ded51bfb927d3c732bf14b20d4e988a7e48882d1478->enter($__internal_fe7891e44c6df014064d8ded51bfb927d3c732bf14b20d4e988a7e48882d1478_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_row"));

        $__internal_658cd9b0a5a888cf7ece0c5a4337ba2e471669d845e5af15197590e63e9c5eb8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_658cd9b0a5a888cf7ece0c5a4337ba2e471669d845e5af15197590e63e9c5eb8->enter($__internal_658cd9b0a5a888cf7ece0c5a4337ba2e471669d845e5af15197590e63e9c5eb8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_row"));

        // line 280
        $context["force_error"] = true;
        // line 281
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_658cd9b0a5a888cf7ece0c5a4337ba2e471669d845e5af15197590e63e9c5eb8->leave($__internal_658cd9b0a5a888cf7ece0c5a4337ba2e471669d845e5af15197590e63e9c5eb8_prof);

        
        $__internal_fe7891e44c6df014064d8ded51bfb927d3c732bf14b20d4e988a7e48882d1478->leave($__internal_fe7891e44c6df014064d8ded51bfb927d3c732bf14b20d4e988a7e48882d1478_prof);

    }

    // line 284
    public function block_date_row($context, array $blocks = array())
    {
        $__internal_fd1d28bacb17a610f95b48a6a9fb695ba368e02225b955f88668831b370f21cb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fd1d28bacb17a610f95b48a6a9fb695ba368e02225b955f88668831b370f21cb->enter($__internal_fd1d28bacb17a610f95b48a6a9fb695ba368e02225b955f88668831b370f21cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_row"));

        $__internal_11619416be7ed24602e7c40cf8938b93b9d25300e43593037d2b06ed6b140495 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_11619416be7ed24602e7c40cf8938b93b9d25300e43593037d2b06ed6b140495->enter($__internal_11619416be7ed24602e7c40cf8938b93b9d25300e43593037d2b06ed6b140495_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_row"));

        // line 285
        $context["force_error"] = true;
        // line 286
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_11619416be7ed24602e7c40cf8938b93b9d25300e43593037d2b06ed6b140495->leave($__internal_11619416be7ed24602e7c40cf8938b93b9d25300e43593037d2b06ed6b140495_prof);

        
        $__internal_fd1d28bacb17a610f95b48a6a9fb695ba368e02225b955f88668831b370f21cb->leave($__internal_fd1d28bacb17a610f95b48a6a9fb695ba368e02225b955f88668831b370f21cb_prof);

    }

    // line 289
    public function block_time_row($context, array $blocks = array())
    {
        $__internal_94940c352bbf2f3b75a1d8530ad359d16352918dcab51bd3b0c07ff0dd2951cb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_94940c352bbf2f3b75a1d8530ad359d16352918dcab51bd3b0c07ff0dd2951cb->enter($__internal_94940c352bbf2f3b75a1d8530ad359d16352918dcab51bd3b0c07ff0dd2951cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_row"));

        $__internal_72839af14fb25d024b8e635b72f141c14290f78a4fc222bc1fd25fe3b83a2e32 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_72839af14fb25d024b8e635b72f141c14290f78a4fc222bc1fd25fe3b83a2e32->enter($__internal_72839af14fb25d024b8e635b72f141c14290f78a4fc222bc1fd25fe3b83a2e32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_row"));

        // line 290
        $context["force_error"] = true;
        // line 291
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_72839af14fb25d024b8e635b72f141c14290f78a4fc222bc1fd25fe3b83a2e32->leave($__internal_72839af14fb25d024b8e635b72f141c14290f78a4fc222bc1fd25fe3b83a2e32_prof);

        
        $__internal_94940c352bbf2f3b75a1d8530ad359d16352918dcab51bd3b0c07ff0dd2951cb->leave($__internal_94940c352bbf2f3b75a1d8530ad359d16352918dcab51bd3b0c07ff0dd2951cb_prof);

    }

    // line 294
    public function block_datetime_row($context, array $blocks = array())
    {
        $__internal_bbeaef4ffda3a92bf5c3e086ff274016fe28263bd23858086ae3b1396258c940 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bbeaef4ffda3a92bf5c3e086ff274016fe28263bd23858086ae3b1396258c940->enter($__internal_bbeaef4ffda3a92bf5c3e086ff274016fe28263bd23858086ae3b1396258c940_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_row"));

        $__internal_0721229e4163fefa75ba26bdc5f87563c2e2fd94ee7756075f74c3d9f33be68f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0721229e4163fefa75ba26bdc5f87563c2e2fd94ee7756075f74c3d9f33be68f->enter($__internal_0721229e4163fefa75ba26bdc5f87563c2e2fd94ee7756075f74c3d9f33be68f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_row"));

        // line 295
        $context["force_error"] = true;
        // line 296
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_0721229e4163fefa75ba26bdc5f87563c2e2fd94ee7756075f74c3d9f33be68f->leave($__internal_0721229e4163fefa75ba26bdc5f87563c2e2fd94ee7756075f74c3d9f33be68f_prof);

        
        $__internal_bbeaef4ffda3a92bf5c3e086ff274016fe28263bd23858086ae3b1396258c940->leave($__internal_bbeaef4ffda3a92bf5c3e086ff274016fe28263bd23858086ae3b1396258c940_prof);

    }

    // line 299
    public function block_checkbox_row($context, array $blocks = array())
    {
        $__internal_a8a9b54f597f78f6013b1b7e9e3dfb5607a792e9e0a89d69d98f7481b5b0c805 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a8a9b54f597f78f6013b1b7e9e3dfb5607a792e9e0a89d69d98f7481b5b0c805->enter($__internal_a8a9b54f597f78f6013b1b7e9e3dfb5607a792e9e0a89d69d98f7481b5b0c805_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        $__internal_9457240f5bef28c2313348a6513961092c16469b7c47bf4737227ade889f3f46 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9457240f5bef28c2313348a6513961092c16469b7c47bf4737227ade889f3f46->enter($__internal_9457240f5bef28c2313348a6513961092c16469b7c47bf4737227ade889f3f46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        // line 300
        echo "<div class=\"row\">
        <div class=\"large-12 columns";
        // line 301
        if ( !($context["valid"] ?? $this->getContext($context, "valid"))) {
            echo " error";
        }
        echo "\">
            ";
        // line 302
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
            ";
        // line 303
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        echo "
        </div>
    </div>";
        
        $__internal_9457240f5bef28c2313348a6513961092c16469b7c47bf4737227ade889f3f46->leave($__internal_9457240f5bef28c2313348a6513961092c16469b7c47bf4737227ade889f3f46_prof);

        
        $__internal_a8a9b54f597f78f6013b1b7e9e3dfb5607a792e9e0a89d69d98f7481b5b0c805->leave($__internal_a8a9b54f597f78f6013b1b7e9e3dfb5607a792e9e0a89d69d98f7481b5b0c805_prof);

    }

    // line 308
    public function block_radio_row($context, array $blocks = array())
    {
        $__internal_236a90266b33a2913f40acb50b9e7ec02dcf9814c6a6253064658b1f1bb1c3b2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_236a90266b33a2913f40acb50b9e7ec02dcf9814c6a6253064658b1f1bb1c3b2->enter($__internal_236a90266b33a2913f40acb50b9e7ec02dcf9814c6a6253064658b1f1bb1c3b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        $__internal_f97fb6bcd691e806ee6a243e659d9dc1df477f4bceab10db3825e01fd138bec8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f97fb6bcd691e806ee6a243e659d9dc1df477f4bceab10db3825e01fd138bec8->enter($__internal_f97fb6bcd691e806ee6a243e659d9dc1df477f4bceab10db3825e01fd138bec8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        // line 309
        echo "<div class=\"row\">
        <div class=\"large-12 columns";
        // line 310
        if ( !($context["valid"] ?? $this->getContext($context, "valid"))) {
            echo " error";
        }
        echo "\">
            ";
        // line 311
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
            ";
        // line 312
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        echo "
        </div>
    </div>";
        
        $__internal_f97fb6bcd691e806ee6a243e659d9dc1df477f4bceab10db3825e01fd138bec8->leave($__internal_f97fb6bcd691e806ee6a243e659d9dc1df477f4bceab10db3825e01fd138bec8_prof);

        
        $__internal_236a90266b33a2913f40acb50b9e7ec02dcf9814c6a6253064658b1f1bb1c3b2->leave($__internal_236a90266b33a2913f40acb50b9e7ec02dcf9814c6a6253064658b1f1bb1c3b2_prof);

    }

    // line 319
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_25a8f6838e67c3139b99a54c8c4dd02ea025635b96d6c13123c9931f221ce789 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_25a8f6838e67c3139b99a54c8c4dd02ea025635b96d6c13123c9931f221ce789->enter($__internal_25a8f6838e67c3139b99a54c8c4dd02ea025635b96d6c13123c9931f221ce789_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        $__internal_ba77f7c7deae5a3c8aeb2e0e678d2b3b3944f05a1ce4d30712629fb419bc4a8e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ba77f7c7deae5a3c8aeb2e0e678d2b3b3944f05a1ce4d30712629fb419bc4a8e->enter($__internal_ba77f7c7deae5a3c8aeb2e0e678d2b3b3944f05a1ce4d30712629fb419bc4a8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 320
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 321
            if ( !Symfony\Bridge\Twig\Extension\twig_is_root_form(($context["form"] ?? $this->getContext($context, "form")))) {
                echo "<small class=\"error\">";
            } else {
                echo "<div data-alert class=\"alert-box alert\">";
            }
            // line 322
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? $this->getContext($context, "errors")));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 323
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "
            ";
                // line 324
                if ( !$this->getAttribute($context["loop"], "last", array())) {
                    echo ", ";
                }
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 326
            if ( !Symfony\Bridge\Twig\Extension\twig_is_root_form(($context["form"] ?? $this->getContext($context, "form")))) {
                echo "</small>";
            } else {
                echo "</div>";
            }
        }
        
        $__internal_ba77f7c7deae5a3c8aeb2e0e678d2b3b3944f05a1ce4d30712629fb419bc4a8e->leave($__internal_ba77f7c7deae5a3c8aeb2e0e678d2b3b3944f05a1ce4d30712629fb419bc4a8e_prof);

        
        $__internal_25a8f6838e67c3139b99a54c8c4dd02ea025635b96d6c13123c9931f221ce789->leave($__internal_25a8f6838e67c3139b99a54c8c4dd02ea025635b96d6c13123c9931f221ce789_prof);

    }

    public function getTemplateName()
    {
        return "foundation_5_layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1078 => 326,  1062 => 324,  1058 => 323,  1041 => 322,  1035 => 321,  1033 => 320,  1024 => 319,  1011 => 312,  1007 => 311,  1001 => 310,  998 => 309,  989 => 308,  976 => 303,  972 => 302,  966 => 301,  963 => 300,  954 => 299,  943 => 296,  941 => 295,  932 => 294,  921 => 291,  919 => 290,  910 => 289,  899 => 286,  897 => 285,  888 => 284,  877 => 281,  875 => 280,  866 => 279,  853 => 274,  849 => 273,  845 => 272,  839 => 271,  836 => 270,  827 => 269,  815 => 263,  811 => 262,  795 => 261,  791 => 258,  788 => 255,  787 => 254,  786 => 253,  784 => 252,  781 => 251,  778 => 250,  775 => 249,  772 => 248,  769 => 247,  767 => 246,  764 => 245,  761 => 244,  758 => 243,  756 => 242,  747 => 241,  737 => 238,  728 => 237,  718 => 234,  709 => 233,  699 => 230,  696 => 229,  694 => 228,  691 => 227,  689 => 226,  687 => 225,  678 => 224,  668 => 221,  665 => 220,  663 => 219,  661 => 218,  652 => 217,  638 => 210,  635 => 209,  632 => 208,  630 => 207,  627 => 206,  621 => 204,  618 => 203,  616 => 202,  607 => 201,  593 => 196,  590 => 195,  584 => 193,  581 => 192,  578 => 191,  576 => 190,  573 => 189,  571 => 188,  562 => 187,  550 => 183,  543 => 181,  541 => 180,  539 => 179,  535 => 178,  530 => 177,  526 => 175,  519 => 173,  517 => 172,  515 => 171,  511 => 170,  508 => 169,  506 => 168,  497 => 167,  487 => 164,  485 => 163,  483 => 162,  477 => 159,  475 => 158,  473 => 157,  471 => 156,  469 => 155,  460 => 153,  458 => 152,  450 => 151,  447 => 149,  445 => 148,  442 => 147,  439 => 146,  437 => 145,  435 => 144,  432 => 143,  429 => 142,  427 => 141,  425 => 140,  416 => 139,  405 => 136,  401 => 134,  398 => 133,  390 => 128,  379 => 121,  371 => 116,  358 => 106,  347 => 99,  344 => 98,  338 => 96,  335 => 95,  332 => 94,  329 => 92,  327 => 91,  318 => 90,  307 => 87,  303 => 85,  301 => 84,  299 => 82,  298 => 81,  297 => 80,  296 => 79,  290 => 77,  287 => 76,  284 => 75,  281 => 73,  279 => 72,  270 => 71,  256 => 66,  252 => 65,  248 => 64,  243 => 62,  239 => 61,  236 => 60,  233 => 59,  230 => 57,  228 => 56,  219 => 55,  205 => 48,  203 => 47,  200 => 45,  191 => 44,  181 => 41,  175 => 38,  172 => 37,  170 => 36,  167 => 35,  165 => 34,  163 => 33,  157 => 30,  154 => 29,  151 => 28,  149 => 27,  146 => 26,  137 => 25,  127 => 22,  125 => 21,  116 => 20,  106 => 17,  103 => 16,  101 => 15,  99 => 14,  90 => 13,  80 => 10,  77 => 9,  75 => 8,  73 => 7,  64 => 6,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"form_div_layout.html.twig\" %}

{# Based on Foundation 5 Doc #}
{# Widgets #}

{% block form_widget_simple -%}
    {% if errors|length > 0 -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {{- parent() -}}
{%- endblock form_widget_simple %}

{% block textarea_widget -%}
    {% if errors|length > 0 -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {{- parent() -}}
{%- endblock textarea_widget %}

{% block button_widget -%}
    {% set attr = attr|merge({class: (attr.class|default('') ~ ' button')|trim}) %}
    {{- parent() -}}
{%- endblock button_widget %}

{% block money_widget -%}
    <div class=\"row collapse\">
        {% set prepend = '{{' == money_pattern[0:2] %}
        {% if not prepend %}
            <div class=\"small-3 large-2 columns\">
                <span class=\"prefix\">{{ money_pattern|form_encode_currency }}</span>
            </div>
        {% endif %}
        <div class=\"small-9 large-10 columns\">
            {{- block('form_widget_simple') -}}
        </div>
        {% if prepend %}
            <div class=\"small-3 large-2 columns\">
                <span class=\"postfix\">{{ money_pattern|form_encode_currency }}</span>
            </div>
        {% endif %}
    </div>
{%- endblock money_widget %}

{% block percent_widget -%}
    <div class=\"row collapse\">
        <div class=\"small-9 large-10 columns\">
            {{- block('form_widget_simple') -}}
        </div>
        <div class=\"small-3 large-2 columns\">
            <span class=\"postfix\">%</span>
        </div>
    </div>
{%- endblock percent_widget %}

{% block datetime_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else %}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' row')|trim}) %}
        <div class=\"row\">
            <div class=\"large-7 columns\">{{ form_errors(form.date) }}</div>
            <div class=\"large-5 columns\">{{ form_errors(form.time) }}</div>
        </div>
        <div {{ block('widget_container_attributes') }}>
            <div class=\"large-7 columns\">{{ form_widget(form.date, { datetime: true } ) }}</div>
            <div class=\"large-5 columns\">{{ form_widget(form.time, { datetime: true } ) }}</div>
        </div>
    {% endif %}
{%- endblock datetime_widget %}

{% block date_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else %}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' row')|trim}) %}
        {% if datetime is not defined or not datetime %}
            <div {{ block('widget_container_attributes') }}>
        {% endif %}
        {{- date_pattern|replace({
            '{{ year }}': '<div class=\"large-4 columns\">' ~ form_widget(form.year) ~ '</div>',
            '{{ month }}': '<div class=\"large-4 columns\">' ~ form_widget(form.month) ~ '</div>',
            '{{ day }}': '<div class=\"large-4 columns\">' ~ form_widget(form.day) ~ '</div>',
        })|raw -}}
        {% if datetime is not defined or not datetime %}
            </div>
        {% endif %}
    {% endif %}
{%- endblock date_widget %}

{% block time_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else %}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' row')|trim}) %}
        {% if datetime is not defined or false == datetime %}
            <div {{ block('widget_container_attributes') -}}>
        {% endif %}
        {% if with_seconds %}
            <div class=\"large-4 columns\">{{ form_widget(form.hour) }}</div>
            <div class=\"large-4 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        {{ form_widget(form.minute) }}
                    </div>
                </div>
            </div>
            <div class=\"large-4 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        {{ form_widget(form.second) }}
                    </div>
                </div>
            </div>
        {% else %}
            <div class=\"large-6 columns\">{{ form_widget(form.hour) }}</div>
            <div class=\"large-6 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        {{ form_widget(form.minute) }}
                    </div>
                </div>
            </div>
        {% endif %}
        {% if datetime is not defined or false == datetime %}
            </div>
        {% endif %}
    {% endif %}
{%- endblock time_widget %}

{% block choice_widget_collapsed -%}
    {% if errors|length > 0 -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}

    {% if multiple -%}
        {% set attr = attr|merge({style: (attr.style|default('') ~ ' height: auto; background-image: none;')|trim}) %}
    {% endif %}

    {% if required and placeholder is none and not placeholder_in_choices and not multiple -%}
        {% set required = false %}
    {%- endif -%}
    <select {{ block('widget_attributes') }}{% if multiple %} multiple=\"multiple\" data-customforms=\"disabled\"{% endif %}>
        {% if placeholder is not none -%}
            <option value=\"\"{% if required and value is empty %} selected=\"selected\"{% endif %}>{{ translation_domain is same as(false) ? placeholder : placeholder|trans({}, translation_domain) }}</option>
        {%- endif %}
        {%- if preferred_choices|length > 0 -%}
            {% set options = preferred_choices %}
            {{- block('choice_widget_options') -}}
            {% if choices|length > 0 and separator is not none -%}
                <option disabled=\"disabled\">{{ separator }}</option>
            {%- endif %}
        {%- endif -%}
        {% set options = choices -%}
        {{- block('choice_widget_options') -}}
    </select>
{%- endblock choice_widget_collapsed %}

{% block choice_widget_expanded -%}
    {% if '-inline' in label_attr.class|default('') %}
        <ul class=\"inline-list\">
            {% for child in form %}
                <li>{{ form_widget(child, {
                        parent_label_class: label_attr.class|default(''),
                    }) }}</li>
            {% endfor %}
        </ul>
    {% else %}
        <div {{ block('widget_container_attributes') }}>
            {% for child in form %}
                {{ form_widget(child, {
                    parent_label_class: label_attr.class|default(''),
                }) }}
            {% endfor %}
        </div>
    {% endif %}
{%- endblock choice_widget_expanded %}

{% block checkbox_widget -%}
    {% set parent_label_class = parent_label_class|default('') %}
    {% if errors|length > 0 -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {% if 'checkbox-inline' in parent_label_class %}
        {{ form_label(form, null, { widget: parent() }) }}
    {% else %}
        <div class=\"checkbox\">
            {{ form_label(form, null, { widget: parent() }) }}
        </div>
    {% endif %}
{%- endblock checkbox_widget %}

{% block radio_widget -%}
    {% set parent_label_class = parent_label_class|default('') %}
    {% if 'radio-inline' in parent_label_class %}
        {{ form_label(form, null, { widget: parent() }) }}
    {% else %}
        {% if errors|length > 0 -%}
            {% set attr = attr|merge({class: (attr.class|default('') ~ ' error')|trim}) %}
        {% endif %}
        <div class=\"radio\">
            {{ form_label(form, null, { widget: parent() }) }}
        </div>
    {% endif %}
{%- endblock radio_widget %}

{# Labels #}

{% block form_label -%}
    {% if errors|length > 0 -%}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {{- parent() -}}
{%- endblock form_label %}

{% block choice_label -%}
    {% if errors|length > 0 -%}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {# remove the checkbox-inline and radio-inline class, it's only useful for embed labels #}
    {% set label_attr = label_attr|merge({class: label_attr.class|default('')|replace({'checkbox-inline': '', 'radio-inline': ''})|trim}) %}
    {{- block('form_label') -}}
{%- endblock choice_label %}

{% block checkbox_label -%}
    {{- block('checkbox_radio_label') -}}
{%- endblock checkbox_label %}

{% block radio_label -%}
    {{- block('checkbox_radio_label') -}}
{%- endblock radio_label %}

{% block checkbox_radio_label -%}
    {% if required %}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' required')|trim}) %}
    {% endif %}
    {% if errors|length > 0 -%}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {% if parent_label_class is defined %}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ parent_label_class)|trim}) %}
    {% endif %}
    {% if label is empty %}
        {%- if label_format is not empty -%}
            {% set label = label_format|replace({
                '%name%': name,
                '%id%': id,
            }) %}
        {%- else -%}
            {% set label = name|humanize %}
        {%- endif -%}
    {% endif %}
    <label{% for attrname, attrvalue in label_attr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}>
        {{ widget|raw }}
        {{ translation_domain is same as(false) ? label : label|trans({}, translation_domain) }}
    </label>
{%- endblock checkbox_radio_label %}

{# Rows #}

{% block form_row -%}
    <div class=\"row\">
        <div class=\"large-12 columns{% if (not compound or force_error|default(false)) and not valid %} error{% endif %}\">
            {{ form_label(form) }}
            {{ form_widget(form) }}
            {{ form_errors(form) }}
        </div>
    </div>
{%- endblock form_row %}

{% block choice_row -%}
    {% set force_error = true %}
    {{ block('form_row') }}
{%- endblock choice_row %}

{% block date_row -%}
    {% set force_error = true %}
    {{ block('form_row') }}
{%- endblock date_row %}

{% block time_row -%}
    {% set force_error = true %}
    {{ block('form_row') }}
{%- endblock time_row %}

{% block datetime_row -%}
    {% set force_error = true %}
    {{ block('form_row') }}
{%- endblock datetime_row %}

{% block checkbox_row -%}
    <div class=\"row\">
        <div class=\"large-12 columns{% if not valid %} error{% endif %}\">
            {{ form_widget(form) }}
            {{ form_errors(form) }}
        </div>
    </div>
{%- endblock checkbox_row %}

{% block radio_row -%}
    <div class=\"row\">
        <div class=\"large-12 columns{% if not valid %} error{% endif %}\">
            {{ form_widget(form) }}
            {{ form_errors(form) }}
        </div>
    </div>
{%- endblock radio_row %}

{# Errors #}

{% block form_errors -%}
    {% if errors|length > 0 -%}
        {% if form is not rootform %}<small class=\"error\">{% else %}<div data-alert class=\"alert-box alert\">{% endif %}
        {%- for error in errors -%}
            {{ error.message }}
            {% if not loop.last %}, {% endif %}
        {%- endfor -%}
        {% if form is not rootform %}</small>{% else %}</div>{% endif %}
    {%- endif %}
{%- endblock form_errors %}
", "foundation_5_layout.html.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bridge/Twig/Resources/views/Form/foundation_5_layout.html.twig");
    }
}
