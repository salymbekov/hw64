<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_963b73ee4eae8b8c1cc408329062a7be7e904bc2bb7446e018d105b502f17181 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5d1dba3ff072dd2430f8f741dd04ea30f4e8cdce978b3fdb7c9efa270da560f1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d1dba3ff072dd2430f8f741dd04ea30f4e8cdce978b3fdb7c9efa270da560f1->enter($__internal_5d1dba3ff072dd2430f8f741dd04ea30f4e8cdce978b3fdb7c9efa270da560f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        $__internal_69ad166ce59d1c01b51d5b4a97bb01d20f2f334a7a56feb2b025836733fffac9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_69ad166ce59d1c01b51d5b4a97bb01d20f2f334a7a56feb2b025836733fffac9->enter($__internal_69ad166ce59d1c01b51d5b4a97bb01d20f2f334a7a56feb2b025836733fffac9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_5d1dba3ff072dd2430f8f741dd04ea30f4e8cdce978b3fdb7c9efa270da560f1->leave($__internal_5d1dba3ff072dd2430f8f741dd04ea30f4e8cdce978b3fdb7c9efa270da560f1_prof);

        
        $__internal_69ad166ce59d1c01b51d5b4a97bb01d20f2f334a7a56feb2b025836733fffac9->leave($__internal_69ad166ce59d1c01b51d5b4a97bb01d20f2f334a7a56feb2b025836733fffac9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
", "@Framework/Form/url_widget.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/url_widget.html.php");
    }
}
