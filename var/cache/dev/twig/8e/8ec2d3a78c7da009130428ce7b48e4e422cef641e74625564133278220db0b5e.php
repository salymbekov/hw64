<?php

/* WebProfilerBundle:Collector:exception.css.twig */
class __TwigTemplate_5dbf5dfdf326b4b2ee168ef93e8dd386cc51073256febe4e18be12d2463592a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_118c647fc1655b3f6998d871faa61a779bbec4fa42258e681c2c96dc7fbc70f9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_118c647fc1655b3f6998d871faa61a779bbec4fa42258e681c2c96dc7fbc70f9->enter($__internal_118c647fc1655b3f6998d871faa61a779bbec4fa42258e681c2c96dc7fbc70f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.css.twig"));

        $__internal_f19d11b82aa9b41d00b4a447b500754876e7dd0879e1b3cdd700123265bc400a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f19d11b82aa9b41d00b4a447b500754876e7dd0879e1b3cdd700123265bc400a->enter($__internal_f19d11b82aa9b41d00b4a447b500754876e7dd0879e1b3cdd700123265bc400a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.css.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "

.container {
    max-width: auto;
    margin: 0;
    padding: 0;
}
.container .container {
    padding: 0;
}

.exception-summary {
    background: #FFF;
    border: 1px solid #E0E0E0;
    box-shadow: 0 0 1px rgba(128, 128, 128, .2);
    margin: 1em 0;
    padding: 10px;
}
.exception-summary.exception-without-message {
    display: none;
}

.exception-message {
    color: #B0413E;
}

.exception-metadata,
.exception-illustration {
    display: none;
}

.exception-message-wrapper .container {
    min-height: auto;
}
";
        
        $__internal_118c647fc1655b3f6998d871faa61a779bbec4fa42258e681c2c96dc7fbc70f9->leave($__internal_118c647fc1655b3f6998d871faa61a779bbec4fa42258e681c2c96dc7fbc70f9_prof);

        
        $__internal_f19d11b82aa9b41d00b4a447b500754876e7dd0879e1b3cdd700123265bc400a->leave($__internal_f19d11b82aa9b41d00b4a447b500754876e7dd0879e1b3cdd700123265bc400a_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/exception.css.twig') }}

.container {
    max-width: auto;
    margin: 0;
    padding: 0;
}
.container .container {
    padding: 0;
}

.exception-summary {
    background: #FFF;
    border: 1px solid #E0E0E0;
    box-shadow: 0 0 1px rgba(128, 128, 128, .2);
    margin: 1em 0;
    padding: 10px;
}
.exception-summary.exception-without-message {
    display: none;
}

.exception-message {
    color: #B0413E;
}

.exception-metadata,
.exception-illustration {
    display: none;
}

.exception-message-wrapper .container {
    min-height: auto;
}
", "WebProfilerBundle:Collector:exception.css.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.css.twig");
    }
}
