<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_09ac1fc8d05963f679bffff5bed6fb84f2a9c52ce18b2fd75a163237d04fa7a5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4ca4b1f64c670fde5646fc0b18bb6a73ca67be8f9b01bb06218573af91913651 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4ca4b1f64c670fde5646fc0b18bb6a73ca67be8f9b01bb06218573af91913651->enter($__internal_4ca4b1f64c670fde5646fc0b18bb6a73ca67be8f9b01bb06218573af91913651_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        $__internal_4d1bf85b993c5cd746ff3143833d534ca4323d3b076776728a10e7336a6379a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d1bf85b993c5cd746ff3143833d534ca4323d3b076776728a10e7336a6379a5->enter($__internal_4d1bf85b993c5cd746ff3143833d534ca4323d3b076776728a10e7336a6379a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_4ca4b1f64c670fde5646fc0b18bb6a73ca67be8f9b01bb06218573af91913651->leave($__internal_4ca4b1f64c670fde5646fc0b18bb6a73ca67be8f9b01bb06218573af91913651_prof);

        
        $__internal_4d1bf85b993c5cd746ff3143833d534ca4323d3b076776728a10e7336a6379a5->leave($__internal_4d1bf85b993c5cd746ff3143833d534ca4323d3b076776728a10e7336a6379a5_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "TwigBundle:Exception:error.css.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.css.twig");
    }
}
