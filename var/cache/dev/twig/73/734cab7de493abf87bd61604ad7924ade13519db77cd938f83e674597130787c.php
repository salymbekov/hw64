<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_bcaba7dc602a91d924cd0b8a91ff4c6a84e0b0a8c92c22952a60db54f8aab6e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_eb8cd1790fa6f0f8a9f020d480e9374e928b6f1561b119df548c4df111521c63 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eb8cd1790fa6f0f8a9f020d480e9374e928b6f1561b119df548c4df111521c63->enter($__internal_eb8cd1790fa6f0f8a9f020d480e9374e928b6f1561b119df548c4df111521c63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        $__internal_4bd737b4cc0b1a505b7f78bceada232c5a842ac1b8869fc97c918c50c7b737dd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4bd737b4cc0b1a505b7f78bceada232c5a842ac1b8869fc97c918c50c7b737dd->enter($__internal_4bd737b4cc0b1a505b7f78bceada232c5a842ac1b8869fc97c918c50c7b737dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
*/
";
        
        $__internal_eb8cd1790fa6f0f8a9f020d480e9374e928b6f1561b119df548c4df111521c63->leave($__internal_eb8cd1790fa6f0f8a9f020d480e9374e928b6f1561b119df548c4df111521c63_prof);

        
        $__internal_4bd737b4cc0b1a505b7f78bceada232c5a842ac1b8869fc97c918c50c7b737dd->leave($__internal_4bd737b4cc0b1a505b7f78bceada232c5a842ac1b8869fc97c918c50c7b737dd_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.css.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.css.twig");
    }
}
