<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_06a341c872eba95342da55c57c64a963514221b12ec8c6388f378b5bfa74b4bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_12cd25786cecfe3161d6e90182f3c0c35525b77830387fd14dcf765705b12b7b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_12cd25786cecfe3161d6e90182f3c0c35525b77830387fd14dcf765705b12b7b->enter($__internal_12cd25786cecfe3161d6e90182f3c0c35525b77830387fd14dcf765705b12b7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        $__internal_32a29a7c15e3ab5956bb0d1734af37e283e3e1c7b51a2d61d0bc03e90aaf38af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_32a29a7c15e3ab5956bb0d1734af37e283e3e1c7b51a2d61d0bc03e90aaf38af->enter($__internal_32a29a7c15e3ab5956bb0d1734af37e283e3e1c7b51a2d61d0bc03e90aaf38af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_12cd25786cecfe3161d6e90182f3c0c35525b77830387fd14dcf765705b12b7b->leave($__internal_12cd25786cecfe3161d6e90182f3c0c35525b77830387fd14dcf765705b12b7b_prof);

        
        $__internal_32a29a7c15e3ab5956bb0d1734af37e283e3e1c7b51a2d61d0bc03e90aaf38af->leave($__internal_32a29a7c15e3ab5956bb0d1734af37e283e3e1c7b51a2d61d0bc03e90aaf38af_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
", "@Framework/Form/form_rest.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rest.html.php");
    }
}
