<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_044b9ffaf01d29f06450413f3ce3d213fd65b638eb47f3a0e34e2d3d36214638 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7fe957a8d46a0dec29bb322ca282806cb0c73c7bd01919cd1b91ddacbb55f647 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7fe957a8d46a0dec29bb322ca282806cb0c73c7bd01919cd1b91ddacbb55f647->enter($__internal_7fe957a8d46a0dec29bb322ca282806cb0c73c7bd01919cd1b91ddacbb55f647_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        $__internal_2ed570fd52acae2072b28398ed9c7f2c5b439a32b07c727591483f61ee15868d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2ed570fd52acae2072b28398ed9c7f2c5b439a32b07c727591483f61ee15868d->enter($__internal_2ed570fd52acae2072b28398ed9c7f2c5b439a32b07c727591483f61ee15868d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form); ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form); ?>
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
";
        
        $__internal_7fe957a8d46a0dec29bb322ca282806cb0c73c7bd01919cd1b91ddacbb55f647->leave($__internal_7fe957a8d46a0dec29bb322ca282806cb0c73c7bd01919cd1b91ddacbb55f647_prof);

        
        $__internal_2ed570fd52acae2072b28398ed9c7f2c5b439a32b07c727591483f61ee15868d->leave($__internal_2ed570fd52acae2072b28398ed9c7f2c5b439a32b07c727591483f61ee15868d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td>
        <?php echo \$view['form']->label(\$form); ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form); ?>
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
", "@Framework/FormTable/form_row.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_row.html.php");
    }
}
