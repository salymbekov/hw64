<?php

/* FOSUserBundle:Group:edit.html.twig */
class __TwigTemplate_728f9cf7034b2fff398ffdb143a6a48548dfa510d398df418daa142d3f4c773f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2b3293b87145bdb73aa2db0b42db541e5423e6765e56671008569b887740efd4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2b3293b87145bdb73aa2db0b42db541e5423e6765e56671008569b887740efd4->enter($__internal_2b3293b87145bdb73aa2db0b42db541e5423e6765e56671008569b887740efd4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $__internal_f0d00d30f43c5844d26c50ca57e8a5d380c866dd28516586d0d95b3eec7a2cc5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f0d00d30f43c5844d26c50ca57e8a5d380c866dd28516586d0d95b3eec7a2cc5->enter($__internal_f0d00d30f43c5844d26c50ca57e8a5d380c866dd28516586d0d95b3eec7a2cc5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2b3293b87145bdb73aa2db0b42db541e5423e6765e56671008569b887740efd4->leave($__internal_2b3293b87145bdb73aa2db0b42db541e5423e6765e56671008569b887740efd4_prof);

        
        $__internal_f0d00d30f43c5844d26c50ca57e8a5d380c866dd28516586d0d95b3eec7a2cc5->leave($__internal_f0d00d30f43c5844d26c50ca57e8a5d380c866dd28516586d0d95b3eec7a2cc5_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_651ccbbc3adb00fa350ae1f05024d4bd8bd1825cde5749e3577d7a158105884c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_651ccbbc3adb00fa350ae1f05024d4bd8bd1825cde5749e3577d7a158105884c->enter($__internal_651ccbbc3adb00fa350ae1f05024d4bd8bd1825cde5749e3577d7a158105884c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_37f4f0c3e85c3f907e2aade85532f22a147ed3b3eea2fc0a218aa0647c3406a2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_37f4f0c3e85c3f907e2aade85532f22a147ed3b3eea2fc0a218aa0647c3406a2->enter($__internal_37f4f0c3e85c3f907e2aade85532f22a147ed3b3eea2fc0a218aa0647c3406a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/edit_content.html.twig", "FOSUserBundle:Group:edit.html.twig", 4)->display($context);
        
        $__internal_37f4f0c3e85c3f907e2aade85532f22a147ed3b3eea2fc0a218aa0647c3406a2->leave($__internal_37f4f0c3e85c3f907e2aade85532f22a147ed3b3eea2fc0a218aa0647c3406a2_prof);

        
        $__internal_651ccbbc3adb00fa350ae1f05024d4bd8bd1825cde5749e3577d7a158105884c->leave($__internal_651ccbbc3adb00fa350ae1f05024d4bd8bd1825cde5749e3577d7a158105884c_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:edit.html.twig", "/home/timur/http/hw/hw64/vendor/friendsofsymfony/user-bundle/Resources/views/Group/edit.html.twig");
    }
}
