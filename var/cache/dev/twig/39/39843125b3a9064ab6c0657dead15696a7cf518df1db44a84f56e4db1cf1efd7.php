<?php

/* form_table_layout.html.twig */
class __TwigTemplate_44d0b5e7c9c3bd95ef2001541a579c762b7371c901650bc1df1a59b5efa85121 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("form_div_layout.html.twig", "form_table_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."form_div_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_row' => array($this, 'block_form_row'),
                'button_row' => array($this, 'block_button_row'),
                'hidden_row' => array($this, 'block_hidden_row'),
                'form_widget_compound' => array($this, 'block_form_widget_compound'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bd18840d0f16d736c3fc516cb1c33c6175dba3c23c80d101177a4347b01df8aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bd18840d0f16d736c3fc516cb1c33c6175dba3c23c80d101177a4347b01df8aa->enter($__internal_bd18840d0f16d736c3fc516cb1c33c6175dba3c23c80d101177a4347b01df8aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form_table_layout.html.twig"));

        $__internal_1f882ebb7a00c0c37b294bbab2b27b5eb255ca566c42f155342667137dde2160 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f882ebb7a00c0c37b294bbab2b27b5eb255ca566c42f155342667137dde2160->enter($__internal_1f882ebb7a00c0c37b294bbab2b27b5eb255ca566c42f155342667137dde2160_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form_table_layout.html.twig"));

        // line 3
        $this->displayBlock('form_row', $context, $blocks);
        // line 15
        $this->displayBlock('button_row', $context, $blocks);
        // line 24
        $this->displayBlock('hidden_row', $context, $blocks);
        // line 32
        $this->displayBlock('form_widget_compound', $context, $blocks);
        
        $__internal_bd18840d0f16d736c3fc516cb1c33c6175dba3c23c80d101177a4347b01df8aa->leave($__internal_bd18840d0f16d736c3fc516cb1c33c6175dba3c23c80d101177a4347b01df8aa_prof);

        
        $__internal_1f882ebb7a00c0c37b294bbab2b27b5eb255ca566c42f155342667137dde2160->leave($__internal_1f882ebb7a00c0c37b294bbab2b27b5eb255ca566c42f155342667137dde2160_prof);

    }

    // line 3
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_8942d9c1fcce43ac59ad1ce8c48c52a0eecc9a08f92014c10e99b67bd205975a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8942d9c1fcce43ac59ad1ce8c48c52a0eecc9a08f92014c10e99b67bd205975a->enter($__internal_8942d9c1fcce43ac59ad1ce8c48c52a0eecc9a08f92014c10e99b67bd205975a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_057dcca52e96088ad0f6bb81423fd069c658f20e4a3a86d49459eb47dd5d1a0d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_057dcca52e96088ad0f6bb81423fd069c658f20e4a3a86d49459eb47dd5d1a0d->enter($__internal_057dcca52e96088ad0f6bb81423fd069c658f20e4a3a86d49459eb47dd5d1a0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 4
        echo "<tr>
        <td>";
        // line 6
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label');
        // line 7
        echo "</td>
        <td>";
        // line 9
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 11
        echo "</td>
    </tr>";
        
        $__internal_057dcca52e96088ad0f6bb81423fd069c658f20e4a3a86d49459eb47dd5d1a0d->leave($__internal_057dcca52e96088ad0f6bb81423fd069c658f20e4a3a86d49459eb47dd5d1a0d_prof);

        
        $__internal_8942d9c1fcce43ac59ad1ce8c48c52a0eecc9a08f92014c10e99b67bd205975a->leave($__internal_8942d9c1fcce43ac59ad1ce8c48c52a0eecc9a08f92014c10e99b67bd205975a_prof);

    }

    // line 15
    public function block_button_row($context, array $blocks = array())
    {
        $__internal_db6a245790f58024390f2ea78575079990181de5c90f2f44bc01265eee88d97c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_db6a245790f58024390f2ea78575079990181de5c90f2f44bc01265eee88d97c->enter($__internal_db6a245790f58024390f2ea78575079990181de5c90f2f44bc01265eee88d97c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        $__internal_5080e5e096406ee6b6a8c93983180e26e02431dff4634605bcab50372b4b55cf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5080e5e096406ee6b6a8c93983180e26e02431dff4634605bcab50372b4b55cf->enter($__internal_5080e5e096406ee6b6a8c93983180e26e02431dff4634605bcab50372b4b55cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        // line 16
        echo "<tr>
        <td></td>
        <td>";
        // line 19
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 20
        echo "</td>
    </tr>";
        
        $__internal_5080e5e096406ee6b6a8c93983180e26e02431dff4634605bcab50372b4b55cf->leave($__internal_5080e5e096406ee6b6a8c93983180e26e02431dff4634605bcab50372b4b55cf_prof);

        
        $__internal_db6a245790f58024390f2ea78575079990181de5c90f2f44bc01265eee88d97c->leave($__internal_db6a245790f58024390f2ea78575079990181de5c90f2f44bc01265eee88d97c_prof);

    }

    // line 24
    public function block_hidden_row($context, array $blocks = array())
    {
        $__internal_146b8992de00d0a076d7771d56cc1414a7b39dd86e6400f14aba32560bf42eeb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_146b8992de00d0a076d7771d56cc1414a7b39dd86e6400f14aba32560bf42eeb->enter($__internal_146b8992de00d0a076d7771d56cc1414a7b39dd86e6400f14aba32560bf42eeb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "hidden_row"));

        $__internal_c70f4c6f49dcae278e69ee2219b769302685cee77ab56fc8d07bd294dd64f12d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c70f4c6f49dcae278e69ee2219b769302685cee77ab56fc8d07bd294dd64f12d->enter($__internal_c70f4c6f49dcae278e69ee2219b769302685cee77ab56fc8d07bd294dd64f12d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "hidden_row"));

        // line 25
        echo "<tr style=\"display: none\">
        <td colspan=\"2\">";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 28
        echo "</td>
    </tr>";
        
        $__internal_c70f4c6f49dcae278e69ee2219b769302685cee77ab56fc8d07bd294dd64f12d->leave($__internal_c70f4c6f49dcae278e69ee2219b769302685cee77ab56fc8d07bd294dd64f12d_prof);

        
        $__internal_146b8992de00d0a076d7771d56cc1414a7b39dd86e6400f14aba32560bf42eeb->leave($__internal_146b8992de00d0a076d7771d56cc1414a7b39dd86e6400f14aba32560bf42eeb_prof);

    }

    // line 32
    public function block_form_widget_compound($context, array $blocks = array())
    {
        $__internal_a6d02b6051a09a657db45a2de346909e5238c517f5bc722cbf190633ffb8b3e8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a6d02b6051a09a657db45a2de346909e5238c517f5bc722cbf190633ffb8b3e8->enter($__internal_a6d02b6051a09a657db45a2de346909e5238c517f5bc722cbf190633ffb8b3e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_compound"));

        $__internal_bdaf30e95863b920c3e9cd522a29220c68be349a725d9bf3f37640774cb7f396 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bdaf30e95863b920c3e9cd522a29220c68be349a725d9bf3f37640774cb7f396->enter($__internal_bdaf30e95863b920c3e9cd522a29220c68be349a725d9bf3f37640774cb7f396_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_compound"));

        // line 33
        echo "<table ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">";
        // line 34
        if ((Symfony\Bridge\Twig\Extension\twig_is_root_form(($context["form"] ?? $this->getContext($context, "form"))) && (twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0))) {
            // line 35
            echo "<tr>
            <td colspan=\"2\">";
            // line 37
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
            // line 38
            echo "</td>
        </tr>";
        }
        // line 41
        $this->displayBlock("form_rows", $context, $blocks);
        // line 42
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'rest');
        // line 43
        echo "</table>";
        
        $__internal_bdaf30e95863b920c3e9cd522a29220c68be349a725d9bf3f37640774cb7f396->leave($__internal_bdaf30e95863b920c3e9cd522a29220c68be349a725d9bf3f37640774cb7f396_prof);

        
        $__internal_a6d02b6051a09a657db45a2de346909e5238c517f5bc722cbf190633ffb8b3e8->leave($__internal_a6d02b6051a09a657db45a2de346909e5238c517f5bc722cbf190633ffb8b3e8_prof);

    }

    public function getTemplateName()
    {
        return "form_table_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  168 => 43,  166 => 42,  164 => 41,  160 => 38,  158 => 37,  155 => 35,  153 => 34,  149 => 33,  140 => 32,  129 => 28,  127 => 27,  124 => 25,  115 => 24,  104 => 20,  102 => 19,  98 => 16,  89 => 15,  78 => 11,  76 => 10,  74 => 9,  71 => 7,  69 => 6,  66 => 4,  57 => 3,  47 => 32,  45 => 24,  43 => 15,  41 => 3,  14 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"form_div_layout.html.twig\" %}

{%- block form_row -%}
    <tr>
        <td>
            {{- form_label(form) -}}
        </td>
        <td>
            {{- form_errors(form) -}}
            {{- form_widget(form) -}}
        </td>
    </tr>
{%- endblock form_row -%}

{%- block button_row -%}
    <tr>
        <td></td>
        <td>
            {{- form_widget(form) -}}
        </td>
    </tr>
{%- endblock button_row -%}

{%- block hidden_row -%}
    <tr style=\"display: none\">
        <td colspan=\"2\">
            {{- form_widget(form) -}}
        </td>
    </tr>
{%- endblock hidden_row -%}

{%- block form_widget_compound -%}
    <table {{ block('widget_container_attributes') }}>
        {%- if form is rootform and errors|length > 0 -%}
        <tr>
            <td colspan=\"2\">
                {{- form_errors(form) -}}
            </td>
        </tr>
        {%- endif -%}
        {{- block('form_rows') -}}
        {{- form_rest(form) -}}
    </table>
{%- endblock form_widget_compound -%}
", "form_table_layout.html.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bridge/Twig/Resources/views/Form/form_table_layout.html.twig");
    }
}
