<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_4125a4e738f60ebd03c81cdf43ea632ccc048035627f036dde2d592858cd6d80 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_55b2d0dd4cb826e1b3130cfbfdc40011d05beb1ca317235d8a4b8a7ae6f3a332 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_55b2d0dd4cb826e1b3130cfbfdc40011d05beb1ca317235d8a4b8a7ae6f3a332->enter($__internal_55b2d0dd4cb826e1b3130cfbfdc40011d05beb1ca317235d8a4b8a7ae6f3a332_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $__internal_5a1b1389d1b7dfe5323a18c8bce50334081a680c69063bc1a20c32128804cf7e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5a1b1389d1b7dfe5323a18c8bce50334081a680c69063bc1a20c32128804cf7e->enter($__internal_5a1b1389d1b7dfe5323a18c8bce50334081a680c69063bc1a20c32128804cf7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_55b2d0dd4cb826e1b3130cfbfdc40011d05beb1ca317235d8a4b8a7ae6f3a332->leave($__internal_55b2d0dd4cb826e1b3130cfbfdc40011d05beb1ca317235d8a4b8a7ae6f3a332_prof);

        
        $__internal_5a1b1389d1b7dfe5323a18c8bce50334081a680c69063bc1a20c32128804cf7e->leave($__internal_5a1b1389d1b7dfe5323a18c8bce50334081a680c69063bc1a20c32128804cf7e_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_9f48fa5c3e535002582e69ccfca149dec59ddb195c4bcf57b2bfd4b6b2a25a8c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9f48fa5c3e535002582e69ccfca149dec59ddb195c4bcf57b2bfd4b6b2a25a8c->enter($__internal_9f48fa5c3e535002582e69ccfca149dec59ddb195c4bcf57b2bfd4b6b2a25a8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_2ae2a3adbc70bb73115cdd36c91ff93e8e330d1ad0d10693db12f8ec4eef602b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2ae2a3adbc70bb73115cdd36c91ff93e8e330d1ad0d10693db12f8ec4eef602b->enter($__internal_2ae2a3adbc70bb73115cdd36c91ff93e8e330d1ad0d10693db12f8ec4eef602b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $__internal_2ae2a3adbc70bb73115cdd36c91ff93e8e330d1ad0d10693db12f8ec4eef602b->leave($__internal_2ae2a3adbc70bb73115cdd36c91ff93e8e330d1ad0d10693db12f8ec4eef602b_prof);

        
        $__internal_9f48fa5c3e535002582e69ccfca149dec59ddb195c4bcf57b2bfd4b6b2a25a8c->leave($__internal_9f48fa5c3e535002582e69ccfca149dec59ddb195c4bcf57b2bfd4b6b2a25a8c_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:show.html.twig", "/home/timur/http/hw/hw64/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/show.html.twig");
    }
}
