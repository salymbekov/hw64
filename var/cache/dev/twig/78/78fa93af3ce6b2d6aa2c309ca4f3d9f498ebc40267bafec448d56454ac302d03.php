<?php

/* bootstrap_3_horizontal_layout.html.twig */
class __TwigTemplate_19c62d1aed936325699910e0a57a00a950a853f71d4c897e6c54feee79584b36 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("bootstrap_3_layout.html.twig", "bootstrap_3_horizontal_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."bootstrap_3_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_start' => array($this, 'block_form_start'),
                'form_label' => array($this, 'block_form_label'),
                'form_label_class' => array($this, 'block_form_label_class'),
                'form_row' => array($this, 'block_form_row'),
                'submit_row' => array($this, 'block_submit_row'),
                'reset_row' => array($this, 'block_reset_row'),
                'form_group_class' => array($this, 'block_form_group_class'),
                'checkbox_row' => array($this, 'block_checkbox_row'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_98bb3bafe925747118a87be9d39f3b8370e2e215c38ac9c6fc36129cbebbf69f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_98bb3bafe925747118a87be9d39f3b8370e2e215c38ac9c6fc36129cbebbf69f->enter($__internal_98bb3bafe925747118a87be9d39f3b8370e2e215c38ac9c6fc36129cbebbf69f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_3_horizontal_layout.html.twig"));

        $__internal_0e006574c5e585426577f33a60526a8dc4d6e17f060154619dd026dbabf08758 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0e006574c5e585426577f33a60526a8dc4d6e17f060154619dd026dbabf08758->enter($__internal_0e006574c5e585426577f33a60526a8dc4d6e17f060154619dd026dbabf08758_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_3_horizontal_layout.html.twig"));

        // line 2
        echo "
";
        // line 3
        $this->displayBlock('form_start', $context, $blocks);
        // line 7
        echo "
";
        // line 9
        echo "
";
        // line 10
        $this->displayBlock('form_label', $context, $blocks);
        // line 18
        echo "
";
        // line 19
        $this->displayBlock('form_label_class', $context, $blocks);
        // line 22
        echo "
";
        // line 24
        echo "
";
        // line 25
        $this->displayBlock('form_row', $context, $blocks);
        // line 34
        echo "
";
        // line 35
        $this->displayBlock('submit_row', $context, $blocks);
        // line 43
        echo "
";
        // line 44
        $this->displayBlock('reset_row', $context, $blocks);
        // line 52
        echo "
";
        // line 53
        $this->displayBlock('form_group_class', $context, $blocks);
        // line 56
        echo "
";
        // line 57
        $this->displayBlock('checkbox_row', $context, $blocks);
        
        $__internal_98bb3bafe925747118a87be9d39f3b8370e2e215c38ac9c6fc36129cbebbf69f->leave($__internal_98bb3bafe925747118a87be9d39f3b8370e2e215c38ac9c6fc36129cbebbf69f_prof);

        
        $__internal_0e006574c5e585426577f33a60526a8dc4d6e17f060154619dd026dbabf08758->leave($__internal_0e006574c5e585426577f33a60526a8dc4d6e17f060154619dd026dbabf08758_prof);

    }

    // line 3
    public function block_form_start($context, array $blocks = array())
    {
        $__internal_835b8bfaef67f4e89083ced2873a04cfa0de249acb3b5a388bc87d1e193e186e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_835b8bfaef67f4e89083ced2873a04cfa0de249acb3b5a388bc87d1e193e186e->enter($__internal_835b8bfaef67f4e89083ced2873a04cfa0de249acb3b5a388bc87d1e193e186e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_start"));

        $__internal_21aff66fc6131c0eb74c8002a5c84eeaeb9fd860dc25580abcbc206b8b2a7bea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_21aff66fc6131c0eb74c8002a5c84eeaeb9fd860dc25580abcbc206b8b2a7bea->enter($__internal_21aff66fc6131c0eb74c8002a5c84eeaeb9fd860dc25580abcbc206b8b2a7bea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_start"));

        // line 4
        $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-horizontal"))));
        // line 5
        $this->displayParentBlock("form_start", $context, $blocks);
        
        $__internal_21aff66fc6131c0eb74c8002a5c84eeaeb9fd860dc25580abcbc206b8b2a7bea->leave($__internal_21aff66fc6131c0eb74c8002a5c84eeaeb9fd860dc25580abcbc206b8b2a7bea_prof);

        
        $__internal_835b8bfaef67f4e89083ced2873a04cfa0de249acb3b5a388bc87d1e193e186e->leave($__internal_835b8bfaef67f4e89083ced2873a04cfa0de249acb3b5a388bc87d1e193e186e_prof);

    }

    // line 10
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_672eb98dc1014771d5b415d34662edecb01437e00674a472bb790b2eb398bfc3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_672eb98dc1014771d5b415d34662edecb01437e00674a472bb790b2eb398bfc3->enter($__internal_672eb98dc1014771d5b415d34662edecb01437e00674a472bb790b2eb398bfc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_057105800d235bfbe47454a7b0c8f1b0f5deaa58405ff4dcccf3e29e937163c7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_057105800d235bfbe47454a7b0c8f1b0f5deaa58405ff4dcccf3e29e937163c7->enter($__internal_057105800d235bfbe47454a7b0c8f1b0f5deaa58405ff4dcccf3e29e937163c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 11
        if ((($context["label"] ?? $this->getContext($context, "label")) === false)) {
            // line 12
            echo "<div class=\"";
            $this->displayBlock("form_label_class", $context, $blocks);
            echo "\"></div>";
        } else {
            // line 14
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter((((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " ") .             $this->renderBlock("form_label_class", $context, $blocks)))));
            // line 15
            $this->displayParentBlock("form_label", $context, $blocks);
        }
        
        $__internal_057105800d235bfbe47454a7b0c8f1b0f5deaa58405ff4dcccf3e29e937163c7->leave($__internal_057105800d235bfbe47454a7b0c8f1b0f5deaa58405ff4dcccf3e29e937163c7_prof);

        
        $__internal_672eb98dc1014771d5b415d34662edecb01437e00674a472bb790b2eb398bfc3->leave($__internal_672eb98dc1014771d5b415d34662edecb01437e00674a472bb790b2eb398bfc3_prof);

    }

    // line 19
    public function block_form_label_class($context, array $blocks = array())
    {
        $__internal_ddd6ccbbd4a2fc1494fa94d8e19c9f3b6b4f4c6b8f184bb917afa484da69b8bd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ddd6ccbbd4a2fc1494fa94d8e19c9f3b6b4f4c6b8f184bb917afa484da69b8bd->enter($__internal_ddd6ccbbd4a2fc1494fa94d8e19c9f3b6b4f4c6b8f184bb917afa484da69b8bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label_class"));

        $__internal_7471e28a35e8163d669041d05189cd1af33f12bbb5b03a6f6ce53b2ca055393d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7471e28a35e8163d669041d05189cd1af33f12bbb5b03a6f6ce53b2ca055393d->enter($__internal_7471e28a35e8163d669041d05189cd1af33f12bbb5b03a6f6ce53b2ca055393d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label_class"));

        // line 20
        echo "col-sm-2";
        
        $__internal_7471e28a35e8163d669041d05189cd1af33f12bbb5b03a6f6ce53b2ca055393d->leave($__internal_7471e28a35e8163d669041d05189cd1af33f12bbb5b03a6f6ce53b2ca055393d_prof);

        
        $__internal_ddd6ccbbd4a2fc1494fa94d8e19c9f3b6b4f4c6b8f184bb917afa484da69b8bd->leave($__internal_ddd6ccbbd4a2fc1494fa94d8e19c9f3b6b4f4c6b8f184bb917afa484da69b8bd_prof);

    }

    // line 25
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_08ccb7d5792840e3c7d0797ca6eac1c0833b168050fd82af2bb7369131808c28 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_08ccb7d5792840e3c7d0797ca6eac1c0833b168050fd82af2bb7369131808c28->enter($__internal_08ccb7d5792840e3c7d0797ca6eac1c0833b168050fd82af2bb7369131808c28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_25e3261d6def98f23157c7383ad01488fdca0a5287110e25a4ca7873a22ce80a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_25e3261d6def98f23157c7383ad01488fdca0a5287110e25a4ca7873a22ce80a->enter($__internal_25e3261d6def98f23157c7383ad01488fdca0a5287110e25a4ca7873a22ce80a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 26
        echo "<div class=\"form-group";
        if ((( !($context["compound"] ?? $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter(($context["force_error"] ?? $this->getContext($context, "force_error")), false)) : (false))) &&  !($context["valid"] ?? $this->getContext($context, "valid")))) {
            echo " has-error";
        }
        echo "\">";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label');
        // line 28
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 31
        echo "</div>
";
        // line 32
        echo "</div>";
        
        $__internal_25e3261d6def98f23157c7383ad01488fdca0a5287110e25a4ca7873a22ce80a->leave($__internal_25e3261d6def98f23157c7383ad01488fdca0a5287110e25a4ca7873a22ce80a_prof);

        
        $__internal_08ccb7d5792840e3c7d0797ca6eac1c0833b168050fd82af2bb7369131808c28->leave($__internal_08ccb7d5792840e3c7d0797ca6eac1c0833b168050fd82af2bb7369131808c28_prof);

    }

    // line 35
    public function block_submit_row($context, array $blocks = array())
    {
        $__internal_8f01213f975f0754906a46ab8ae432390b27fa9a80b191787e350b141bc52850 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8f01213f975f0754906a46ab8ae432390b27fa9a80b191787e350b141bc52850->enter($__internal_8f01213f975f0754906a46ab8ae432390b27fa9a80b191787e350b141bc52850_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_row"));

        $__internal_4cd08d1bfd6583d107e03c71e36c9b6f1970596fd4054f8cddf80910cb0e8bb2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4cd08d1bfd6583d107e03c71e36c9b6f1970596fd4054f8cddf80910cb0e8bb2->enter($__internal_4cd08d1bfd6583d107e03c71e36c9b6f1970596fd4054f8cddf80910cb0e8bb2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_row"));

        // line 36
        echo "<div class=\"form-group\">";
        // line 37
        echo "<div class=\"";
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>";
        // line 38
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 40
        echo "</div>";
        // line 41
        echo "</div>";
        
        $__internal_4cd08d1bfd6583d107e03c71e36c9b6f1970596fd4054f8cddf80910cb0e8bb2->leave($__internal_4cd08d1bfd6583d107e03c71e36c9b6f1970596fd4054f8cddf80910cb0e8bb2_prof);

        
        $__internal_8f01213f975f0754906a46ab8ae432390b27fa9a80b191787e350b141bc52850->leave($__internal_8f01213f975f0754906a46ab8ae432390b27fa9a80b191787e350b141bc52850_prof);

    }

    // line 44
    public function block_reset_row($context, array $blocks = array())
    {
        $__internal_cac00b7286c88ea399b643c91de6fda691f9e89548743f70adf197b4889759ac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cac00b7286c88ea399b643c91de6fda691f9e89548743f70adf197b4889759ac->enter($__internal_cac00b7286c88ea399b643c91de6fda691f9e89548743f70adf197b4889759ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "reset_row"));

        $__internal_ba64665248daa8bdd7a1ecfaced80bc393441fc1a11f8259e2cfb9e6eaa113a0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ba64665248daa8bdd7a1ecfaced80bc393441fc1a11f8259e2cfb9e6eaa113a0->enter($__internal_ba64665248daa8bdd7a1ecfaced80bc393441fc1a11f8259e2cfb9e6eaa113a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "reset_row"));

        // line 45
        echo "<div class=\"form-group\">";
        // line 46
        echo "<div class=\"";
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>";
        // line 47
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 48
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 49
        echo "</div>";
        // line 50
        echo "</div>";
        
        $__internal_ba64665248daa8bdd7a1ecfaced80bc393441fc1a11f8259e2cfb9e6eaa113a0->leave($__internal_ba64665248daa8bdd7a1ecfaced80bc393441fc1a11f8259e2cfb9e6eaa113a0_prof);

        
        $__internal_cac00b7286c88ea399b643c91de6fda691f9e89548743f70adf197b4889759ac->leave($__internal_cac00b7286c88ea399b643c91de6fda691f9e89548743f70adf197b4889759ac_prof);

    }

    // line 53
    public function block_form_group_class($context, array $blocks = array())
    {
        $__internal_5c2c2f45047a40646e0af358af46d059cdfb64e1b41b48d9fe5a19f642bb79df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5c2c2f45047a40646e0af358af46d059cdfb64e1b41b48d9fe5a19f642bb79df->enter($__internal_5c2c2f45047a40646e0af358af46d059cdfb64e1b41b48d9fe5a19f642bb79df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_group_class"));

        $__internal_3e21b18f30d608ab0fe850b3a061f3b492903e2f209ee6d39c471f55ea44b2c3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3e21b18f30d608ab0fe850b3a061f3b492903e2f209ee6d39c471f55ea44b2c3->enter($__internal_3e21b18f30d608ab0fe850b3a061f3b492903e2f209ee6d39c471f55ea44b2c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_group_class"));

        // line 54
        echo "col-sm-10";
        
        $__internal_3e21b18f30d608ab0fe850b3a061f3b492903e2f209ee6d39c471f55ea44b2c3->leave($__internal_3e21b18f30d608ab0fe850b3a061f3b492903e2f209ee6d39c471f55ea44b2c3_prof);

        
        $__internal_5c2c2f45047a40646e0af358af46d059cdfb64e1b41b48d9fe5a19f642bb79df->leave($__internal_5c2c2f45047a40646e0af358af46d059cdfb64e1b41b48d9fe5a19f642bb79df_prof);

    }

    // line 57
    public function block_checkbox_row($context, array $blocks = array())
    {
        $__internal_9ce1f8943620ad1e6095a0ae84ebebb7c700da044b5eae7553374244894b39cd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ce1f8943620ad1e6095a0ae84ebebb7c700da044b5eae7553374244894b39cd->enter($__internal_9ce1f8943620ad1e6095a0ae84ebebb7c700da044b5eae7553374244894b39cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        $__internal_6ee51207f45826bacb2816df2240e0ad60b8e617b0db20d0d067653fecc75aeb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6ee51207f45826bacb2816df2240e0ad60b8e617b0db20d0d067653fecc75aeb->enter($__internal_6ee51207f45826bacb2816df2240e0ad60b8e617b0db20d0d067653fecc75aeb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        // line 58
        echo "<div class=\"form-group";
        if ( !($context["valid"] ?? $this->getContext($context, "valid"))) {
            echo " has-error";
        }
        echo "\">";
        // line 59
        echo "<div class=\"";
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>";
        // line 60
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 61
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 62
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 63
        echo "</div>";
        // line 64
        echo "</div>";
        
        $__internal_6ee51207f45826bacb2816df2240e0ad60b8e617b0db20d0d067653fecc75aeb->leave($__internal_6ee51207f45826bacb2816df2240e0ad60b8e617b0db20d0d067653fecc75aeb_prof);

        
        $__internal_9ce1f8943620ad1e6095a0ae84ebebb7c700da044b5eae7553374244894b39cd->leave($__internal_9ce1f8943620ad1e6095a0ae84ebebb7c700da044b5eae7553374244894b39cd_prof);

    }

    public function getTemplateName()
    {
        return "bootstrap_3_horizontal_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  320 => 64,  318 => 63,  316 => 62,  314 => 61,  310 => 60,  306 => 59,  300 => 58,  291 => 57,  281 => 54,  272 => 53,  262 => 50,  260 => 49,  258 => 48,  254 => 47,  250 => 46,  248 => 45,  239 => 44,  229 => 41,  227 => 40,  225 => 39,  221 => 38,  217 => 37,  215 => 36,  206 => 35,  196 => 32,  193 => 31,  191 => 30,  189 => 29,  185 => 28,  183 => 27,  177 => 26,  168 => 25,  158 => 20,  149 => 19,  138 => 15,  136 => 14,  131 => 12,  129 => 11,  120 => 10,  110 => 5,  108 => 4,  99 => 3,  89 => 57,  86 => 56,  84 => 53,  81 => 52,  79 => 44,  76 => 43,  74 => 35,  71 => 34,  69 => 25,  66 => 24,  63 => 22,  61 => 19,  58 => 18,  56 => 10,  53 => 9,  50 => 7,  48 => 3,  45 => 2,  14 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"bootstrap_3_layout.html.twig\" %}

{% block form_start -%}
    {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-horizontal')|trim}) %}
    {{- parent() -}}
{%- endblock form_start %}

{# Labels #}

{% block form_label -%}
    {%- if label is same as(false) -%}
        <div class=\"{{ block('form_label_class') }}\"></div>
    {%- else -%}
        {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' ' ~ block('form_label_class'))|trim}) -%}
        {{- parent() -}}
    {%- endif -%}
{%- endblock form_label %}

{% block form_label_class -%}
col-sm-2
{%- endblock form_label_class %}

{# Rows #}

{% block form_row -%}
    <div class=\"form-group{% if (not compound or force_error|default(false)) and not valid %} has-error{% endif %}\">
        {{- form_label(form) -}}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
            {{- form_errors(form) -}}
        </div>
{##}</div>
{%- endblock form_row %}

{% block submit_row -%}
    <div class=\"form-group\">{#--#}
        <div class=\"{{ block('form_label_class') }}\"></div>{#--#}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
        </div>{#--#}
    </div>
{%- endblock submit_row %}

{% block reset_row -%}
    <div class=\"form-group\">{#--#}
        <div class=\"{{ block('form_label_class') }}\"></div>{#--#}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
        </div>{#--#}
    </div>
{%- endblock reset_row %}

{% block form_group_class -%}
col-sm-10
{%- endblock form_group_class %}

{% block checkbox_row -%}
    <div class=\"form-group{% if not valid %} has-error{% endif %}\">{#--#}
        <div class=\"{{ block('form_label_class') }}\"></div>{#--#}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
            {{- form_errors(form) -}}
        </div>{#--#}
    </div>
{%- endblock checkbox_row %}
", "bootstrap_3_horizontal_layout.html.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bridge/Twig/Resources/views/Form/bootstrap_3_horizontal_layout.html.twig");
    }
}
