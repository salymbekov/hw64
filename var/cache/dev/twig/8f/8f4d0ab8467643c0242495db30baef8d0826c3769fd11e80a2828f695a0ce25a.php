<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_1791f8fce2f29df1073aa0fbf3794e13fae759f6384ff6d0108b96a61835ac6e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0a21bed2b970741c08381e8a526a5f4e858e0610f8a2c721659fa83b84631dd6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0a21bed2b970741c08381e8a526a5f4e858e0610f8a2c721659fa83b84631dd6->enter($__internal_0a21bed2b970741c08381e8a526a5f4e858e0610f8a2c721659fa83b84631dd6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        $__internal_e0a28a599b6152b1240c1fbba1a5f57b106d18ababc6669b92a315293202bf17 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e0a28a599b6152b1240c1fbba1a5f57b106d18ababc6669b92a315293202bf17->enter($__internal_e0a28a599b6152b1240c1fbba1a5f57b106d18ababc6669b92a315293202bf17_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_0a21bed2b970741c08381e8a526a5f4e858e0610f8a2c721659fa83b84631dd6->leave($__internal_0a21bed2b970741c08381e8a526a5f4e858e0610f8a2c721659fa83b84631dd6_prof);

        
        $__internal_e0a28a599b6152b1240c1fbba1a5f57b106d18ababc6669b92a315293202bf17->leave($__internal_e0a28a599b6152b1240c1fbba1a5f57b106d18ababc6669b92a315293202bf17_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
", "@Framework/Form/form_end.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_end.html.php");
    }
}
