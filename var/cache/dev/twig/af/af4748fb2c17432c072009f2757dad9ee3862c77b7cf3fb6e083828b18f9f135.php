<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_63d39a4a6a523b54272b4f820d789d8f0f05f9242ff0eb24bbd8757af439df06 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_005aeb4c7a55626fd728c9fc28b2ee5055b24366b923cf6a973dcb49952a4014 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_005aeb4c7a55626fd728c9fc28b2ee5055b24366b923cf6a973dcb49952a4014->enter($__internal_005aeb4c7a55626fd728c9fc28b2ee5055b24366b923cf6a973dcb49952a4014_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        $__internal_412e98d13616175536eee0af3a563e76dbd41f331048eb335c8f2bbd02a518c8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_412e98d13616175536eee0af3a563e76dbd41f331048eb335c8f2bbd02a518c8->enter($__internal_412e98d13616175536eee0af3a563e76dbd41f331048eb335c8f2bbd02a518c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_005aeb4c7a55626fd728c9fc28b2ee5055b24366b923cf6a973dcb49952a4014->leave($__internal_005aeb4c7a55626fd728c9fc28b2ee5055b24366b923cf6a973dcb49952a4014_prof);

        
        $__internal_412e98d13616175536eee0af3a563e76dbd41f331048eb335c8f2bbd02a518c8->leave($__internal_412e98d13616175536eee0af3a563e76dbd41f331048eb335c8f2bbd02a518c8_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "TwigBundle:Exception:error.atom.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.atom.twig");
    }
}
