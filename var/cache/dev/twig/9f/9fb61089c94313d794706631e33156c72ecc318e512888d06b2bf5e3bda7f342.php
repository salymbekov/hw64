<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_8afc9ba48a750eb40cce0c939d429db686b8ef685461b4f5cd8b07415c072504 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a298440f12f676580ca291a92f51ee8804602bab7960d16ff006e227787645c4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a298440f12f676580ca291a92f51ee8804602bab7960d16ff006e227787645c4->enter($__internal_a298440f12f676580ca291a92f51ee8804602bab7960d16ff006e227787645c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        $__internal_fdad2b503efeacb5122a584517b5a6122b7017f56633cb94abc4f0eeaa4cf0db = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fdad2b503efeacb5122a584517b5a6122b7017f56633cb94abc4f0eeaa4cf0db->enter($__internal_fdad2b503efeacb5122a584517b5a6122b7017f56633cb94abc4f0eeaa4cf0db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
";
        
        $__internal_a298440f12f676580ca291a92f51ee8804602bab7960d16ff006e227787645c4->leave($__internal_a298440f12f676580ca291a92f51ee8804602bab7960d16ff006e227787645c4_prof);

        
        $__internal_fdad2b503efeacb5122a584517b5a6122b7017f56633cb94abc4f0eeaa4cf0db->leave($__internal_fdad2b503efeacb5122a584517b5a6122b7017f56633cb94abc4f0eeaa4cf0db_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "TwigBundle:Exception:exception.rdf.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.rdf.twig");
    }
}
