<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_6f28ee296b9858842dc258e04ee42dc2ac3f686b79c608bd8b804a71dea71af4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Security:login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c1159eb55dba282170a3f8b95d146abd4e3a88e945a42a35d3b4e227a690ce1d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c1159eb55dba282170a3f8b95d146abd4e3a88e945a42a35d3b4e227a690ce1d->enter($__internal_c1159eb55dba282170a3f8b95d146abd4e3a88e945a42a35d3b4e227a690ce1d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        $__internal_c1fc69080681d15812774576d69922d726262487a2424caa2fb97213b0b0bc01 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c1fc69080681d15812774576d69922d726262487a2424caa2fb97213b0b0bc01->enter($__internal_c1fc69080681d15812774576d69922d726262487a2424caa2fb97213b0b0bc01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c1159eb55dba282170a3f8b95d146abd4e3a88e945a42a35d3b4e227a690ce1d->leave($__internal_c1159eb55dba282170a3f8b95d146abd4e3a88e945a42a35d3b4e227a690ce1d_prof);

        
        $__internal_c1fc69080681d15812774576d69922d726262487a2424caa2fb97213b0b0bc01->leave($__internal_c1fc69080681d15812774576d69922d726262487a2424caa2fb97213b0b0bc01_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_74437abf4a8c671358e94cd160250629dfbf10c05d037deb3eee327c61b163ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_74437abf4a8c671358e94cd160250629dfbf10c05d037deb3eee327c61b163ef->enter($__internal_74437abf4a8c671358e94cd160250629dfbf10c05d037deb3eee327c61b163ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_1413057284227debb9ba803cadc83572a096c5971184965f8d5971d4e10e2c19 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1413057284227debb9ba803cadc83572a096c5971184965f8d5971d4e10e2c19->enter($__internal_1413057284227debb9ba803cadc83572a096c5971184965f8d5971d4e10e2c19_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_1413057284227debb9ba803cadc83572a096c5971184965f8d5971d4e10e2c19->leave($__internal_1413057284227debb9ba803cadc83572a096c5971184965f8d5971d4e10e2c19_prof);

        
        $__internal_74437abf4a8c671358e94cd160250629dfbf10c05d037deb3eee327c61b163ef->leave($__internal_74437abf4a8c671358e94cd160250629dfbf10c05d037deb3eee327c61b163ef_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}
", "FOSUserBundle:Security:login.html.twig", "/home/timur/http/hw/hw64/vendor/friendsofsymfony/user-bundle/Resources/views/Security/login.html.twig");
    }
}
