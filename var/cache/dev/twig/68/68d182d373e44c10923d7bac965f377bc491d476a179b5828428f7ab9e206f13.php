<?php

/* AppBundle:Words:translate.html.twig */
class __TwigTemplate_8082be6e4c2d3c9f1dbd62f6ffe78efbc867d67d5ca70f59bf133554d6b45b38 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "AppBundle:Words:translate.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_66816610b00095d3626dd747dfdb9668aa9b7661c66ea5f0bebdc33eff4d94ac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_66816610b00095d3626dd747dfdb9668aa9b7661c66ea5f0bebdc33eff4d94ac->enter($__internal_66816610b00095d3626dd747dfdb9668aa9b7661c66ea5f0bebdc33eff4d94ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Words:translate.html.twig"));

        $__internal_f471f4e13dd73319903e11d64aad2685fed1fcf40093e9713929652c99dd915f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f471f4e13dd73319903e11d64aad2685fed1fcf40093e9713929652c99dd915f->enter($__internal_f471f4e13dd73319903e11d64aad2685fed1fcf40093e9713929652c99dd915f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Words:translate.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_66816610b00095d3626dd747dfdb9668aa9b7661c66ea5f0bebdc33eff4d94ac->leave($__internal_66816610b00095d3626dd747dfdb9668aa9b7661c66ea5f0bebdc33eff4d94ac_prof);

        
        $__internal_f471f4e13dd73319903e11d64aad2685fed1fcf40093e9713929652c99dd915f->leave($__internal_f471f4e13dd73319903e11d64aad2685fed1fcf40093e9713929652c99dd915f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_6a87439a7fd977a632b754f4f61e75719c418c79b526bc9f93223821050bf14d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6a87439a7fd977a632b754f4f61e75719c418c79b526bc9f93223821050bf14d->enter($__internal_6a87439a7fd977a632b754f4f61e75719c418c79b526bc9f93223821050bf14d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_39ccd0d37752d97dfe351f46144d210e9638984a8b376d156d36dbdd0c0c5e7a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_39ccd0d37752d97dfe351f46144d210e9638984a8b376d156d36dbdd0c0c5e7a->enter($__internal_39ccd0d37752d97dfe351f46144d210e9638984a8b376d156d36dbdd0c0c5e7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "AppBundle:Words:translate";
        
        $__internal_39ccd0d37752d97dfe351f46144d210e9638984a8b376d156d36dbdd0c0c5e7a->leave($__internal_39ccd0d37752d97dfe351f46144d210e9638984a8b376d156d36dbdd0c0c5e7a_prof);

        
        $__internal_6a87439a7fd977a632b754f4f61e75719c418c79b526bc9f93223821050bf14d->leave($__internal_6a87439a7fd977a632b754f4f61e75719c418c79b526bc9f93223821050bf14d_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_01c2b2e9382be63419b1463b9d432c520d7a0290f8401b70f892716cdafedd14 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_01c2b2e9382be63419b1463b9d432c520d7a0290f8401b70f892716cdafedd14->enter($__internal_01c2b2e9382be63419b1463b9d432c520d7a0290f8401b70f892716cdafedd14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_752d55c8e001a66383b4a4d88775ea4720db983cc72af5eee72cf431e64bd8b1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_752d55c8e001a66383b4a4d88775ea4720db983cc72af5eee72cf431e64bd8b1->enter($__internal_752d55c8e001a66383b4a4d88775ea4720db983cc72af5eee72cf431e64bd8b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <p>";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("Переводы слова", array(), "messages");
        echo ": <b>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["words"] ?? $this->getContext($context, "words")), "translate", array(0 => "ru"), "method"), "word", array()), "html", null, true);
        echo "</b></p>


    ";
        // line 9
        if ($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array())) {
            // line 10
            echo "
        ";
            // line 11
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
            echo "
        <p>
            ";
            // line 13
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "enWord", array()), 'label');
            echo "
            ";
            // line 14
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "enWord", array()), 'widget', array("attr" => array("value" => $this->getAttribute($this->getAttribute(($context["words"] ?? $this->getContext($context, "words")), "translate", array(0 => "en", 1 => false), "method"), "word", array()))));
            echo "
        </p>
        <p>
            ";
            // line 17
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "frWord", array()), 'label');
            echo "
            ";
            // line 18
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "frWord", array()), 'widget', array("attr" => array("value" => $this->getAttribute($this->getAttribute(($context["words"] ?? $this->getContext($context, "words")), "translate", array(0 => "fr", 1 => false), "method"), "word", array()))));
            echo "
        </p>
        <p>
            ";
            // line 21
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "spWord", array()), 'label');
            echo "
            ";
            // line 22
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "spWord", array()), 'widget', array("attr" => array("value" => $this->getAttribute($this->getAttribute(($context["words"] ?? $this->getContext($context, "words")), "translate", array(0 => "sp", 1 => false), "method"), "word", array()))));
            echo "
        </p>
        <p>
            ";
            // line 25
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "grWord", array()), 'label');
            echo "
            ";
            // line 26
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "grWord", array()), 'widget', array("attr" => array("value" => $this->getAttribute($this->getAttribute(($context["words"] ?? $this->getContext($context, "words")), "translate", array(0 => "gr", 1 => false), "method"), "word", array()))));
            echo "
        </p>
        <p>
            ";
            // line 29
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "itWord", array()), 'label');
            echo "
            ";
            // line 30
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "itWord", array()), 'widget', array("attr" => array("value" => $this->getAttribute($this->getAttribute(($context["words"] ?? $this->getContext($context, "words")), "translate", array(0 => "it", 1 => false), "method"), "word", array()))));
            echo "
        </p>
        <p>
            ";
            // line 33
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
            echo "
        </p>
        ";
        } else {
            // line 36
            echo "        <p>";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("перевод на английсский", array(), "messages");
            echo ": ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["words"] ?? $this->getContext($context, "words")), "translate", array(0 => "en", 1 => false), "method"), "word", array()), "html", null, true);
            echo "</p>
        <p>";
            // line 37
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("перевод на французский", array(), "messages");
            echo ":";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["words"] ?? $this->getContext($context, "words")), "translate", array(0 => "fr", 1 => false), "method"), "word", array()), "html", null, true);
            echo "</p>
        <p>";
            // line 38
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("перевод на испанский", array(), "messages");
            echo ":";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["words"] ?? $this->getContext($context, "words")), "translate", array(0 => "sp", 1 => false), "method"), "word", array()), "html", null, true);
            echo "</p>
        <p>";
            // line 39
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("перевод на греческий", array(), "messages");
            echo ":";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["words"] ?? $this->getContext($context, "words")), "translate", array(0 => "gr", 1 => false), "method"), "word", array()), "html", null, true);
            echo "</p>
        <p>";
            // line 40
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("перевод на итальянский", array(), "messages");
            echo ":";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["words"] ?? $this->getContext($context, "words")), "translate", array(0 => "it", 1 => false), "method"), "word", array()), "html", null, true);
            echo "</p>
    ";
        }
        // line 42
        echo "
";
        
        $__internal_752d55c8e001a66383b4a4d88775ea4720db983cc72af5eee72cf431e64bd8b1->leave($__internal_752d55c8e001a66383b4a4d88775ea4720db983cc72af5eee72cf431e64bd8b1_prof);

        
        $__internal_01c2b2e9382be63419b1463b9d432c520d7a0290f8401b70f892716cdafedd14->leave($__internal_01c2b2e9382be63419b1463b9d432c520d7a0290f8401b70f892716cdafedd14_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Words:translate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  175 => 42,  168 => 40,  162 => 39,  156 => 38,  150 => 37,  143 => 36,  137 => 33,  131 => 30,  127 => 29,  121 => 26,  117 => 25,  111 => 22,  107 => 21,  101 => 18,  97 => 17,  91 => 14,  87 => 13,  82 => 11,  79 => 10,  77 => 9,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base.html.twig\" %}

{% block title %}AppBundle:Words:translate{% endblock %}

{% block body %}
    <p>{% trans %}Переводы слова{% endtrans %}: <b>{{ words.translate('ru').word }}</b></p>


    {% if app.user %}

        {{ form_start(form) }}
        <p>
            {{ form_label(form.enWord) }}
            {{ form_widget(form.enWord, {'attr':{'value': words.translate('en',false).word}}) }}
        </p>
        <p>
            {{ form_label(form.frWord) }}
            {{ form_widget(form.frWord, {'attr':{'value': words.translate('fr',false).word}}) }}
        </p>
        <p>
            {{ form_label(form.spWord) }}
            {{ form_widget(form.spWord, {'attr':{'value': words.translate('sp',false).word}}) }}
        </p>
        <p>
            {{ form_label(form.grWord) }}
            {{ form_widget(form.grWord, {'attr':{'value': words.translate('gr',false).word}}) }}
        </p>
        <p>
            {{ form_label(form.itWord) }}
            {{ form_widget(form.itWord, {'attr':{'value': words.translate('it',false).word}}) }}
        </p>
        <p>
            {{ form_end(form) }}
        </p>
        {% else %}
        <p>{% trans %}перевод на английсский{% endtrans %}: {{ words.translate('en', false).word }}</p>
        <p>{% trans %}перевод на французский{% endtrans %}:{{ words.translate('fr', false).word }}</p>
        <p>{% trans %}перевод на испанский{% endtrans %}:{{ words.translate('sp', false).word }}</p>
        <p>{% trans %}перевод на греческий{% endtrans %}:{{ words.translate('gr', false).word }}</p>
        <p>{% trans %}перевод на итальянский{% endtrans %}:{{ words.translate('it', false).word }}</p>
    {% endif %}

{% endblock %}
", "AppBundle:Words:translate.html.twig", "/home/timur/http/hw/hw64/src/AppBundle/Resources/views/Words/translate.html.twig");
    }
}
