<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_6dbc9b3fc35b5dd2ebec565f417a98126669dc30f3d6265a341150149f5e5a9a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_987234d771bfd89f73e406b516ec219b9c032940271ab3e23230df41b3d1ac51 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_987234d771bfd89f73e406b516ec219b9c032940271ab3e23230df41b3d1ac51->enter($__internal_987234d771bfd89f73e406b516ec219b9c032940271ab3e23230df41b3d1ac51_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        $__internal_be8d36910e74748a936b8c9f6e3671ee84d848cddea04b33781250f96149aa6b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_be8d36910e74748a936b8c9f6e3671ee84d848cddea04b33781250f96149aa6b->enter($__internal_be8d36910e74748a936b8c9f6e3671ee84d848cddea04b33781250f96149aa6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_987234d771bfd89f73e406b516ec219b9c032940271ab3e23230df41b3d1ac51->leave($__internal_987234d771bfd89f73e406b516ec219b9c032940271ab3e23230df41b3d1ac51_prof);

        
        $__internal_be8d36910e74748a936b8c9f6e3671ee84d848cddea04b33781250f96149aa6b->leave($__internal_be8d36910e74748a936b8c9f6e3671ee84d848cddea04b33781250f96149aa6b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
", "@Framework/Form/form_rows.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rows.html.php");
    }
}
