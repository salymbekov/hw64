<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_c3482120444ec4212f29fd72d5c8cdf229954491febc4d8d5462805f6ca11976 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9482d13d8252c87451be3576236827e0fd36eb04926f7c9d9a799b93647dfc4e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9482d13d8252c87451be3576236827e0fd36eb04926f7c9d9a799b93647dfc4e->enter($__internal_9482d13d8252c87451be3576236827e0fd36eb04926f7c9d9a799b93647dfc4e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        $__internal_53cb654df7f87059f75026259cd356a95d5b70de51bd0d390e439db8fd095178 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_53cb654df7f87059f75026259cd356a95d5b70de51bd0d390e439db8fd095178->enter($__internal_53cb654df7f87059f75026259cd356a95d5b70de51bd0d390e439db8fd095178_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_9482d13d8252c87451be3576236827e0fd36eb04926f7c9d9a799b93647dfc4e->leave($__internal_9482d13d8252c87451be3576236827e0fd36eb04926f7c9d9a799b93647dfc4e_prof);

        
        $__internal_53cb654df7f87059f75026259cd356a95d5b70de51bd0d390e439db8fd095178->leave($__internal_53cb654df7f87059f75026259cd356a95d5b70de51bd0d390e439db8fd095178_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/form_row.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_row.html.php");
    }
}
