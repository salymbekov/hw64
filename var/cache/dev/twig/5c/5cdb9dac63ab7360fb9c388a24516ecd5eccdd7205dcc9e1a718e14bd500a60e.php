<?php

/* @Framework/Form/choice_attributes.html.php */
class __TwigTemplate_4fe3abab2768d3ab35c63193568bba9779994cbee1c3ce6170c928ed3c4d41b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ac6b8ac6706e7966066ec2a6f3ad3cda399ea8655556ab8dd5148fdb0fe6022e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ac6b8ac6706e7966066ec2a6f3ad3cda399ea8655556ab8dd5148fdb0fe6022e->enter($__internal_ac6b8ac6706e7966066ec2a6f3ad3cda399ea8655556ab8dd5148fdb0fe6022e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        $__internal_fbe12f5e3bf57abb66694973975627fc4c1187747eed564ad18fc442d62f1ec8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fbe12f5e3bf57abb66694973975627fc4c1187747eed564ad18fc442d62f1ec8->enter($__internal_fbe12f5e3bf57abb66694973975627fc4c1187747eed564ad18fc442d62f1ec8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        // line 1
        echo "<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
";
        
        $__internal_ac6b8ac6706e7966066ec2a6f3ad3cda399ea8655556ab8dd5148fdb0fe6022e->leave($__internal_ac6b8ac6706e7966066ec2a6f3ad3cda399ea8655556ab8dd5148fdb0fe6022e_prof);

        
        $__internal_fbe12f5e3bf57abb66694973975627fc4c1187747eed564ad18fc442d62f1ec8->leave($__internal_fbe12f5e3bf57abb66694973975627fc4c1187747eed564ad18fc442d62f1ec8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
", "@Framework/Form/choice_attributes.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_attributes.html.php");
    }
}
