<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_5419521521c74fff6338e0a72f697c63a31ee3f963aa66622abcf0d598a253e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3fb7c90143eea1d3f2665d015905d89d34fc5675311219d0d1e60b99f16e2ff1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3fb7c90143eea1d3f2665d015905d89d34fc5675311219d0d1e60b99f16e2ff1->enter($__internal_3fb7c90143eea1d3f2665d015905d89d34fc5675311219d0d1e60b99f16e2ff1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        $__internal_8c354f1c48c561acea0cb3435861910895f9baca5cd0bcd75eb460badbf6ced8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8c354f1c48c561acea0cb3435861910895f9baca5cd0bcd75eb460badbf6ced8->enter($__internal_8c354f1c48c561acea0cb3435861910895f9baca5cd0bcd75eb460badbf6ced8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_3fb7c90143eea1d3f2665d015905d89d34fc5675311219d0d1e60b99f16e2ff1->leave($__internal_3fb7c90143eea1d3f2665d015905d89d34fc5675311219d0d1e60b99f16e2ff1_prof);

        
        $__internal_8c354f1c48c561acea0cb3435861910895f9baca5cd0bcd75eb460badbf6ced8->leave($__internal_8c354f1c48c561acea0cb3435861910895f9baca5cd0bcd75eb460badbf6ced8_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_2a323f0affdc5278a193679dea61564c9b6abf8aa88c793ff4d2c86d4eb8c932 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a323f0affdc5278a193679dea61564c9b6abf8aa88c793ff4d2c86d4eb8c932->enter($__internal_2a323f0affdc5278a193679dea61564c9b6abf8aa88c793ff4d2c86d4eb8c932_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_f1b1759a4e616d98498f61318d66f1f99fc086c1ef51cac40448f12edfb84935 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f1b1759a4e616d98498f61318d66f1f99fc086c1ef51cac40448f12edfb84935->enter($__internal_f1b1759a4e616d98498f61318d66f1f99fc086c1ef51cac40448f12edfb84935_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_f1b1759a4e616d98498f61318d66f1f99fc086c1ef51cac40448f12edfb84935->leave($__internal_f1b1759a4e616d98498f61318d66f1f99fc086c1ef51cac40448f12edfb84935_prof);

        
        $__internal_2a323f0affdc5278a193679dea61564c9b6abf8aa88c793ff4d2c86d4eb8c932->leave($__internal_2a323f0affdc5278a193679dea61564c9b6abf8aa88c793ff4d2c86d4eb8c932_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
