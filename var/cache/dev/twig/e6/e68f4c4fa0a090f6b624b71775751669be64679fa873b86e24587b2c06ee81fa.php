<?php

/* @Framework/Form/widget_container_attributes.html.php */
class __TwigTemplate_0bb55643e483a7dabf8c499a42412ea024baf7ad90f3594cccb08bf07d3f184f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_94dbbf60c6089ac657a5d6962e804fa17ff223ffeaa8fd3ec7491674a5548be2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_94dbbf60c6089ac657a5d6962e804fa17ff223ffeaa8fd3ec7491674a5548be2->enter($__internal_94dbbf60c6089ac657a5d6962e804fa17ff223ffeaa8fd3ec7491674a5548be2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_container_attributes.html.php"));

        $__internal_969371e5891acf3f9568b70a7e1e634b9429d642bc9ae5e0e40bbdcbf0b9345e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_969371e5891acf3f9568b70a7e1e634b9429d642bc9ae5e0e40bbdcbf0b9345e->enter($__internal_969371e5891acf3f9568b70a7e1e634b9429d642bc9ae5e0e40bbdcbf0b9345e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_container_attributes.html.php"));

        // line 1
        echo "<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_94dbbf60c6089ac657a5d6962e804fa17ff223ffeaa8fd3ec7491674a5548be2->leave($__internal_94dbbf60c6089ac657a5d6962e804fa17ff223ffeaa8fd3ec7491674a5548be2_prof);

        
        $__internal_969371e5891acf3f9568b70a7e1e634b9429d642bc9ae5e0e40bbdcbf0b9345e->leave($__internal_969371e5891acf3f9568b70a7e1e634b9429d642bc9ae5e0e40bbdcbf0b9345e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/widget_container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/widget_container_attributes.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/widget_container_attributes.html.php");
    }
}
