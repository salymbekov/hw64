<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_c4d0c44e862019ce08693ee9dca079b7e841d7867e67e7bdbe199f8e3829926a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_84f47115b181a4b441436cd019f3b2be0eeb8dd4eabe2740f0faa7821297dc51 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_84f47115b181a4b441436cd019f3b2be0eeb8dd4eabe2740f0faa7821297dc51->enter($__internal_84f47115b181a4b441436cd019f3b2be0eeb8dd4eabe2740f0faa7821297dc51_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        $__internal_402e6fadb15d94ba04b65c27ad7f12d97db55cccef2175a2d06cdaed925bc41e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_402e6fadb15d94ba04b65c27ad7f12d97db55cccef2175a2d06cdaed925bc41e->enter($__internal_402e6fadb15d94ba04b65c27ad7f12d97db55cccef2175a2d06cdaed925bc41e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_84f47115b181a4b441436cd019f3b2be0eeb8dd4eabe2740f0faa7821297dc51->leave($__internal_84f47115b181a4b441436cd019f3b2be0eeb8dd4eabe2740f0faa7821297dc51_prof);

        
        $__internal_402e6fadb15d94ba04b65c27ad7f12d97db55cccef2175a2d06cdaed925bc41e->leave($__internal_402e6fadb15d94ba04b65c27ad7f12d97db55cccef2175a2d06cdaed925bc41e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_row.html.php");
    }
}
