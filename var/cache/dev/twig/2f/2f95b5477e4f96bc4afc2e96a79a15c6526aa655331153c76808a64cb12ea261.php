<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_9badba71597738567e101e71f829a8a62c3f17dc7c468b28c1ad0cc9f5dea753 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_38696666ab9fb3b91f06ac9df096eb9a3c8a280d339f789ca7cd8568ad9ad1c8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_38696666ab9fb3b91f06ac9df096eb9a3c8a280d339f789ca7cd8568ad9ad1c8->enter($__internal_38696666ab9fb3b91f06ac9df096eb9a3c8a280d339f789ca7cd8568ad9ad1c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        $__internal_f3c65e7bf5c0116cbea4ab31f3c299d6140aec7d1e1ffefffef88f9a18d78f6b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f3c65e7bf5c0116cbea4ab31f3c299d6140aec7d1e1ffefffef88f9a18d78f6b->enter($__internal_f3c65e7bf5c0116cbea4ab31f3c299d6140aec7d1e1ffefffef88f9a18d78f6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => ($context["status_code"] ?? $this->getContext($context, "status_code")), "message" => ($context["status_text"] ?? $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_38696666ab9fb3b91f06ac9df096eb9a3c8a280d339f789ca7cd8568ad9ad1c8->leave($__internal_38696666ab9fb3b91f06ac9df096eb9a3c8a280d339f789ca7cd8568ad9ad1c8_prof);

        
        $__internal_f3c65e7bf5c0116cbea4ab31f3c299d6140aec7d1e1ffefffef88f9a18d78f6b->leave($__internal_f3c65e7bf5c0116cbea4ab31f3c299d6140aec7d1e1ffefffef88f9a18d78f6b_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}
", "TwigBundle:Exception:error.json.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.json.twig");
    }
}
