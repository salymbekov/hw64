<?php

/* @Framework/Form/button_attributes.html.php */
class __TwigTemplate_6e07aac54b608940e1bd7eba7568191312de1ce30240a77ba5422a7628545315 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3493c802f3e754f0195e234a51010b3d9618b8687a8a828ba6c959bdca85f99d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3493c802f3e754f0195e234a51010b3d9618b8687a8a828ba6c959bdca85f99d->enter($__internal_3493c802f3e754f0195e234a51010b3d9618b8687a8a828ba6c959bdca85f99d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_attributes.html.php"));

        $__internal_6f76c488da03863ea18d49e4bdd7ce13cf11452ae4eed92e423523a1d2cea4b0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6f76c488da03863ea18d49e4bdd7ce13cf11452ae4eed92e423523a1d2cea4b0->enter($__internal_6f76c488da03863ea18d49e4bdd7ce13cf11452ae4eed92e423523a1d2cea4b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_attributes.html.php"));

        // line 1
        echo "id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_3493c802f3e754f0195e234a51010b3d9618b8687a8a828ba6c959bdca85f99d->leave($__internal_3493c802f3e754f0195e234a51010b3d9618b8687a8a828ba6c959bdca85f99d_prof);

        
        $__internal_6f76c488da03863ea18d49e4bdd7ce13cf11452ae4eed92e423523a1d2cea4b0->leave($__internal_6f76c488da03863ea18d49e4bdd7ce13cf11452ae4eed92e423523a1d2cea4b0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/button_attributes.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_attributes.html.php");
    }
}
