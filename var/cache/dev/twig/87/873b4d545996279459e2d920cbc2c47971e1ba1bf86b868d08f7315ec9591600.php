<?php

/* FOSUserBundle:Group:new.html.twig */
class __TwigTemplate_30ce12be6d0679cd264cdf9577c5deb2280fa73b87ae70ed9449f3d7fc32db9a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f1faf164e869d5b710a9bf7c2ab22a52972234c564168288fec9f80247588f9f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f1faf164e869d5b710a9bf7c2ab22a52972234c564168288fec9f80247588f9f->enter($__internal_f1faf164e869d5b710a9bf7c2ab22a52972234c564168288fec9f80247588f9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $__internal_dfd6d17d11ffe31ebb3a7bd438ca20cbebd007d1cef23edf99c877fa915ee8e3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dfd6d17d11ffe31ebb3a7bd438ca20cbebd007d1cef23edf99c877fa915ee8e3->enter($__internal_dfd6d17d11ffe31ebb3a7bd438ca20cbebd007d1cef23edf99c877fa915ee8e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f1faf164e869d5b710a9bf7c2ab22a52972234c564168288fec9f80247588f9f->leave($__internal_f1faf164e869d5b710a9bf7c2ab22a52972234c564168288fec9f80247588f9f_prof);

        
        $__internal_dfd6d17d11ffe31ebb3a7bd438ca20cbebd007d1cef23edf99c877fa915ee8e3->leave($__internal_dfd6d17d11ffe31ebb3a7bd438ca20cbebd007d1cef23edf99c877fa915ee8e3_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_826d3189f58ed2ebb9d1190192fff83d7fd597f01648647456783d5abf04ff6d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_826d3189f58ed2ebb9d1190192fff83d7fd597f01648647456783d5abf04ff6d->enter($__internal_826d3189f58ed2ebb9d1190192fff83d7fd597f01648647456783d5abf04ff6d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_5ef7b06124c9672c9c9d012043643304d0a2c0d36bfeb49f44f22140f2c8427c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5ef7b06124c9672c9c9d012043643304d0a2c0d36bfeb49f44f22140f2c8427c->enter($__internal_5ef7b06124c9672c9c9d012043643304d0a2c0d36bfeb49f44f22140f2c8427c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/new_content.html.twig", "FOSUserBundle:Group:new.html.twig", 4)->display($context);
        
        $__internal_5ef7b06124c9672c9c9d012043643304d0a2c0d36bfeb49f44f22140f2c8427c->leave($__internal_5ef7b06124c9672c9c9d012043643304d0a2c0d36bfeb49f44f22140f2c8427c_prof);

        
        $__internal_826d3189f58ed2ebb9d1190192fff83d7fd597f01648647456783d5abf04ff6d->leave($__internal_826d3189f58ed2ebb9d1190192fff83d7fd597f01648647456783d5abf04ff6d_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/new_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:new.html.twig", "/home/timur/http/hw/hw64/vendor/friendsofsymfony/user-bundle/Resources/views/Group/new.html.twig");
    }
}
