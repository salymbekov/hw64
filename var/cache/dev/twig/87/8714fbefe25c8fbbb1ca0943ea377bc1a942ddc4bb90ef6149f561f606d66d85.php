<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_703314cec892e28a4f0bc69baa14e6ebe2f1790b61968a9a0309e3ef83e48e86 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f290e4e1ceee14b4f196cc41df1cef3fcd286d11b03f28a1c4882f709b159ef0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f290e4e1ceee14b4f196cc41df1cef3fcd286d11b03f28a1c4882f709b159ef0->enter($__internal_f290e4e1ceee14b4f196cc41df1cef3fcd286d11b03f28a1c4882f709b159ef0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        $__internal_c48edd3a55c825a29af76aa883830f5109904b0cf5f3f726edb2a3a4a40db752 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c48edd3a55c825a29af76aa883830f5109904b0cf5f3f726edb2a3a4a40db752->enter($__internal_c48edd3a55c825a29af76aa883830f5109904b0cf5f3f726edb2a3a4a40db752_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_f290e4e1ceee14b4f196cc41df1cef3fcd286d11b03f28a1c4882f709b159ef0->leave($__internal_f290e4e1ceee14b4f196cc41df1cef3fcd286d11b03f28a1c4882f709b159ef0_prof);

        
        $__internal_c48edd3a55c825a29af76aa883830f5109904b0cf5f3f726edb2a3a4a40db752->leave($__internal_c48edd3a55c825a29af76aa883830f5109904b0cf5f3f726edb2a3a4a40db752_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_widget.html.php");
    }
}
