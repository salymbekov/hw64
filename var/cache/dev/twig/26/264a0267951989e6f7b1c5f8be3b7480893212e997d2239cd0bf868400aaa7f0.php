<?php

/* @Framework/Form/widget_attributes.html.php */
class __TwigTemplate_82f905f0bdf50b9ff812645b4358375492ffd12d3a22857c321c628d5d0c4179 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b0d44b6fb83fe3d81f0786e8cacfa2ab2686e5a7605c88d42b6c37a9fc655833 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b0d44b6fb83fe3d81f0786e8cacfa2ab2686e5a7605c88d42b6c37a9fc655833->enter($__internal_b0d44b6fb83fe3d81f0786e8cacfa2ab2686e5a7605c88d42b6c37a9fc655833_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_attributes.html.php"));

        $__internal_54047d44d5c8e819410427b40ece57426c617b8f319df79304a5f66092c076bc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_54047d44d5c8e819410427b40ece57426c617b8f319df79304a5f66092c076bc->enter($__internal_54047d44d5c8e819410427b40ece57426c617b8f319df79304a5f66092c076bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_attributes.html.php"));

        // line 1
        echo "id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php if (\$required): ?> required=\"required\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_b0d44b6fb83fe3d81f0786e8cacfa2ab2686e5a7605c88d42b6c37a9fc655833->leave($__internal_b0d44b6fb83fe3d81f0786e8cacfa2ab2686e5a7605c88d42b6c37a9fc655833_prof);

        
        $__internal_54047d44d5c8e819410427b40ece57426c617b8f319df79304a5f66092c076bc->leave($__internal_54047d44d5c8e819410427b40ece57426c617b8f319df79304a5f66092c076bc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/widget_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php if (\$required): ?> required=\"required\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/widget_attributes.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/widget_attributes.html.php");
    }
}
