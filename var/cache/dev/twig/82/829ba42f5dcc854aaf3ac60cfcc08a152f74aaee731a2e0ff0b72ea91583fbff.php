<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_a07e9bad0e67d3ed3662f5115a1ed466ff9cd879f0a178e3dece30a0bef59566 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b7767ac27a6cf8f0cd861c4c03fc3786a7c9be0cbc1d7b03fef95a9c25d7c3b4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b7767ac27a6cf8f0cd861c4c03fc3786a7c9be0cbc1d7b03fef95a9c25d7c3b4->enter($__internal_b7767ac27a6cf8f0cd861c4c03fc3786a7c9be0cbc1d7b03fef95a9c25d7c3b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $__internal_44a7ad06d847a808aa8066ef0d7d275020c708ee0e0cb9c980560b30ceece331 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_44a7ad06d847a808aa8066ef0d7d275020c708ee0e0cb9c980560b30ceece331->enter($__internal_44a7ad06d847a808aa8066ef0d7d275020c708ee0e0cb9c980560b30ceece331_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b7767ac27a6cf8f0cd861c4c03fc3786a7c9be0cbc1d7b03fef95a9c25d7c3b4->leave($__internal_b7767ac27a6cf8f0cd861c4c03fc3786a7c9be0cbc1d7b03fef95a9c25d7c3b4_prof);

        
        $__internal_44a7ad06d847a808aa8066ef0d7d275020c708ee0e0cb9c980560b30ceece331->leave($__internal_44a7ad06d847a808aa8066ef0d7d275020c708ee0e0cb9c980560b30ceece331_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ece9bacfb51c875d22bc385dd195799317033759ed756cb7296ea5473f857c9d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ece9bacfb51c875d22bc385dd195799317033759ed756cb7296ea5473f857c9d->enter($__internal_ece9bacfb51c875d22bc385dd195799317033759ed756cb7296ea5473f857c9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_05a9cecea56722f21812a055a2a0fcb014da72ca5c0141ac4155b9236aee48a0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_05a9cecea56722f21812a055a2a0fcb014da72ca5c0141ac4155b9236aee48a0->enter($__internal_05a9cecea56722f21812a055a2a0fcb014da72ca5c0141ac4155b9236aee48a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "FOSUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $__internal_05a9cecea56722f21812a055a2a0fcb014da72ca5c0141ac4155b9236aee48a0->leave($__internal_05a9cecea56722f21812a055a2a0fcb014da72ca5c0141ac4155b9236aee48a0_prof);

        
        $__internal_ece9bacfb51c875d22bc385dd195799317033759ed756cb7296ea5473f857c9d->leave($__internal_ece9bacfb51c875d22bc385dd195799317033759ed756cb7296ea5473f857c9d_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:edit.html.twig", "/home/timur/http/hw/hw64/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/edit.html.twig");
    }
}
