<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_91c31ea7aa35c80848f0bf92f00ddc83100eb8653289225e8a363f6a8b679545 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d61477f0add95dd8aeb3ee0d402d87d2ef8edfe400fdb5af04fb358e9fcc6c63 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d61477f0add95dd8aeb3ee0d402d87d2ef8edfe400fdb5af04fb358e9fcc6c63->enter($__internal_d61477f0add95dd8aeb3ee0d402d87d2ef8edfe400fdb5af04fb358e9fcc6c63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        $__internal_022692056d144aa1ddd970ee7c69ad78827b62b13b3b9afddc0171aba003b505 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_022692056d144aa1ddd970ee7c69ad78827b62b13b3b9afddc0171aba003b505->enter($__internal_022692056d144aa1ddd970ee7c69ad78827b62b13b3b9afddc0171aba003b505_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_d61477f0add95dd8aeb3ee0d402d87d2ef8edfe400fdb5af04fb358e9fcc6c63->leave($__internal_d61477f0add95dd8aeb3ee0d402d87d2ef8edfe400fdb5af04fb358e9fcc6c63_prof);

        
        $__internal_022692056d144aa1ddd970ee7c69ad78827b62b13b3b9afddc0171aba003b505->leave($__internal_022692056d144aa1ddd970ee7c69ad78827b62b13b3b9afddc0171aba003b505_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
", "@Framework/Form/form.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form.html.php");
    }
}
