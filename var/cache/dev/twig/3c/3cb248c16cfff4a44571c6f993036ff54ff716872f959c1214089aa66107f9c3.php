<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_a243248e3d9d9c4fc3f8772cee7ee48076b113883e0ab5561d29166c68782caf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_48e39adea5a143d519a09dfc943b26b392daad1fb383e688c90f55a40c754a0c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_48e39adea5a143d519a09dfc943b26b392daad1fb383e688c90f55a40c754a0c->enter($__internal_48e39adea5a143d519a09dfc943b26b392daad1fb383e688c90f55a40c754a0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $__internal_c798e4fd5c4a542a6cbf941fc7fdf0a81a25c351ee74f4afb36ac75eaac43a2f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c798e4fd5c4a542a6cbf941fc7fdf0a81a25c351ee74f4afb36ac75eaac43a2f->enter($__internal_c798e4fd5c4a542a6cbf941fc7fdf0a81a25c351ee74f4afb36ac75eaac43a2f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_48e39adea5a143d519a09dfc943b26b392daad1fb383e688c90f55a40c754a0c->leave($__internal_48e39adea5a143d519a09dfc943b26b392daad1fb383e688c90f55a40c754a0c_prof);

        
        $__internal_c798e4fd5c4a542a6cbf941fc7fdf0a81a25c351ee74f4afb36ac75eaac43a2f->leave($__internal_c798e4fd5c4a542a6cbf941fc7fdf0a81a25c351ee74f4afb36ac75eaac43a2f_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_240f6ba4092b31594b4625f430dfb7d9c801128ee9fd90eedec3c7ee0f8bf09e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_240f6ba4092b31594b4625f430dfb7d9c801128ee9fd90eedec3c7ee0f8bf09e->enter($__internal_240f6ba4092b31594b4625f430dfb7d9c801128ee9fd90eedec3c7ee0f8bf09e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_6be6d5b02ad75c5d466bee50a136ed78435b9305cca42eb3156fd0f022e87976 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6be6d5b02ad75c5d466bee50a136ed78435b9305cca42eb3156fd0f022e87976->enter($__internal_6be6d5b02ad75c5d466bee50a136ed78435b9305cca42eb3156fd0f022e87976_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/reset_content.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $__internal_6be6d5b02ad75c5d466bee50a136ed78435b9305cca42eb3156fd0f022e87976->leave($__internal_6be6d5b02ad75c5d466bee50a136ed78435b9305cca42eb3156fd0f022e87976_prof);

        
        $__internal_240f6ba4092b31594b4625f430dfb7d9c801128ee9fd90eedec3c7ee0f8bf09e->leave($__internal_240f6ba4092b31594b4625f430dfb7d9c801128ee9fd90eedec3c7ee0f8bf09e_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/reset_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:reset.html.twig", "/home/timur/http/hw/hw64/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/reset.html.twig");
    }
}
