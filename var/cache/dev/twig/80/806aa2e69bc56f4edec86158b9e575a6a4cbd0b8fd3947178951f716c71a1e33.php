<?php

/* FOSUserBundle:ChangePassword:change_password.html.twig */
class __TwigTemplate_f93a27cf62eea576fa89b497a1ddfdf679af57c19b3ac81c0cbb0e9b22703bf6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d590099fb7d23581968e5753d6bcb5a64afac90f1accddd75b717ecd5c302755 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d590099fb7d23581968e5753d6bcb5a64afac90f1accddd75b717ecd5c302755->enter($__internal_d590099fb7d23581968e5753d6bcb5a64afac90f1accddd75b717ecd5c302755_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $__internal_dfb2d2dc67afcb52bf9ce8019981cec5e9261d2e0f5960194b06d94ca674be7e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dfb2d2dc67afcb52bf9ce8019981cec5e9261d2e0f5960194b06d94ca674be7e->enter($__internal_dfb2d2dc67afcb52bf9ce8019981cec5e9261d2e0f5960194b06d94ca674be7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d590099fb7d23581968e5753d6bcb5a64afac90f1accddd75b717ecd5c302755->leave($__internal_d590099fb7d23581968e5753d6bcb5a64afac90f1accddd75b717ecd5c302755_prof);

        
        $__internal_dfb2d2dc67afcb52bf9ce8019981cec5e9261d2e0f5960194b06d94ca674be7e->leave($__internal_dfb2d2dc67afcb52bf9ce8019981cec5e9261d2e0f5960194b06d94ca674be7e_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_46c0092394ba7a61e8a8e10b0a615dc479e36bbb4d5c6c6defebe67f1f0d8262 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_46c0092394ba7a61e8a8e10b0a615dc479e36bbb4d5c6c6defebe67f1f0d8262->enter($__internal_46c0092394ba7a61e8a8e10b0a615dc479e36bbb4d5c6c6defebe67f1f0d8262_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_207d379fe16824abd2a27a64df322f409bbe1fce2db95c85237f02bda4a09c9a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_207d379fe16824abd2a27a64df322f409bbe1fce2db95c85237f02bda4a09c9a->enter($__internal_207d379fe16824abd2a27a64df322f409bbe1fce2db95c85237f02bda4a09c9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 4)->display($context);
        
        $__internal_207d379fe16824abd2a27a64df322f409bbe1fce2db95c85237f02bda4a09c9a->leave($__internal_207d379fe16824abd2a27a64df322f409bbe1fce2db95c85237f02bda4a09c9a_prof);

        
        $__internal_46c0092394ba7a61e8a8e10b0a615dc479e36bbb4d5c6c6defebe67f1f0d8262->leave($__internal_46c0092394ba7a61e8a8e10b0a615dc479e36bbb4d5c6c6defebe67f1f0d8262_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/ChangePassword/change_password_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:ChangePassword:change_password.html.twig", "/home/timur/http/hw/hw64/vendor/friendsofsymfony/user-bundle/Resources/views/ChangePassword/change_password.html.twig");
    }
}
