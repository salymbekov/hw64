<?php

/* TwigBundle:Exception:traces.xml.twig */
class __TwigTemplate_18662f105994d9f1cf02041e26f3080b530181643044bf1c881b4cc90e688209 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8eef6d0f7cdad8d756ad36d5c2aa4c4a3d2c69ab853d873bbc5467a4d56efc7e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8eef6d0f7cdad8d756ad36d5c2aa4c4a3d2c69ab853d873bbc5467a4d56efc7e->enter($__internal_8eef6d0f7cdad8d756ad36d5c2aa4c4a3d2c69ab853d873bbc5467a4d56efc7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.xml.twig"));

        $__internal_78bfc1f279137dce746a665246fe6795fca3f8b2cec9599d21f7ecaabdcf479a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_78bfc1f279137dce746a665246fe6795fca3f8b2cec9599d21f7ecaabdcf479a->enter($__internal_78bfc1f279137dce746a665246fe6795fca3f8b2cec9599d21f7ecaabdcf479a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.xml.twig"));

        // line 1
        echo "        <traces>
";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "trace", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
            // line 3
            echo "            <trace>
";
            // line 4
            echo twig_include($this->env, $context, "@Twig/Exception/trace.txt.twig", array("trace" => $context["trace"]), false);
            echo "

            </trace>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "        </traces>
";
        
        $__internal_8eef6d0f7cdad8d756ad36d5c2aa4c4a3d2c69ab853d873bbc5467a4d56efc7e->leave($__internal_8eef6d0f7cdad8d756ad36d5c2aa4c4a3d2c69ab853d873bbc5467a4d56efc7e_prof);

        
        $__internal_78bfc1f279137dce746a665246fe6795fca3f8b2cec9599d21f7ecaabdcf479a->leave($__internal_78bfc1f279137dce746a665246fe6795fca3f8b2cec9599d21f7ecaabdcf479a_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:traces.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 8,  35 => 4,  32 => 3,  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("        <traces>
{% for trace in exception.trace %}
            <trace>
{{ include('@Twig/Exception/trace.txt.twig', { trace: trace }, with_context = false) }}

            </trace>
{% endfor %}
        </traces>
", "TwigBundle:Exception:traces.xml.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/traces.xml.twig");
    }
}
