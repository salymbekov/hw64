<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_05a27c47a58686df9dbcd2e535088881f445feee5009a091d2d065b21e338ded extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cd588b22f9832e5a0635eeb2a94f64d6a3477a6ef8ba8089e70e3b7338914162 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cd588b22f9832e5a0635eeb2a94f64d6a3477a6ef8ba8089e70e3b7338914162->enter($__internal_cd588b22f9832e5a0635eeb2a94f64d6a3477a6ef8ba8089e70e3b7338914162_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        $__internal_1a08e36390bda82e38ff5847ad200d32481033235058e0c5105711051730a34d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1a08e36390bda82e38ff5847ad200d32481033235058e0c5105711051730a34d->enter($__internal_1a08e36390bda82e38ff5847ad200d32481033235058e0c5105711051730a34d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_cd588b22f9832e5a0635eeb2a94f64d6a3477a6ef8ba8089e70e3b7338914162->leave($__internal_cd588b22f9832e5a0635eeb2a94f64d6a3477a6ef8ba8089e70e3b7338914162_prof);

        
        $__internal_1a08e36390bda82e38ff5847ad200d32481033235058e0c5105711051730a34d->leave($__internal_1a08e36390bda82e38ff5847ad200d32481033235058e0c5105711051730a34d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
", "@Framework/Form/choice_widget_expanded.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget_expanded.html.php");
    }
}
