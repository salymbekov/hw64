<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_6e2b3dc6b4949a28c4a4019a17259328b4a691fad9e3fffc02f11e777d813c5d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cc6e68224772f3e75118fa03553a60918728c7e2787d303c3de5c01c191f97d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cc6e68224772f3e75118fa03553a60918728c7e2787d303c3de5c01c191f97d1->enter($__internal_cc6e68224772f3e75118fa03553a60918728c7e2787d303c3de5c01c191f97d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        $__internal_45c7204f297ff514604748f762ade8d8ffd6835b613f69c2c3393ff0aef0f6e3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_45c7204f297ff514604748f762ade8d8ffd6835b613f69c2c3393ff0aef0f6e3->enter($__internal_45c7204f297ff514604748f762ade8d8ffd6835b613f69c2c3393ff0aef0f6e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_cc6e68224772f3e75118fa03553a60918728c7e2787d303c3de5c01c191f97d1->leave($__internal_cc6e68224772f3e75118fa03553a60918728c7e2787d303c3de5c01c191f97d1_prof);

        
        $__internal_45c7204f297ff514604748f762ade8d8ffd6835b613f69c2c3393ff0aef0f6e3->leave($__internal_45c7204f297ff514604748f762ade8d8ffd6835b613f69c2c3393ff0aef0f6e3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/password_widget.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/password_widget.html.php");
    }
}
