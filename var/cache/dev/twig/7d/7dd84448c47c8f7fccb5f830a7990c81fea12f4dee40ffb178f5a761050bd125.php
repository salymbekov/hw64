<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_eac8095231b6938ed03b99b815109d328f77406df78fb9a6446abc90f13eb488 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fa1e393294285a5bc0169c032bae37d33a8d6abd8f4eef5e98e0bc3e984d6db0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fa1e393294285a5bc0169c032bae37d33a8d6abd8f4eef5e98e0bc3e984d6db0->enter($__internal_fa1e393294285a5bc0169c032bae37d33a8d6abd8f4eef5e98e0bc3e984d6db0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        $__internal_5aa048e0d109a3cf9a2a5e43fe844d3a4dd9babada11793caa668dda7d5b8759 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5aa048e0d109a3cf9a2a5e43fe844d3a4dd9babada11793caa668dda7d5b8759->enter($__internal_5aa048e0d109a3cf9a2a5e43fe844d3a4dd9babada11793caa668dda7d5b8759_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes'); ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form); ?>
        </td>
    </tr>
    <?php endif; ?>
    <?php echo \$view['form']->block(\$form, 'form_rows'); ?>
    <?php echo \$view['form']->rest(\$form); ?>
</table>
";
        
        $__internal_fa1e393294285a5bc0169c032bae37d33a8d6abd8f4eef5e98e0bc3e984d6db0->leave($__internal_fa1e393294285a5bc0169c032bae37d33a8d6abd8f4eef5e98e0bc3e984d6db0_prof);

        
        $__internal_5aa048e0d109a3cf9a2a5e43fe844d3a4dd9babada11793caa668dda7d5b8759->leave($__internal_5aa048e0d109a3cf9a2a5e43fe844d3a4dd9babada11793caa668dda7d5b8759_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes'); ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form); ?>
        </td>
    </tr>
    <?php endif; ?>
    <?php echo \$view['form']->block(\$form, 'form_rows'); ?>
    <?php echo \$view['form']->rest(\$form); ?>
</table>
", "@Framework/FormTable/form_widget_compound.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_widget_compound.html.php");
    }
}
