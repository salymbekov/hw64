<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_2f24925f86d4359fb07f7e1cf31aae5335e77fd4ca0e0f1c77126798a72c4b11 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7a26466ca0f91b8126a656b9471085a357ae0740d62a642cfa5cfc176ddeacff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7a26466ca0f91b8126a656b9471085a357ae0740d62a642cfa5cfc176ddeacff->enter($__internal_7a26466ca0f91b8126a656b9471085a357ae0740d62a642cfa5cfc176ddeacff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        $__internal_3a333fd629fabece9af5ce79c7978bc7bbac0c3df36513d67919ecabb36dae9c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3a333fd629fabece9af5ce79c7978bc7bbac0c3df36513d67919ecabb36dae9c->enter($__internal_3a333fd629fabece9af5ce79c7978bc7bbac0c3df36513d67919ecabb36dae9c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_7a26466ca0f91b8126a656b9471085a357ae0740d62a642cfa5cfc176ddeacff->leave($__internal_7a26466ca0f91b8126a656b9471085a357ae0740d62a642cfa5cfc176ddeacff_prof);

        
        $__internal_3a333fd629fabece9af5ce79c7978bc7bbac0c3df36513d67919ecabb36dae9c->leave($__internal_3a333fd629fabece9af5ce79c7978bc7bbac0c3df36513d67919ecabb36dae9c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/form_errors.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_errors.html.php");
    }
}
