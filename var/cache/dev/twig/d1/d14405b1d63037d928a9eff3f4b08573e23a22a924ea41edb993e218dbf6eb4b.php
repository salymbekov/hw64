<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_f03c55ddad3579f13f81ea89d4db9be0dfa09fb32b11c96a94693bde757992ab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_791c372f3981f3a4ab59953bb630a577dd8fe56887041e33c2615e7d84a8edef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_791c372f3981f3a4ab59953bb630a577dd8fe56887041e33c2615e7d84a8edef->enter($__internal_791c372f3981f3a4ab59953bb630a577dd8fe56887041e33c2615e7d84a8edef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $__internal_5ca0feb29a63bcee56fab5b6e55716278394654c6fad5ba3a95393637643d2be = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5ca0feb29a63bcee56fab5b6e55716278394654c6fad5ba3a95393637643d2be->enter($__internal_5ca0feb29a63bcee56fab5b6e55716278394654c6fad5ba3a95393637643d2be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_791c372f3981f3a4ab59953bb630a577dd8fe56887041e33c2615e7d84a8edef->leave($__internal_791c372f3981f3a4ab59953bb630a577dd8fe56887041e33c2615e7d84a8edef_prof);

        
        $__internal_5ca0feb29a63bcee56fab5b6e55716278394654c6fad5ba3a95393637643d2be->leave($__internal_5ca0feb29a63bcee56fab5b6e55716278394654c6fad5ba3a95393637643d2be_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_06cb57543817de7bcefdfb498b50aaca52483cc163cfbc2e30306f8f991bb570 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_06cb57543817de7bcefdfb498b50aaca52483cc163cfbc2e30306f8f991bb570->enter($__internal_06cb57543817de7bcefdfb498b50aaca52483cc163cfbc2e30306f8f991bb570_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_fe074d9bbd4d21b2e685035c9955a57cda5b7a49b82b7cec8e9e93940c31cea8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fe074d9bbd4d21b2e685035c9955a57cda5b7a49b82b7cec8e9e93940c31cea8->enter($__internal_fe074d9bbd4d21b2e685035c9955a57cda5b7a49b82b7cec8e9e93940c31cea8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_fe074d9bbd4d21b2e685035c9955a57cda5b7a49b82b7cec8e9e93940c31cea8->leave($__internal_fe074d9bbd4d21b2e685035c9955a57cda5b7a49b82b7cec8e9e93940c31cea8_prof);

        
        $__internal_06cb57543817de7bcefdfb498b50aaca52483cc163cfbc2e30306f8f991bb570->leave($__internal_06cb57543817de7bcefdfb498b50aaca52483cc163cfbc2e30306f8f991bb570_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_975639db97813cf9afbfc7c14106a5f66836202318ce2a9e52cc76689ea07d14 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_975639db97813cf9afbfc7c14106a5f66836202318ce2a9e52cc76689ea07d14->enter($__internal_975639db97813cf9afbfc7c14106a5f66836202318ce2a9e52cc76689ea07d14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_4109a27bb1a7d2cb2460e665502d93020517f0a548c6a695b8a29e1a98ffc246 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4109a27bb1a7d2cb2460e665502d93020517f0a548c6a695b8a29e1a98ffc246->enter($__internal_4109a27bb1a7d2cb2460e665502d93020517f0a548c6a695b8a29e1a98ffc246_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_4109a27bb1a7d2cb2460e665502d93020517f0a548c6a695b8a29e1a98ffc246->leave($__internal_4109a27bb1a7d2cb2460e665502d93020517f0a548c6a695b8a29e1a98ffc246_prof);

        
        $__internal_975639db97813cf9afbfc7c14106a5f66836202318ce2a9e52cc76689ea07d14->leave($__internal_975639db97813cf9afbfc7c14106a5f66836202318ce2a9e52cc76689ea07d14_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_628350a3085f0a16214a7f4e36ff8d419ba6cf0ce20c1540b48ec26b04ea6c95 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_628350a3085f0a16214a7f4e36ff8d419ba6cf0ce20c1540b48ec26b04ea6c95->enter($__internal_628350a3085f0a16214a7f4e36ff8d419ba6cf0ce20c1540b48ec26b04ea6c95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_de821e5f46a511d201c1b92c85a07f7e326b52dbb68313a514df5357c7d0c416 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_de821e5f46a511d201c1b92c85a07f7e326b52dbb68313a514df5357c7d0c416->enter($__internal_de821e5f46a511d201c1b92c85a07f7e326b52dbb68313a514df5357c7d0c416_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_de821e5f46a511d201c1b92c85a07f7e326b52dbb68313a514df5357c7d0c416->leave($__internal_de821e5f46a511d201c1b92c85a07f7e326b52dbb68313a514df5357c7d0c416_prof);

        
        $__internal_628350a3085f0a16214a7f4e36ff8d419ba6cf0ce20c1540b48ec26b04ea6c95->leave($__internal_628350a3085f0a16214a7f4e36ff8d419ba6cf0ce20c1540b48ec26b04ea6c95_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "WebProfilerBundle:Collector:router.html.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
