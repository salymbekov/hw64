<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_d018735f261aa5278a4c0f1304b7279750c8d3038672409c062c60504a291627 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e281b1a0a194757915041d2c395ad6bd047e2f90b8d7eea13e0334c929dd4685 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e281b1a0a194757915041d2c395ad6bd047e2f90b8d7eea13e0334c929dd4685->enter($__internal_e281b1a0a194757915041d2c395ad6bd047e2f90b8d7eea13e0334c929dd4685_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $__internal_de769e4b19aaddbee912514e1db8ffd55b4d2909090ce26cf17c285ec3fbca13 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_de769e4b19aaddbee912514e1db8ffd55b4d2909090ce26cf17c285ec3fbca13->enter($__internal_de769e4b19aaddbee912514e1db8ffd55b4d2909090ce26cf17c285ec3fbca13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e281b1a0a194757915041d2c395ad6bd047e2f90b8d7eea13e0334c929dd4685->leave($__internal_e281b1a0a194757915041d2c395ad6bd047e2f90b8d7eea13e0334c929dd4685_prof);

        
        $__internal_de769e4b19aaddbee912514e1db8ffd55b4d2909090ce26cf17c285ec3fbca13->leave($__internal_de769e4b19aaddbee912514e1db8ffd55b4d2909090ce26cf17c285ec3fbca13_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_f57fdf1dac0f12d628cddbe0642ef682dfe7a3bc467ccc5c135662732f880883 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f57fdf1dac0f12d628cddbe0642ef682dfe7a3bc467ccc5c135662732f880883->enter($__internal_f57fdf1dac0f12d628cddbe0642ef682dfe7a3bc467ccc5c135662732f880883_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_182236129a3632ac1433ed54b45faa6647f074d32eb00103d68f483661d25fa3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_182236129a3632ac1433ed54b45faa6647f074d32eb00103d68f483661d25fa3->enter($__internal_182236129a3632ac1433ed54b45faa6647f074d32eb00103d68f483661d25fa3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.confirmed", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if (($context["targetUrl"] ?? $this->getContext($context, "targetUrl"))) {
            // line 8
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, ($context["targetUrl"] ?? $this->getContext($context, "targetUrl")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $__internal_182236129a3632ac1433ed54b45faa6647f074d32eb00103d68f483661d25fa3->leave($__internal_182236129a3632ac1433ed54b45faa6647f074d32eb00103d68f483661d25fa3_prof);

        
        $__internal_f57fdf1dac0f12d628cddbe0642ef682dfe7a3bc467ccc5c135662732f880883->leave($__internal_f57fdf1dac0f12d628cddbe0642ef682dfe7a3bc467ccc5c135662732f880883_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 8,  54 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>
    {% if targetUrl %}
    <p><a href=\"{{ targetUrl }}\">{{ 'registration.back'|trans }}</a></p>
    {% endif %}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:confirmed.html.twig", "/home/timur/http/hw/hw64/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/confirmed.html.twig");
    }
}
