<?php

/* @Twig/images/chevron-right.svg */
class __TwigTemplate_35004c29145e28721447a9265d0f670601fb35aa9b9d0e91b71436866ffc481a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a745ddeb14e4996ccb497976b29e7bbc84075cfa4ea6e1e11d941ba33a01bdaf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a745ddeb14e4996ccb497976b29e7bbc84075cfa4ea6e1e11d941ba33a01bdaf->enter($__internal_a745ddeb14e4996ccb497976b29e7bbc84075cfa4ea6e1e11d941ba33a01bdaf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        $__internal_fbd3a723e44aff4d9b11d4581f3ac50f6db510076e079ad91b5ab2ed227e7df7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fbd3a723e44aff4d9b11d4581f3ac50f6db510076e079ad91b5ab2ed227e7df7->enter($__internal_fbd3a723e44aff4d9b11d4581f3ac50f6db510076e079ad91b5ab2ed227e7df7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
";
        
        $__internal_a745ddeb14e4996ccb497976b29e7bbc84075cfa4ea6e1e11d941ba33a01bdaf->leave($__internal_a745ddeb14e4996ccb497976b29e7bbc84075cfa4ea6e1e11d941ba33a01bdaf_prof);

        
        $__internal_fbd3a723e44aff4d9b11d4581f3ac50f6db510076e079ad91b5ab2ed227e7df7->leave($__internal_fbd3a723e44aff4d9b11d4581f3ac50f6db510076e079ad91b5ab2ed227e7df7_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/chevron-right.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
", "@Twig/images/chevron-right.svg", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/chevron-right.svg");
    }
}
