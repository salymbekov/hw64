<?php

/* WebProfilerBundle:Profiler:info.html.twig */
class __TwigTemplate_92979114c1c31ba01e4f3be5015cc29f1f97107debdd4ab1214c6468fd0f34f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Profiler:info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'block_summary'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_50e28cbb005b964a6cc974b4970ccc486f211c6abecb17a678371d25578c4147 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_50e28cbb005b964a6cc974b4970ccc486f211c6abecb17a678371d25578c4147->enter($__internal_50e28cbb005b964a6cc974b4970ccc486f211c6abecb17a678371d25578c4147_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        $__internal_afed3a11cd21527abdea4f3464435a31b98844b45813e359ea30d5fc36146b79 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_afed3a11cd21527abdea4f3464435a31b98844b45813e359ea30d5fc36146b79->enter($__internal_afed3a11cd21527abdea4f3464435a31b98844b45813e359ea30d5fc36146b79_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        // line 3
        $context["messages"] = array("no_token" => array("status" => "error", "title" => (((((        // line 6
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 7
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : (""))) . "\" was not found in the database.")))));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_50e28cbb005b964a6cc974b4970ccc486f211c6abecb17a678371d25578c4147->leave($__internal_50e28cbb005b964a6cc974b4970ccc486f211c6abecb17a678371d25578c4147_prof);

        
        $__internal_afed3a11cd21527abdea4f3464435a31b98844b45813e359ea30d5fc36146b79->leave($__internal_afed3a11cd21527abdea4f3464435a31b98844b45813e359ea30d5fc36146b79_prof);

    }

    // line 11
    public function block_summary($context, array $blocks = array())
    {
        $__internal_3e957f8e23ecda02703c65988d1f8963bd552b68cdca5d56057121b64175ebe8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3e957f8e23ecda02703c65988d1f8963bd552b68cdca5d56057121b64175ebe8->enter($__internal_3e957f8e23ecda02703c65988d1f8963bd552b68cdca5d56057121b64175ebe8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        $__internal_a5eb8de4aa994bf84e7f9227dfa785171a232dc91ff2e859a7653290bdcb779d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a5eb8de4aa994bf84e7f9227dfa785171a232dc91ff2e859a7653290bdcb779d->enter($__internal_a5eb8de4aa994bf84e7f9227dfa785171a232dc91ff2e859a7653290bdcb779d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        // line 12
        echo "    <div class=\"status status-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 14
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $__internal_a5eb8de4aa994bf84e7f9227dfa785171a232dc91ff2e859a7653290bdcb779d->leave($__internal_a5eb8de4aa994bf84e7f9227dfa785171a232dc91ff2e859a7653290bdcb779d_prof);

        
        $__internal_3e957f8e23ecda02703c65988d1f8963bd552b68cdca5d56057121b64175ebe8->leave($__internal_3e957f8e23ecda02703c65988d1f8963bd552b68cdca5d56057121b64175ebe8_prof);

    }

    // line 19
    public function block_panel($context, array $blocks = array())
    {
        $__internal_16f82f358d3a9e04f670181258191fa5b1b0f68b4328a0f8013303802fba8322 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_16f82f358d3a9e04f670181258191fa5b1b0f68b4328a0f8013303802fba8322->enter($__internal_16f82f358d3a9e04f670181258191fa5b1b0f68b4328a0f8013303802fba8322_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_b4370a5737ad60314453ad6dd5a1ede4eded8223aac63241d1fb67ac912708fa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b4370a5737ad60314453ad6dd5a1ede4eded8223aac63241d1fb67ac912708fa->enter($__internal_b4370a5737ad60314453ad6dd5a1ede4eded8223aac63241d1fb67ac912708fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 20
        echo "    <h2>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_b4370a5737ad60314453ad6dd5a1ede4eded8223aac63241d1fb67ac912708fa->leave($__internal_b4370a5737ad60314453ad6dd5a1ede4eded8223aac63241d1fb67ac912708fa_prof);

        
        $__internal_16f82f358d3a9e04f670181258191fa5b1b0f68b4328a0f8013303802fba8322->leave($__internal_16f82f358d3a9e04f670181258191fa5b1b0f68b4328a0f8013303802fba8322_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 21,  84 => 20,  75 => 19,  61 => 14,  55 => 12,  46 => 11,  36 => 1,  34 => 7,  33 => 6,  32 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% set messages = {
    'no_token' : {
        status:  'error',
        title:   (token|default('') == 'latest') ? 'There are no profiles' : 'Token not found',
        message: (token|default('') == 'latest') ? 'No profiles found in the database.' : 'Token \"' ~ token|default('') ~ '\" was not found in the database.'
    }
} %}

{% block summary %}
    <div class=\"status status-{{ messages[about].status }}\">
        <div class=\"container\">
            <h2>{{ messages[about].status|title }}</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ messages[about].title }}</h2>
    <p>{{ messages[about].message }}</p>
{% endblock %}
", "WebProfilerBundle:Profiler:info.html.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/info.html.twig");
    }
}
