<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_40a43f3c24bdc552263470df555b7fe4efcdb293a35d981d83244c1000034994 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7f8bb9415d173b715d46a90b3531ff27386af7da155037350fd9578e43e06d55 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7f8bb9415d173b715d46a90b3531ff27386af7da155037350fd9578e43e06d55->enter($__internal_7f8bb9415d173b715d46a90b3531ff27386af7da155037350fd9578e43e06d55_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        $__internal_2448514392b9f4eed0babea8b3e406742a702660a26e5b01ac2c4032681afd51 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2448514392b9f4eed0babea8b3e406742a702660a26e5b01ac2c4032681afd51->enter($__internal_2448514392b9f4eed0babea8b3e406742a702660a26e5b01ac2c4032681afd51_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
";
        
        $__internal_7f8bb9415d173b715d46a90b3531ff27386af7da155037350fd9578e43e06d55->leave($__internal_7f8bb9415d173b715d46a90b3531ff27386af7da155037350fd9578e43e06d55_prof);

        
        $__internal_2448514392b9f4eed0babea8b3e406742a702660a26e5b01ac2c4032681afd51->leave($__internal_2448514392b9f4eed0babea8b3e406742a702660a26e5b01ac2c4032681afd51_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
", "@Framework/FormTable/hidden_row.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/hidden_row.html.php");
    }
}
