<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_73ea2d506c697e1c19562d95566569ae4d8c7ab0629e2f151580f9d354767f0a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_eaa3b04fda284643161c90f50add0dcf615fb34e3c1dbc0274f1051e7139bfc1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eaa3b04fda284643161c90f50add0dcf615fb34e3c1dbc0274f1051e7139bfc1->enter($__internal_eaa3b04fda284643161c90f50add0dcf615fb34e3c1dbc0274f1051e7139bfc1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        $__internal_feff41eea2a06bded7f6b89c7453dc8a9f0fc4db65ab8fa8701add2121758786 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_feff41eea2a06bded7f6b89c7453dc8a9f0fc4db65ab8fa8701add2121758786->enter($__internal_feff41eea2a06bded7f6b89c7453dc8a9f0fc4db65ab8fa8701add2121758786_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
";
        
        $__internal_eaa3b04fda284643161c90f50add0dcf615fb34e3c1dbc0274f1051e7139bfc1->leave($__internal_eaa3b04fda284643161c90f50add0dcf615fb34e3c1dbc0274f1051e7139bfc1_prof);

        
        $__internal_feff41eea2a06bded7f6b89c7453dc8a9f0fc4db65ab8fa8701add2121758786->leave($__internal_feff41eea2a06bded7f6b89c7453dc8a9f0fc4db65ab8fa8701add2121758786_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "TwigBundle:Exception:exception.atom.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.atom.twig");
    }
}
