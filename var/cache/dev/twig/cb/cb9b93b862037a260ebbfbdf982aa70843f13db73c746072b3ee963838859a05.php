<?php

/* FOSUserBundle:Group:new_content.html.twig */
class __TwigTemplate_720f689443154709869b66109723260de41842a09e1f45d44115bfe8108db1f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8db3aaccce94b5e449bb3ff985f4aacdefb18ebe999b18966ef77a7a79ac48da = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8db3aaccce94b5e449bb3ff985f4aacdefb18ebe999b18966ef77a7a79ac48da->enter($__internal_8db3aaccce94b5e449bb3ff985f4aacdefb18ebe999b18966ef77a7a79ac48da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new_content.html.twig"));

        $__internal_d6f72d2e963423fd8087a9abee45b25da14a33a04e80726aa7aef59c18cc145f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d6f72d2e963423fd8087a9abee45b25da14a33a04e80726aa7aef59c18cc145f->enter($__internal_d6f72d2e963423fd8087a9abee45b25da14a33a04e80726aa7aef59c18cc145f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new_content.html.twig"));

        // line 2
        echo "
";
        // line 3
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_group_new"), "attr" => array("class" => "fos_user_group_new")));
        echo "
    ";
        // line 4
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
    <div>
        <input type=\"submit\" value=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.new.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
";
        // line 8
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_8db3aaccce94b5e449bb3ff985f4aacdefb18ebe999b18966ef77a7a79ac48da->leave($__internal_8db3aaccce94b5e449bb3ff985f4aacdefb18ebe999b18966ef77a7a79ac48da_prof);

        
        $__internal_d6f72d2e963423fd8087a9abee45b25da14a33a04e80726aa7aef59c18cc145f->leave($__internal_d6f72d2e963423fd8087a9abee45b25da14a33a04e80726aa7aef59c18cc145f_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 8,  37 => 6,  32 => 4,  28 => 3,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

{{ form_start(form, { 'action': path('fos_user_group_new'), 'attr': { 'class': 'fos_user_group_new' } }) }}
    {{ form_widget(form) }}
    <div>
        <input type=\"submit\" value=\"{{ 'group.new.submit'|trans }}\" />
    </div>
{{ form_end(form) }}
", "FOSUserBundle:Group:new_content.html.twig", "/home/timur/http/hw/hw64/vendor/friendsofsymfony/user-bundle/Resources/views/Group/new_content.html.twig");
    }
}
