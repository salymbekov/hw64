<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_5e84dd09dcab411a9d2fa3ab02db5a84476abf4bbd3a961293f60a8a1e3e1c8e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3872bc267b1ccdfae7504949d98b6377ce8fb653a5e4bfb65a93318f439f57b0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3872bc267b1ccdfae7504949d98b6377ce8fb653a5e4bfb65a93318f439f57b0->enter($__internal_3872bc267b1ccdfae7504949d98b6377ce8fb653a5e4bfb65a93318f439f57b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        $__internal_c53cbcba19cf1627cc34d3e2cde785e6e6bd3ae82ce4e4d4de32520157abd12f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c53cbcba19cf1627cc34d3e2cde785e6e6bd3ae82ce4e4d4de32520157abd12f->enter($__internal_c53cbcba19cf1627cc34d3e2cde785e6e6bd3ae82ce4e4d4de32520157abd12f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_3872bc267b1ccdfae7504949d98b6377ce8fb653a5e4bfb65a93318f439f57b0->leave($__internal_3872bc267b1ccdfae7504949d98b6377ce8fb653a5e4bfb65a93318f439f57b0_prof);

        
        $__internal_c53cbcba19cf1627cc34d3e2cde785e6e6bd3ae82ce4e4d4de32520157abd12f->leave($__internal_c53cbcba19cf1627cc34d3e2cde785e6e6bd3ae82ce4e4d4de32520157abd12f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
", "@Framework/Form/percent_widget.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/percent_widget.html.php");
    }
}
