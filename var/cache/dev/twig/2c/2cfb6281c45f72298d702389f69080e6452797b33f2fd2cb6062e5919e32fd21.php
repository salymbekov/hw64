<?php

/* TwigBundle::layout.html.twig */
class __TwigTemplate_799109897a00351cb424175a57a6f83b0265a2b331b093f13546dd3cec733282 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8e95ea08c5186c9cd9ecff2ff10c279c0e657e69d5e236be2e32ec8edc173e5d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8e95ea08c5186c9cd9ecff2ff10c279c0e657e69d5e236be2e32ec8edc173e5d->enter($__internal_8e95ea08c5186c9cd9ecff2ff10c279c0e657e69d5e236be2e32ec8edc173e5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle::layout.html.twig"));

        $__internal_472a1548002b4095fd735df7b5926280167c43a4ec998de945b38e536a94c59d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_472a1548002b4095fd735df7b5926280167c43a4ec998de945b38e536a94c59d->enter($__internal_472a1548002b4095fd735df7b5926280167c43a4ec998de945b38e536a94c59d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle::layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_8e95ea08c5186c9cd9ecff2ff10c279c0e657e69d5e236be2e32ec8edc173e5d->leave($__internal_8e95ea08c5186c9cd9ecff2ff10c279c0e657e69d5e236be2e32ec8edc173e5d_prof);

        
        $__internal_472a1548002b4095fd735df7b5926280167c43a4ec998de945b38e536a94c59d->leave($__internal_472a1548002b4095fd735df7b5926280167c43a4ec998de945b38e536a94c59d_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_e7e5c66ed8691a89d2876f609e9d44134d48383fb299356becb5ad871bc941cc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e7e5c66ed8691a89d2876f609e9d44134d48383fb299356becb5ad871bc941cc->enter($__internal_e7e5c66ed8691a89d2876f609e9d44134d48383fb299356becb5ad871bc941cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_17b87ef4a9ca505e02426a1df4bc15731a262bd127734613972be3ee866aa323 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_17b87ef4a9ca505e02426a1df4bc15731a262bd127734613972be3ee866aa323->enter($__internal_17b87ef4a9ca505e02426a1df4bc15731a262bd127734613972be3ee866aa323_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_17b87ef4a9ca505e02426a1df4bc15731a262bd127734613972be3ee866aa323->leave($__internal_17b87ef4a9ca505e02426a1df4bc15731a262bd127734613972be3ee866aa323_prof);

        
        $__internal_e7e5c66ed8691a89d2876f609e9d44134d48383fb299356becb5ad871bc941cc->leave($__internal_e7e5c66ed8691a89d2876f609e9d44134d48383fb299356becb5ad871bc941cc_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_1d2a673300ad544ac9381633efae273e924e985b4fde27521bad3ea3922472b6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1d2a673300ad544ac9381633efae273e924e985b4fde27521bad3ea3922472b6->enter($__internal_1d2a673300ad544ac9381633efae273e924e985b4fde27521bad3ea3922472b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_e583bbacb7bb6e6f01bfa44993fb17d2b8746576396fe9e1c40a070184fd3514 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e583bbacb7bb6e6f01bfa44993fb17d2b8746576396fe9e1c40a070184fd3514->enter($__internal_e583bbacb7bb6e6f01bfa44993fb17d2b8746576396fe9e1c40a070184fd3514_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_e583bbacb7bb6e6f01bfa44993fb17d2b8746576396fe9e1c40a070184fd3514->leave($__internal_e583bbacb7bb6e6f01bfa44993fb17d2b8746576396fe9e1c40a070184fd3514_prof);

        
        $__internal_1d2a673300ad544ac9381633efae273e924e985b4fde27521bad3ea3922472b6->leave($__internal_1d2a673300ad544ac9381633efae273e924e985b4fde27521bad3ea3922472b6_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_fd6eadf559459cce455c87ef943499fc5706a5c4d66f4a16792d3b9496d34565 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fd6eadf559459cce455c87ef943499fc5706a5c4d66f4a16792d3b9496d34565->enter($__internal_fd6eadf559459cce455c87ef943499fc5706a5c4d66f4a16792d3b9496d34565_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_9b7752ff69d83d053be886e8c8b5016fea957a236158f4a5e273a37c92880469 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9b7752ff69d83d053be886e8c8b5016fea957a236158f4a5e273a37c92880469->enter($__internal_9b7752ff69d83d053be886e8c8b5016fea957a236158f4a5e273a37c92880469_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_9b7752ff69d83d053be886e8c8b5016fea957a236158f4a5e273a37c92880469->leave($__internal_9b7752ff69d83d053be886e8c8b5016fea957a236158f4a5e273a37c92880469_prof);

        
        $__internal_fd6eadf559459cce455c87ef943499fc5706a5c4d66f4a16792d3b9496d34565->leave($__internal_fd6eadf559459cce455c87ef943499fc5706a5c4d66f4a16792d3b9496d34565_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "TwigBundle::layout.html.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/layout.html.twig");
    }
}
