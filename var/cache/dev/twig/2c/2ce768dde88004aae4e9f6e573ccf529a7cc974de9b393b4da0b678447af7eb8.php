<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_cf60f412588ae2780a275b865561e7a72f352c41cd7bd4e78dbac73daf475542 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3c37727f83090712f3ff818aa845fa4b51839c2c4fd141be0bbdc1837c0701e6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3c37727f83090712f3ff818aa845fa4b51839c2c4fd141be0bbdc1837c0701e6->enter($__internal_3c37727f83090712f3ff818aa845fa4b51839c2c4fd141be0bbdc1837c0701e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        $__internal_4abd7a1ba8f6c25107e248c6565f08b67c2e31a1bf519d9becedff44fd424321 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4abd7a1ba8f6c25107e248c6565f08b67c2e31a1bf519d9becedff44fd424321->enter($__internal_4abd7a1ba8f6c25107e248c6565f08b67c2e31a1bf519d9becedff44fd424321_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_3c37727f83090712f3ff818aa845fa4b51839c2c4fd141be0bbdc1837c0701e6->leave($__internal_3c37727f83090712f3ff818aa845fa4b51839c2c4fd141be0bbdc1837c0701e6_prof);

        
        $__internal_4abd7a1ba8f6c25107e248c6565f08b67c2e31a1bf519d9becedff44fd424321->leave($__internal_4abd7a1ba8f6c25107e248c6565f08b67c2e31a1bf519d9becedff44fd424321_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
", "@Framework/Form/textarea_widget.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/textarea_widget.html.php");
    }
}
