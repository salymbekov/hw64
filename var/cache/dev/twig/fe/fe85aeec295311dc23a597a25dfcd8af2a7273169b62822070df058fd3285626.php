<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_bbc4409889b2e7bc12039131ab2af7f3e62205196a8da2d83e0872c675d97d35 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_612590c9512ff69ec7aec623c118c5435ce3a64143943b103547574d3098e9bc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_612590c9512ff69ec7aec623c118c5435ce3a64143943b103547574d3098e9bc->enter($__internal_612590c9512ff69ec7aec623c118c5435ce3a64143943b103547574d3098e9bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        $__internal_8772e2ab1d485d34037b06f6d5cee4dc5eef222e2da7c5d9a258bbffe3f2ed46 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8772e2ab1d485d34037b06f6d5cee4dc5eef222e2da7c5d9a258bbffe3f2ed46->enter($__internal_8772e2ab1d485d34037b06f6d5cee4dc5eef222e2da7c5d9a258bbffe3f2ed46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
*/
";
        
        $__internal_612590c9512ff69ec7aec623c118c5435ce3a64143943b103547574d3098e9bc->leave($__internal_612590c9512ff69ec7aec623c118c5435ce3a64143943b103547574d3098e9bc_prof);

        
        $__internal_8772e2ab1d485d34037b06f6d5cee4dc5eef222e2da7c5d9a258bbffe3f2ed46->leave($__internal_8772e2ab1d485d34037b06f6d5cee4dc5eef222e2da7c5d9a258bbffe3f2ed46_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.js.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.js.twig");
    }
}
