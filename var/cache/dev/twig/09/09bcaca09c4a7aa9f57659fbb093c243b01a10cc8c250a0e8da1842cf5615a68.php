<?php

/* @Twig/images/icon-support.svg */
class __TwigTemplate_cec79b0f33282fd1c6d08397791f4037eb878c120f1970f18fadaa84224f6bd7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_db85c050a0b960b7027dbf72e36e0ba25a014d3ea4679e0e6b4e41ecfc62d0d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_db85c050a0b960b7027dbf72e36e0ba25a014d3ea4679e0e6b4e41ecfc62d0d4->enter($__internal_db85c050a0b960b7027dbf72e36e0ba25a014d3ea4679e0e6b4e41ecfc62d0d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-support.svg"));

        $__internal_6c72f9ef6fe6d5e77ae9eb021a6692138755991617aa4ac12d786a4c05b427da = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6c72f9ef6fe6d5e77ae9eb021a6692138755991617aa4ac12d786a4c05b427da->enter($__internal_6c72f9ef6fe6d5e77ae9eb021a6692138755991617aa4ac12d786a4c05b427da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-support.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M896 0q182 0 348 71t286 191 191 286 71 348-71 348-191 286-286 191-348 71-348-71-286-191-191-286T0 896t71-348 191-286T548 71 896 0zm0 128q-190 0-361 90l194 194q82-28 167-28t167 28l194-194q-171-90-361-90zM218 1257l194-194q-28-82-28-167t28-167L218 535q-90 171-90 361t90 361zm678 407q190 0 361-90l-194-194q-82 28-167 28t-167-28l-194 194q171 90 361 90zm0-384q159 0 271.5-112.5T1280 896t-112.5-271.5T896 512 624.5 624.5 512 896t112.5 271.5T896 1280zm484-217l194 194q90-171 90-361t-90-361l-194 194q28 82 28 167t-28 167z\"/></svg>
";
        
        $__internal_db85c050a0b960b7027dbf72e36e0ba25a014d3ea4679e0e6b4e41ecfc62d0d4->leave($__internal_db85c050a0b960b7027dbf72e36e0ba25a014d3ea4679e0e6b4e41ecfc62d0d4_prof);

        
        $__internal_6c72f9ef6fe6d5e77ae9eb021a6692138755991617aa4ac12d786a4c05b427da->leave($__internal_6c72f9ef6fe6d5e77ae9eb021a6692138755991617aa4ac12d786a4c05b427da_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-support.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M896 0q182 0 348 71t286 191 191 286 71 348-71 348-191 286-286 191-348 71-348-71-286-191-191-286T0 896t71-348 191-286T548 71 896 0zm0 128q-190 0-361 90l194 194q82-28 167-28t167 28l194-194q-171-90-361-90zM218 1257l194-194q-28-82-28-167t28-167L218 535q-90 171-90 361t90 361zm678 407q190 0 361-90l-194-194q-82 28-167 28t-167-28l-194 194q171 90 361 90zm0-384q159 0 271.5-112.5T1280 896t-112.5-271.5T896 512 624.5 624.5 512 896t112.5 271.5T896 1280zm484-217l194 194q90-171 90-361t-90-361l-194 194q28 82 28 167t-28 167z\"/></svg>
", "@Twig/images/icon-support.svg", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/icon-support.svg");
    }
}
