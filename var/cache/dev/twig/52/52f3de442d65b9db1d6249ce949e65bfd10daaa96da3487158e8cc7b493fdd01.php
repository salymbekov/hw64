<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_a121d622e39f52b9c7f0ae3125aa29cf2130fe269d3282480901b0aab508332b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ffc1096f6d53a0da8ab36e55831fe739d9a99a1e1ca218459999728356132e8c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ffc1096f6d53a0da8ab36e55831fe739d9a99a1e1ca218459999728356132e8c->enter($__internal_ffc1096f6d53a0da8ab36e55831fe739d9a99a1e1ca218459999728356132e8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        $__internal_bc7cd3a3188bdc36342e509213f628f303925f58cb461c84ff58793eeaac260a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bc7cd3a3188bdc36342e509213f628f303925f58cb461c84ff58793eeaac260a->enter($__internal_bc7cd3a3188bdc36342e509213f628f303925f58cb461c84ff58793eeaac260a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_ffc1096f6d53a0da8ab36e55831fe739d9a99a1e1ca218459999728356132e8c->leave($__internal_ffc1096f6d53a0da8ab36e55831fe739d9a99a1e1ca218459999728356132e8c_prof);

        
        $__internal_bc7cd3a3188bdc36342e509213f628f303925f58cb461c84ff58793eeaac260a->leave($__internal_bc7cd3a3188bdc36342e509213f628f303925f58cb461c84ff58793eeaac260a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
", "@Framework/Form/form_widget_simple.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget_simple.html.php");
    }
}
