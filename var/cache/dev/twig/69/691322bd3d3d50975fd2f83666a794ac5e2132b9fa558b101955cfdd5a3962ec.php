<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_d267110e264aedde550ab8a229100a26fbb5d39a4ece8c423ed26c510b349a43 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2f9b815330870bf3cdf542a31eec82865aca0c72fd4154f7ebb7cd3b65df9f0c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2f9b815330870bf3cdf542a31eec82865aca0c72fd4154f7ebb7cd3b65df9f0c->enter($__internal_2f9b815330870bf3cdf542a31eec82865aca0c72fd4154f7ebb7cd3b65df9f0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        $__internal_0f943aaf3336ffd0426e732276c3d1b786471b67480e690591c360855ba5236f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0f943aaf3336ffd0426e732276c3d1b786471b67480e690591c360855ba5236f->enter($__internal_0f943aaf3336ffd0426e732276c3d1b786471b67480e690591c360855ba5236f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
";
        
        $__internal_2f9b815330870bf3cdf542a31eec82865aca0c72fd4154f7ebb7cd3b65df9f0c->leave($__internal_2f9b815330870bf3cdf542a31eec82865aca0c72fd4154f7ebb7cd3b65df9f0c_prof);

        
        $__internal_0f943aaf3336ffd0426e732276c3d1b786471b67480e690591c360855ba5236f->leave($__internal_0f943aaf3336ffd0426e732276c3d1b786471b67480e690591c360855ba5236f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
", "@Framework/FormTable/button_row.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/button_row.html.php");
    }
}
