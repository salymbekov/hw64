<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_5bcadf3a9bce4c4b386f5a36a18fa92cd0642e31fd5801402f87ac9904ba87cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0ee84583be1d08660fa3962ef901ee1dc686ff93ce96083e217e5611fd2f76a3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0ee84583be1d08660fa3962ef901ee1dc686ff93ce96083e217e5611fd2f76a3->enter($__internal_0ee84583be1d08660fa3962ef901ee1dc686ff93ce96083e217e5611fd2f76a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        $__internal_15ad6eb37c94e4cf98a3b5d1354210ebcf3abbd74c4d5942c3d3f0c7bfd51eec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_15ad6eb37c94e4cf98a3b5d1354210ebcf3abbd74c4d5942c3d3f0c7bfd51eec->enter($__internal_15ad6eb37c94e4cf98a3b5d1354210ebcf3abbd74c4d5942c3d3f0c7bfd51eec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_0ee84583be1d08660fa3962ef901ee1dc686ff93ce96083e217e5611fd2f76a3->leave($__internal_0ee84583be1d08660fa3962ef901ee1dc686ff93ce96083e217e5611fd2f76a3_prof);

        
        $__internal_15ad6eb37c94e4cf98a3b5d1354210ebcf3abbd74c4d5942c3d3f0c7bfd51eec->leave($__internal_15ad6eb37c94e4cf98a3b5d1354210ebcf3abbd74c4d5942c3d3f0c7bfd51eec_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/search_widget.html.php");
    }
}
