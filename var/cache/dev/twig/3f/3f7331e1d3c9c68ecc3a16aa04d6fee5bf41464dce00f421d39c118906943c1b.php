<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_a08f2a6b9c33024a2633be2c13f69e184bb1448a28944f18babffe7fe98fd80e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2105ea18f85c65d1c0838129f2b152783e2c26aa96f913ae9761cd5f48159c41 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2105ea18f85c65d1c0838129f2b152783e2c26aa96f913ae9761cd5f48159c41->enter($__internal_2105ea18f85c65d1c0838129f2b152783e2c26aa96f913ae9761cd5f48159c41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        $__internal_963231047f8278fdbafccddb503759a86bf787804b0332b7bec78f9d85c6d8ec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_963231047f8278fdbafccddb503759a86bf787804b0332b7bec78f9d85c6d8ec->enter($__internal_963231047f8278fdbafccddb503759a86bf787804b0332b7bec78f9d85c6d8ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->formEncodeCurrency(\$money_pattern, \$view['form']->block(\$form, 'form_widget_simple')) ?>
";
        
        $__internal_2105ea18f85c65d1c0838129f2b152783e2c26aa96f913ae9761cd5f48159c41->leave($__internal_2105ea18f85c65d1c0838129f2b152783e2c26aa96f913ae9761cd5f48159c41_prof);

        
        $__internal_963231047f8278fdbafccddb503759a86bf787804b0332b7bec78f9d85c6d8ec->leave($__internal_963231047f8278fdbafccddb503759a86bf787804b0332b7bec78f9d85c6d8ec_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->formEncodeCurrency(\$money_pattern, \$view['form']->block(\$form, 'form_widget_simple')) ?>
", "@Framework/Form/money_widget.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/money_widget.html.php");
    }
}
