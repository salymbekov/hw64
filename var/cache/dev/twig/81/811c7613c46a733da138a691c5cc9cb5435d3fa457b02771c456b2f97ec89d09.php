<?php

/* bootstrap_4_horizontal_layout.html.twig */
class __TwigTemplate_7268e8656cfa9c47e49a50ba0ce9bc04e4e4abb6b732a98a6bd70990e3b7b3b6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("bootstrap_4_layout.html.twig", "bootstrap_4_horizontal_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."bootstrap_4_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_label' => array($this, 'block_form_label'),
                'form_label_class' => array($this, 'block_form_label_class'),
                'form_row' => array($this, 'block_form_row'),
                'fieldset_form_row' => array($this, 'block_fieldset_form_row'),
                'submit_row' => array($this, 'block_submit_row'),
                'reset_row' => array($this, 'block_reset_row'),
                'form_group_class' => array($this, 'block_form_group_class'),
                'checkbox_row' => array($this, 'block_checkbox_row'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8de3df8a995131b4cb6e71f208f48ce333f48fe4affbf0a51a781d0bd50eef4c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8de3df8a995131b4cb6e71f208f48ce333f48fe4affbf0a51a781d0bd50eef4c->enter($__internal_8de3df8a995131b4cb6e71f208f48ce333f48fe4affbf0a51a781d0bd50eef4c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_4_horizontal_layout.html.twig"));

        $__internal_d7ecb4abb2f4fd2b056a589e5294164af49428b55b709c5b0de120aa453a1d7a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d7ecb4abb2f4fd2b056a589e5294164af49428b55b709c5b0de120aa453a1d7a->enter($__internal_d7ecb4abb2f4fd2b056a589e5294164af49428b55b709c5b0de120aa453a1d7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_4_horizontal_layout.html.twig"));

        // line 2
        echo "
";
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('form_label', $context, $blocks);
        // line 16
        echo "
";
        // line 17
        $this->displayBlock('form_label_class', $context, $blocks);
        // line 20
        echo "
";
        // line 22
        echo "
";
        // line 23
        $this->displayBlock('form_row', $context, $blocks);
        // line 35
        echo "
";
        // line 36
        $this->displayBlock('fieldset_form_row', $context, $blocks);
        // line 46
        echo "
";
        // line 47
        $this->displayBlock('submit_row', $context, $blocks);
        // line 55
        echo "
";
        // line 56
        $this->displayBlock('reset_row', $context, $blocks);
        // line 64
        echo "
";
        // line 65
        $this->displayBlock('form_group_class', $context, $blocks);
        // line 68
        echo "
";
        // line 69
        $this->displayBlock('checkbox_row', $context, $blocks);
        
        $__internal_8de3df8a995131b4cb6e71f208f48ce333f48fe4affbf0a51a781d0bd50eef4c->leave($__internal_8de3df8a995131b4cb6e71f208f48ce333f48fe4affbf0a51a781d0bd50eef4c_prof);

        
        $__internal_d7ecb4abb2f4fd2b056a589e5294164af49428b55b709c5b0de120aa453a1d7a->leave($__internal_d7ecb4abb2f4fd2b056a589e5294164af49428b55b709c5b0de120aa453a1d7a_prof);

    }

    // line 5
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_ab63ac86a14346e3ce835eb011930877e35a0c2c594696df02aa29ffbae811ab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ab63ac86a14346e3ce835eb011930877e35a0c2c594696df02aa29ffbae811ab->enter($__internal_ab63ac86a14346e3ce835eb011930877e35a0c2c594696df02aa29ffbae811ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_9ac153f5318fe66d431bbc4d412b0777a0b84e42b96731b0338bc7351352970a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9ac153f5318fe66d431bbc4d412b0777a0b84e42b96731b0338bc7351352970a->enter($__internal_9ac153f5318fe66d431bbc4d412b0777a0b84e42b96731b0338bc7351352970a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 6
        if ((($context["label"] ?? $this->getContext($context, "label")) === false)) {
            // line 7
            echo "<div class=\"";
            $this->displayBlock("form_label_class", $context, $blocks);
            echo "\"></div>";
        } else {
            // line 9
            if (( !array_key_exists("expanded", $context) ||  !($context["expanded"] ?? $this->getContext($context, "expanded")))) {
                // line 10
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " col-form-label"))));
            }
            // line 12
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter((((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " ") .             $this->renderBlock("form_label_class", $context, $blocks)))));
            // line 13
            $this->displayParentBlock("form_label", $context, $blocks);
        }
        
        $__internal_9ac153f5318fe66d431bbc4d412b0777a0b84e42b96731b0338bc7351352970a->leave($__internal_9ac153f5318fe66d431bbc4d412b0777a0b84e42b96731b0338bc7351352970a_prof);

        
        $__internal_ab63ac86a14346e3ce835eb011930877e35a0c2c594696df02aa29ffbae811ab->leave($__internal_ab63ac86a14346e3ce835eb011930877e35a0c2c594696df02aa29ffbae811ab_prof);

    }

    // line 17
    public function block_form_label_class($context, array $blocks = array())
    {
        $__internal_9ba638134112fde7c8ed1a455cdfae05cb8101e40ea7c1d7e1715aabec8c1836 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ba638134112fde7c8ed1a455cdfae05cb8101e40ea7c1d7e1715aabec8c1836->enter($__internal_9ba638134112fde7c8ed1a455cdfae05cb8101e40ea7c1d7e1715aabec8c1836_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label_class"));

        $__internal_a9ad7a808823d615a3d22fdb175d1b6bb549a4098f10f703c6b3abad39eabb85 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a9ad7a808823d615a3d22fdb175d1b6bb549a4098f10f703c6b3abad39eabb85->enter($__internal_a9ad7a808823d615a3d22fdb175d1b6bb549a4098f10f703c6b3abad39eabb85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label_class"));

        // line 18
        echo "col-sm-2";
        
        $__internal_a9ad7a808823d615a3d22fdb175d1b6bb549a4098f10f703c6b3abad39eabb85->leave($__internal_a9ad7a808823d615a3d22fdb175d1b6bb549a4098f10f703c6b3abad39eabb85_prof);

        
        $__internal_9ba638134112fde7c8ed1a455cdfae05cb8101e40ea7c1d7e1715aabec8c1836->leave($__internal_9ba638134112fde7c8ed1a455cdfae05cb8101e40ea7c1d7e1715aabec8c1836_prof);

    }

    // line 23
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_e14187b11d4343172aabef48e76aed72dadfc18aa65dd919f28aa576b7bcb01e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e14187b11d4343172aabef48e76aed72dadfc18aa65dd919f28aa576b7bcb01e->enter($__internal_e14187b11d4343172aabef48e76aed72dadfc18aa65dd919f28aa576b7bcb01e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_bba4097c409771e313ec915cf0f28518052062d4328b8c54139df4159b3fb62b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bba4097c409771e313ec915cf0f28518052062d4328b8c54139df4159b3fb62b->enter($__internal_bba4097c409771e313ec915cf0f28518052062d4328b8c54139df4159b3fb62b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 24
        if ((array_key_exists("expanded", $context) && ($context["expanded"] ?? $this->getContext($context, "expanded")))) {
            // line 25
            $this->displayBlock("fieldset_form_row", $context, $blocks);
        } else {
            // line 27
            echo "<div class=\"form-group row";
            if ((( !($context["compound"] ?? $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter(($context["force_error"] ?? $this->getContext($context, "force_error")), false)) : (false))) &&  !($context["valid"] ?? $this->getContext($context, "valid")))) {
                echo " is-invalid";
            }
            echo "\">";
            // line 28
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label');
            // line 29
            echo "<div class=\"";
            $this->displayBlock("form_group_class", $context, $blocks);
            echo "\">";
            // line 30
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
            // line 31
            echo "</div>
    ";
            // line 32
            echo "</div>";
        }
        
        $__internal_bba4097c409771e313ec915cf0f28518052062d4328b8c54139df4159b3fb62b->leave($__internal_bba4097c409771e313ec915cf0f28518052062d4328b8c54139df4159b3fb62b_prof);

        
        $__internal_e14187b11d4343172aabef48e76aed72dadfc18aa65dd919f28aa576b7bcb01e->leave($__internal_e14187b11d4343172aabef48e76aed72dadfc18aa65dd919f28aa576b7bcb01e_prof);

    }

    // line 36
    public function block_fieldset_form_row($context, array $blocks = array())
    {
        $__internal_d8dfcd4a4d048c065638a9af206d1a0f968d98ecf4ee1fdf0eae5fba1fd5c964 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d8dfcd4a4d048c065638a9af206d1a0f968d98ecf4ee1fdf0eae5fba1fd5c964->enter($__internal_d8dfcd4a4d048c065638a9af206d1a0f968d98ecf4ee1fdf0eae5fba1fd5c964_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fieldset_form_row"));

        $__internal_b85ae0e37f2c2239686fb8863e3abfa5eaccc975dbdf258e7a68b7352c624a02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b85ae0e37f2c2239686fb8863e3abfa5eaccc975dbdf258e7a68b7352c624a02->enter($__internal_b85ae0e37f2c2239686fb8863e3abfa5eaccc975dbdf258e7a68b7352c624a02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fieldset_form_row"));

        // line 37
        echo "<fieldset class=\"form-group\">
        <div class=\"row";
        // line 38
        if ((( !($context["compound"] ?? $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter(($context["force_error"] ?? $this->getContext($context, "force_error")), false)) : (false))) &&  !($context["valid"] ?? $this->getContext($context, "valid")))) {
            echo " is-invalid";
        }
        echo "\">";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label');
        // line 40
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 41
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 42
        echo "</div>
        </div>
";
        // line 44
        echo "</fieldset>";
        
        $__internal_b85ae0e37f2c2239686fb8863e3abfa5eaccc975dbdf258e7a68b7352c624a02->leave($__internal_b85ae0e37f2c2239686fb8863e3abfa5eaccc975dbdf258e7a68b7352c624a02_prof);

        
        $__internal_d8dfcd4a4d048c065638a9af206d1a0f968d98ecf4ee1fdf0eae5fba1fd5c964->leave($__internal_d8dfcd4a4d048c065638a9af206d1a0f968d98ecf4ee1fdf0eae5fba1fd5c964_prof);

    }

    // line 47
    public function block_submit_row($context, array $blocks = array())
    {
        $__internal_26cdb608a4a50d668069af36bba4c087f81c154ee3fa94bea5da6314db7c6778 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_26cdb608a4a50d668069af36bba4c087f81c154ee3fa94bea5da6314db7c6778->enter($__internal_26cdb608a4a50d668069af36bba4c087f81c154ee3fa94bea5da6314db7c6778_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_row"));

        $__internal_b8d654305f83e2f1e3059d55ac66d16b21eee638e29e899a3cfc724875e87651 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8d654305f83e2f1e3059d55ac66d16b21eee638e29e899a3cfc724875e87651->enter($__internal_b8d654305f83e2f1e3059d55ac66d16b21eee638e29e899a3cfc724875e87651_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_row"));

        // line 48
        echo "<div class=\"form-group row\">";
        // line 49
        echo "<div class=\"";
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>";
        // line 50
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 51
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 52
        echo "</div>";
        // line 53
        echo "</div>";
        
        $__internal_b8d654305f83e2f1e3059d55ac66d16b21eee638e29e899a3cfc724875e87651->leave($__internal_b8d654305f83e2f1e3059d55ac66d16b21eee638e29e899a3cfc724875e87651_prof);

        
        $__internal_26cdb608a4a50d668069af36bba4c087f81c154ee3fa94bea5da6314db7c6778->leave($__internal_26cdb608a4a50d668069af36bba4c087f81c154ee3fa94bea5da6314db7c6778_prof);

    }

    // line 56
    public function block_reset_row($context, array $blocks = array())
    {
        $__internal_1718572cc24f9ddb7395e8094dec79eec384d0d543db0c051262beadcfa747e9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1718572cc24f9ddb7395e8094dec79eec384d0d543db0c051262beadcfa747e9->enter($__internal_1718572cc24f9ddb7395e8094dec79eec384d0d543db0c051262beadcfa747e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "reset_row"));

        $__internal_184bbae20ded49c48abb3b037960f27e4ee6c16a25ca23edcbd18bb5ed8526f3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_184bbae20ded49c48abb3b037960f27e4ee6c16a25ca23edcbd18bb5ed8526f3->enter($__internal_184bbae20ded49c48abb3b037960f27e4ee6c16a25ca23edcbd18bb5ed8526f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "reset_row"));

        // line 57
        echo "<div class=\"form-group row\">";
        // line 58
        echo "<div class=\"";
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>";
        // line 59
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 60
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 61
        echo "</div>";
        // line 62
        echo "</div>";
        
        $__internal_184bbae20ded49c48abb3b037960f27e4ee6c16a25ca23edcbd18bb5ed8526f3->leave($__internal_184bbae20ded49c48abb3b037960f27e4ee6c16a25ca23edcbd18bb5ed8526f3_prof);

        
        $__internal_1718572cc24f9ddb7395e8094dec79eec384d0d543db0c051262beadcfa747e9->leave($__internal_1718572cc24f9ddb7395e8094dec79eec384d0d543db0c051262beadcfa747e9_prof);

    }

    // line 65
    public function block_form_group_class($context, array $blocks = array())
    {
        $__internal_e9aad181c40d03f074dcae65ac3fc8ffaed33bb3da02eeadf1d931a0a41d775d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e9aad181c40d03f074dcae65ac3fc8ffaed33bb3da02eeadf1d931a0a41d775d->enter($__internal_e9aad181c40d03f074dcae65ac3fc8ffaed33bb3da02eeadf1d931a0a41d775d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_group_class"));

        $__internal_16c3b77dc1b91973afa858e4ae2e668817f3f90c4c2abfe054e9c594df9315d6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_16c3b77dc1b91973afa858e4ae2e668817f3f90c4c2abfe054e9c594df9315d6->enter($__internal_16c3b77dc1b91973afa858e4ae2e668817f3f90c4c2abfe054e9c594df9315d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_group_class"));

        // line 66
        echo "col-sm-10";
        
        $__internal_16c3b77dc1b91973afa858e4ae2e668817f3f90c4c2abfe054e9c594df9315d6->leave($__internal_16c3b77dc1b91973afa858e4ae2e668817f3f90c4c2abfe054e9c594df9315d6_prof);

        
        $__internal_e9aad181c40d03f074dcae65ac3fc8ffaed33bb3da02eeadf1d931a0a41d775d->leave($__internal_e9aad181c40d03f074dcae65ac3fc8ffaed33bb3da02eeadf1d931a0a41d775d_prof);

    }

    // line 69
    public function block_checkbox_row($context, array $blocks = array())
    {
        $__internal_1bfdc80498415384f77346ace3a2d140b5b4ecb2dddddeb650498e4db747e714 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1bfdc80498415384f77346ace3a2d140b5b4ecb2dddddeb650498e4db747e714->enter($__internal_1bfdc80498415384f77346ace3a2d140b5b4ecb2dddddeb650498e4db747e714_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        $__internal_b231da7fef1de63ea449ab91c3a8e84bbae06795013a53189a97b3779311d121 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b231da7fef1de63ea449ab91c3a8e84bbae06795013a53189a97b3779311d121->enter($__internal_b231da7fef1de63ea449ab91c3a8e84bbae06795013a53189a97b3779311d121_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        // line 70
        echo "<div class=\"form-group row\">";
        // line 71
        echo "<div class=\"";
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>";
        // line 72
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 73
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 74
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 75
        echo "</div>";
        // line 76
        echo "</div>";
        
        $__internal_b231da7fef1de63ea449ab91c3a8e84bbae06795013a53189a97b3779311d121->leave($__internal_b231da7fef1de63ea449ab91c3a8e84bbae06795013a53189a97b3779311d121_prof);

        
        $__internal_1bfdc80498415384f77346ace3a2d140b5b4ecb2dddddeb650498e4db747e714->leave($__internal_1bfdc80498415384f77346ace3a2d140b5b4ecb2dddddeb650498e4db747e714_prof);

    }

    public function getTemplateName()
    {
        return "bootstrap_4_horizontal_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  343 => 76,  341 => 75,  339 => 74,  337 => 73,  333 => 72,  329 => 71,  327 => 70,  318 => 69,  308 => 66,  299 => 65,  289 => 62,  287 => 61,  285 => 60,  281 => 59,  277 => 58,  275 => 57,  266 => 56,  256 => 53,  254 => 52,  252 => 51,  248 => 50,  244 => 49,  242 => 48,  233 => 47,  223 => 44,  219 => 42,  217 => 41,  213 => 40,  211 => 39,  206 => 38,  203 => 37,  194 => 36,  183 => 32,  180 => 31,  178 => 30,  174 => 29,  172 => 28,  166 => 27,  163 => 25,  161 => 24,  152 => 23,  142 => 18,  133 => 17,  122 => 13,  120 => 12,  117 => 10,  115 => 9,  110 => 7,  108 => 6,  99 => 5,  89 => 69,  86 => 68,  84 => 65,  81 => 64,  79 => 56,  76 => 55,  74 => 47,  71 => 46,  69 => 36,  66 => 35,  64 => 23,  61 => 22,  58 => 20,  56 => 17,  53 => 16,  51 => 5,  48 => 4,  45 => 2,  14 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"bootstrap_4_layout.html.twig\" %}

{# Labels #}

{% block form_label -%}
    {%- if label is same as(false) -%}
        <div class=\"{{ block('form_label_class') }}\"></div>
    {%- else -%}
        {%- if expanded is not defined or not expanded -%}
            {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' col-form-label')|trim}) -%}
        {%- endif -%}
        {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' ' ~ block('form_label_class'))|trim}) -%}
        {{- parent() -}}
    {%- endif -%}
{%- endblock form_label %}

{% block form_label_class -%}
col-sm-2
{%- endblock form_label_class %}

{# Rows #}

{% block form_row -%}
    {%- if expanded is defined and expanded -%}
        {{ block('fieldset_form_row') }}
    {%- else -%}
        <div class=\"form-group row{% if (not compound or force_error|default(false)) and not valid %} is-invalid{% endif %}\">
            {{- form_label(form) -}}
            <div class=\"{{ block('form_group_class') }}\">
                {{- form_widget(form) -}}
            </div>
    {##}</div>
    {%- endif -%}
{%- endblock form_row %}

{% block fieldset_form_row -%}
    <fieldset class=\"form-group\">
        <div class=\"row{% if (not compound or force_error|default(false)) and not valid %} is-invalid{% endif %}\">
            {{- form_label(form) -}}
            <div class=\"{{ block('form_group_class') }}\">
                {{- form_widget(form) -}}
            </div>
        </div>
{##}</fieldset>
{%- endblock fieldset_form_row %}

{% block submit_row -%}
    <div class=\"form-group row\">{#--#}
        <div class=\"{{ block('form_label_class') }}\"></div>{#--#}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
        </div>{#--#}
    </div>
{%- endblock submit_row %}

{% block reset_row -%}
    <div class=\"form-group row\">{#--#}
        <div class=\"{{ block('form_label_class') }}\"></div>{#--#}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
        </div>{#--#}
    </div>
{%- endblock reset_row %}

{% block form_group_class -%}
col-sm-10
{%- endblock form_group_class %}

{% block checkbox_row -%}
    <div class=\"form-group row\">{#--#}
        <div class=\"{{ block('form_label_class') }}\"></div>{#--#}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
            {{- form_errors(form) -}}
        </div>{#--#}
    </div>
{%- endblock checkbox_row %}
", "bootstrap_4_horizontal_layout.html.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bridge/Twig/Resources/views/Form/bootstrap_4_horizontal_layout.html.twig");
    }
}
