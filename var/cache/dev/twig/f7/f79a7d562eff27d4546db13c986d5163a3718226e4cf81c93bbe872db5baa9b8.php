<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_a424217e1c1d69a2da965b4a9039563b9a8d709b1251dfc1ea953c2b34f0dad2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b4973a589594f160bd9eec4ce2883e9da40082da5135275d6b8ea668bad8d115 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b4973a589594f160bd9eec4ce2883e9da40082da5135275d6b8ea668bad8d115->enter($__internal_b4973a589594f160bd9eec4ce2883e9da40082da5135275d6b8ea668bad8d115_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        $__internal_dccc4a6c7e6316215518241018c908189fe5ab775210bbc88ac1379dc1869e65 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dccc4a6c7e6316215518241018c908189fe5ab775210bbc88ac1379dc1869e65->enter($__internal_dccc4a6c7e6316215518241018c908189fe5ab775210bbc88ac1379dc1869e65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_b4973a589594f160bd9eec4ce2883e9da40082da5135275d6b8ea668bad8d115->leave($__internal_b4973a589594f160bd9eec4ce2883e9da40082da5135275d6b8ea668bad8d115_prof);

        
        $__internal_dccc4a6c7e6316215518241018c908189fe5ab775210bbc88ac1379dc1869e65->leave($__internal_dccc4a6c7e6316215518241018c908189fe5ab775210bbc88ac1379dc1869e65_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
", "@Framework/Form/datetime_widget.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/datetime_widget.html.php");
    }
}
