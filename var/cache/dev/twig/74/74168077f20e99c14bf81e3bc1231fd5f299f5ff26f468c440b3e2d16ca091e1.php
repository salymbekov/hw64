<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_961627600fdc84124a5f61cc4b87bf0a38f8aaabdadb3908c5d4eb691ef85a62 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6e1aec77bbd6341110c90fc988359628bc01e7f17ba414346931fd4cd9116af7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6e1aec77bbd6341110c90fc988359628bc01e7f17ba414346931fd4cd9116af7->enter($__internal_6e1aec77bbd6341110c90fc988359628bc01e7f17ba414346931fd4cd9116af7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        $__internal_e38db69b0ec22241f1ce27e27759b09d951edc27e02a3d1dbc34c1c54d3adb9f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e38db69b0ec22241f1ce27e27759b09d951edc27e02a3d1dbc34c1c54d3adb9f->enter($__internal_e38db69b0ec22241f1ce27e27759b09d951edc27e02a3d1dbc34c1c54d3adb9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_6e1aec77bbd6341110c90fc988359628bc01e7f17ba414346931fd4cd9116af7->leave($__internal_6e1aec77bbd6341110c90fc988359628bc01e7f17ba414346931fd4cd9116af7_prof);

        
        $__internal_e38db69b0ec22241f1ce27e27759b09d951edc27e02a3d1dbc34c1c54d3adb9f->leave($__internal_e38db69b0ec22241f1ce27e27759b09d951edc27e02a3d1dbc34c1c54d3adb9f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/radio_widget.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/radio_widget.html.php");
    }
}
