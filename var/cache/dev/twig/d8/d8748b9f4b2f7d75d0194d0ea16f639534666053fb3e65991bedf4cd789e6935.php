<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_1f8f0836b03c6c80c6f534105ed0b94b74ef2a97e335c72eef63e5a3f2772a41 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a987f51600eb4b64941015e40f4b114952687ce9199ac407282dfe55a5c477b9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a987f51600eb4b64941015e40f4b114952687ce9199ac407282dfe55a5c477b9->enter($__internal_a987f51600eb4b64941015e40f4b114952687ce9199ac407282dfe55a5c477b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        $__internal_f9db67c20c66cea29977657510c340ee325a0d6b77bbfeb61dc72e228153de75 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f9db67c20c66cea29977657510c340ee325a0d6b77bbfeb61dc72e228153de75->enter($__internal_f9db67c20c66cea29977657510c340ee325a0d6b77bbfeb61dc72e228153de75_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_a987f51600eb4b64941015e40f4b114952687ce9199ac407282dfe55a5c477b9->leave($__internal_a987f51600eb4b64941015e40f4b114952687ce9199ac407282dfe55a5c477b9_prof);

        
        $__internal_f9db67c20c66cea29977657510c340ee325a0d6b77bbfeb61dc72e228153de75->leave($__internal_f9db67c20c66cea29977657510c340ee325a0d6b77bbfeb61dc72e228153de75_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_options.html.php");
    }
}
