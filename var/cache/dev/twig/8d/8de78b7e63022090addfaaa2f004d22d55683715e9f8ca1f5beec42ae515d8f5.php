<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_fa96409c1aecd110ed82af243a036e92e15b9d13228836c2c57d197e549842f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_14583c8290fccfabdcf05750e2cacf779e092250c3ae069092712599bcc7e8d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_14583c8290fccfabdcf05750e2cacf779e092250c3ae069092712599bcc7e8d1->enter($__internal_14583c8290fccfabdcf05750e2cacf779e092250c3ae069092712599bcc7e8d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        $__internal_1f5266b933d82e73b3728f4532f480b146b5e9c00bc71688f3b5a2d631fd7768 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f5266b933d82e73b3728f4532f480b146b5e9c00bc71688f3b5a2d631fd7768->enter($__internal_1f5266b933d82e73b3728f4532f480b146b5e9c00bc71688f3b5a2d631fd7768_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_14583c8290fccfabdcf05750e2cacf779e092250c3ae069092712599bcc7e8d1->leave($__internal_14583c8290fccfabdcf05750e2cacf779e092250c3ae069092712599bcc7e8d1_prof);

        
        $__internal_1f5266b933d82e73b3728f4532f480b146b5e9c00bc71688f3b5a2d631fd7768->leave($__internal_1f5266b933d82e73b3728f4532f480b146b5e9c00bc71688f3b5a2d631fd7768_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/submit_widget.html.php");
    }
}
