<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_cc702ef4261b4cb0b7c26d2a3b6c2931104ec984262b2802160394862a6ffaae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_55b1c420810234cd196be7796cc45b29b75b0e2d4bd15d15c2602c143888a778 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_55b1c420810234cd196be7796cc45b29b75b0e2d4bd15d15c2602c143888a778->enter($__internal_55b1c420810234cd196be7796cc45b29b75b0e2d4bd15d15c2602c143888a778_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $__internal_eb36dcad8cda893ab29e8b59ac6f36deacb5ca82a69e00b04cb8541890419424 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eb36dcad8cda893ab29e8b59ac6f36deacb5ca82a69e00b04cb8541890419424->enter($__internal_eb36dcad8cda893ab29e8b59ac6f36deacb5ca82a69e00b04cb8541890419424_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_55b1c420810234cd196be7796cc45b29b75b0e2d4bd15d15c2602c143888a778->leave($__internal_55b1c420810234cd196be7796cc45b29b75b0e2d4bd15d15c2602c143888a778_prof);

        
        $__internal_eb36dcad8cda893ab29e8b59ac6f36deacb5ca82a69e00b04cb8541890419424->leave($__internal_eb36dcad8cda893ab29e8b59ac6f36deacb5ca82a69e00b04cb8541890419424_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ff348fc745f0965d0461e99b8da300679dc2e01c1fdd7c5cb8e88cb89891a32e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ff348fc745f0965d0461e99b8da300679dc2e01c1fdd7c5cb8e88cb89891a32e->enter($__internal_ff348fc745f0965d0461e99b8da300679dc2e01c1fdd7c5cb8e88cb89891a32e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_1837f7316a6630b811b0cab06058ca015325cd70f8f113053a0bc01ab00ecc7e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1837f7316a6630b811b0cab06058ca015325cd70f8f113053a0bc01ab00ecc7e->enter($__internal_1837f7316a6630b811b0cab06058ca015325cd70f8f113053a0bc01ab00ecc7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_1837f7316a6630b811b0cab06058ca015325cd70f8f113053a0bc01ab00ecc7e->leave($__internal_1837f7316a6630b811b0cab06058ca015325cd70f8f113053a0bc01ab00ecc7e_prof);

        
        $__internal_ff348fc745f0965d0461e99b8da300679dc2e01c1fdd7c5cb8e88cb89891a32e->leave($__internal_ff348fc745f0965d0461e99b8da300679dc2e01c1fdd7c5cb8e88cb89891a32e_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/request_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:request.html.twig", "/home/timur/http/hw/hw64/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/request.html.twig");
    }
}
