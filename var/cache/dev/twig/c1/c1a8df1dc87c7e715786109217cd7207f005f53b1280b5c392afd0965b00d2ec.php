<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_082517671404ae28ee63694b49197393e2ac0ce2972f4f32ff774156e20948b8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6f3d068434f8392a0579d5ad47a64a24db2e4b4d04d3c2b3dd423be343e9106f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6f3d068434f8392a0579d5ad47a64a24db2e4b4d04d3c2b3dd423be343e9106f->enter($__internal_6f3d068434f8392a0579d5ad47a64a24db2e4b4d04d3c2b3dd423be343e9106f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        $__internal_a94cac0ee7ffc34263fdad96a79b084639b6896ade097f33a62144533e46264a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a94cac0ee7ffc34263fdad96a79b084639b6896ade097f33a62144533e46264a->enter($__internal_a94cac0ee7ffc34263fdad96a79b084639b6896ade097f33a62144533e46264a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_6f3d068434f8392a0579d5ad47a64a24db2e4b4d04d3c2b3dd423be343e9106f->leave($__internal_6f3d068434f8392a0579d5ad47a64a24db2e4b4d04d3c2b3dd423be343e9106f_prof);

        
        $__internal_a94cac0ee7ffc34263fdad96a79b084639b6896ade097f33a62144533e46264a->leave($__internal_a94cac0ee7ffc34263fdad96a79b084639b6896ade097f33a62144533e46264a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
", "@Framework/Form/number_widget.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/number_widget.html.php");
    }
}
