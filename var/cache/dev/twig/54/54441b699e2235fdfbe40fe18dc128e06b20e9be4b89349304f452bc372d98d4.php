<?php

/* TwigBundle:Exception:error.rdf.twig */
class __TwigTemplate_11fcb2f6937a7ef435e404d441942cd07230ac7a6d025461d922292161730ce9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_07cf01641297850b24aa63dea4f4512dcfda42fcd578bd9d9b8cb60d9ea1a6e6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_07cf01641297850b24aa63dea4f4512dcfda42fcd578bd9d9b8cb60d9ea1a6e6->enter($__internal_07cf01641297850b24aa63dea4f4512dcfda42fcd578bd9d9b8cb60d9ea1a6e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        $__internal_9d1284f72810082e4ae0966677153d3b77fd01835fed33677f7583cc3cc23b5c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9d1284f72810082e4ae0966677153d3b77fd01835fed33677f7583cc3cc23b5c->enter($__internal_9d1284f72810082e4ae0966677153d3b77fd01835fed33677f7583cc3cc23b5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_07cf01641297850b24aa63dea4f4512dcfda42fcd578bd9d9b8cb60d9ea1a6e6->leave($__internal_07cf01641297850b24aa63dea4f4512dcfda42fcd578bd9d9b8cb60d9ea1a6e6_prof);

        
        $__internal_9d1284f72810082e4ae0966677153d3b77fd01835fed33677f7583cc3cc23b5c->leave($__internal_9d1284f72810082e4ae0966677153d3b77fd01835fed33677f7583cc3cc23b5c_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "TwigBundle:Exception:error.rdf.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.rdf.twig");
    }
}
