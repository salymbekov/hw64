<?php

/* FOSUserBundle:Group:show_content.html.twig */
class __TwigTemplate_5357e4d058b16ddc0fd3ca6bce082dc81fa75c4654dad94af3373495a8ac8466 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7ded88cd9775e563b8876cd599d99121849d2f5f472dbe4015d55e92eec64bd3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7ded88cd9775e563b8876cd599d99121849d2f5f472dbe4015d55e92eec64bd3->enter($__internal_7ded88cd9775e563b8876cd599d99121849d2f5f472dbe4015d55e92eec64bd3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        $__internal_e051b9e7444ab629fea6cc8ec30c692da073d79e5efffc2898ab7cc0db2bd620 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e051b9e7444ab629fea6cc8ec30c692da073d79e5efffc2898ab7cc0db2bd620->enter($__internal_e051b9e7444ab629fea6cc8ec30c692da073d79e5efffc2898ab7cc0db2bd620_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_group_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.show.name", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["group"] ?? $this->getContext($context, "group")), "getName", array(), "method"), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_7ded88cd9775e563b8876cd599d99121849d2f5f472dbe4015d55e92eec64bd3->leave($__internal_7ded88cd9775e563b8876cd599d99121849d2f5f472dbe4015d55e92eec64bd3_prof);

        
        $__internal_e051b9e7444ab629fea6cc8ec30c692da073d79e5efffc2898ab7cc0db2bd620->leave($__internal_e051b9e7444ab629fea6cc8ec30c692da073d79e5efffc2898ab7cc0db2bd620_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 4,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"fos_user_group_show\">
    <p>{{ 'group.show.name'|trans }}: {{ group.getName() }}</p>
</div>
", "FOSUserBundle:Group:show_content.html.twig", "/home/timur/http/hw/hw64/vendor/friendsofsymfony/user-bundle/Resources/views/Group/show_content.html.twig");
    }
}
