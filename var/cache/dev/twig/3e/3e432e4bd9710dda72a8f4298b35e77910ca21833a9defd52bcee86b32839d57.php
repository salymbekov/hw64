<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_612f1b5ad3104a3ffaddcc6910cfd3fce732b789ae32a0ddcec9516ca3301d84 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_165ed24ad2254bfc78499a39f1c9b43277610a2f98bf74ebc19fb18dbb2e831f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_165ed24ad2254bfc78499a39f1c9b43277610a2f98bf74ebc19fb18dbb2e831f->enter($__internal_165ed24ad2254bfc78499a39f1c9b43277610a2f98bf74ebc19fb18dbb2e831f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        $__internal_3c0571ec24d85a1aec9ca3feaf6eb129fae817e0421b1168ee175a0998b1c59a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3c0571ec24d85a1aec9ca3feaf6eb129fae817e0421b1168ee175a0998b1c59a->enter($__internal_3c0571ec24d85a1aec9ca3feaf6eb129fae817e0421b1168ee175a0998b1c59a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php foreach (\$attr as \$k => \$v): ?>
<?php if ('placeholder' === \$k || 'title' === \$k): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$v, array(), \$translation_domain) : \$v)) ?>
<?php elseif (true === \$v): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (false !== \$v): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
";
        
        $__internal_165ed24ad2254bfc78499a39f1c9b43277610a2f98bf74ebc19fb18dbb2e831f->leave($__internal_165ed24ad2254bfc78499a39f1c9b43277610a2f98bf74ebc19fb18dbb2e831f_prof);

        
        $__internal_3c0571ec24d85a1aec9ca3feaf6eb129fae817e0421b1168ee175a0998b1c59a->leave($__internal_3c0571ec24d85a1aec9ca3feaf6eb129fae817e0421b1168ee175a0998b1c59a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$attr as \$k => \$v): ?>
<?php if ('placeholder' === \$k || 'title' === \$k): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$v, array(), \$translation_domain) : \$v)) ?>
<?php elseif (true === \$v): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (false !== \$v): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
", "@Framework/Form/attributes.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/attributes.html.php");
    }
}
