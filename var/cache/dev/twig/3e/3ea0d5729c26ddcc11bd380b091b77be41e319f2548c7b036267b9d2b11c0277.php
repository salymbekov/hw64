<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_af5f2e5a4dfedf3262f1071901f8a07a43edebd4fc455220f65109afcc309489 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2d164b4819ea558d5a1f748612a22d07c51b2458b141346d3c315811c6bc04c0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2d164b4819ea558d5a1f748612a22d07c51b2458b141346d3c315811c6bc04c0->enter($__internal_2d164b4819ea558d5a1f748612a22d07c51b2458b141346d3c315811c6bc04c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $__internal_42f00b35d116bd5c5f0542830a22800b3bef0cb7d4bc0d887c7cf89aa9b3c78b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_42f00b35d116bd5c5f0542830a22800b3bef0cb7d4bc0d887c7cf89aa9b3c78b->enter($__internal_42f00b35d116bd5c5f0542830a22800b3bef0cb7d4bc0d887c7cf89aa9b3c78b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2d164b4819ea558d5a1f748612a22d07c51b2458b141346d3c315811c6bc04c0->leave($__internal_2d164b4819ea558d5a1f748612a22d07c51b2458b141346d3c315811c6bc04c0_prof);

        
        $__internal_42f00b35d116bd5c5f0542830a22800b3bef0cb7d4bc0d887c7cf89aa9b3c78b->leave($__internal_42f00b35d116bd5c5f0542830a22800b3bef0cb7d4bc0d887c7cf89aa9b3c78b_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_34e84e698ce81b94146513b360e9814487503922d98428eac3c22e39c8a9f32f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_34e84e698ce81b94146513b360e9814487503922d98428eac3c22e39c8a9f32f->enter($__internal_34e84e698ce81b94146513b360e9814487503922d98428eac3c22e39c8a9f32f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_b45c342e5f955eb636c77b3c2af15615a27a6fa64cc686a9d205755da2b57a87 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b45c342e5f955eb636c77b3c2af15615a27a6fa64cc686a9d205755da2b57a87->enter($__internal_b45c342e5f955eb636c77b3c2af15615a27a6fa64cc686a9d205755da2b57a87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 4)->display($context);
        
        $__internal_b45c342e5f955eb636c77b3c2af15615a27a6fa64cc686a9d205755da2b57a87->leave($__internal_b45c342e5f955eb636c77b3c2af15615a27a6fa64cc686a9d205755da2b57a87_prof);

        
        $__internal_34e84e698ce81b94146513b360e9814487503922d98428eac3c22e39c8a9f32f->leave($__internal_34e84e698ce81b94146513b360e9814487503922d98428eac3c22e39c8a9f32f_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:register.html.twig", "/home/timur/http/hw/hw64/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register.html.twig");
    }
}
