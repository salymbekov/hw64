<?php

/* FOSUserBundle:Group:show.html.twig */
class __TwigTemplate_0ba54df916a08beeca6864d90efb4bc0faa36a77c7b754854349c8aaa7f86569 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b1323515c6f880e2384ad8de77dbde77d5cf33a456ea766d8de3970012b61706 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b1323515c6f880e2384ad8de77dbde77d5cf33a456ea766d8de3970012b61706->enter($__internal_b1323515c6f880e2384ad8de77dbde77d5cf33a456ea766d8de3970012b61706_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $__internal_4d2663f1d1779e61a247e0ebe20b8f13d9234c112a934fd84c4aa1ab59bdc322 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d2663f1d1779e61a247e0ebe20b8f13d9234c112a934fd84c4aa1ab59bdc322->enter($__internal_4d2663f1d1779e61a247e0ebe20b8f13d9234c112a934fd84c4aa1ab59bdc322_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b1323515c6f880e2384ad8de77dbde77d5cf33a456ea766d8de3970012b61706->leave($__internal_b1323515c6f880e2384ad8de77dbde77d5cf33a456ea766d8de3970012b61706_prof);

        
        $__internal_4d2663f1d1779e61a247e0ebe20b8f13d9234c112a934fd84c4aa1ab59bdc322->leave($__internal_4d2663f1d1779e61a247e0ebe20b8f13d9234c112a934fd84c4aa1ab59bdc322_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_94d35e2a759a7f148739a7b21de6604b23e0e01ff7174e1d7a2668d6e12dd635 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_94d35e2a759a7f148739a7b21de6604b23e0e01ff7174e1d7a2668d6e12dd635->enter($__internal_94d35e2a759a7f148739a7b21de6604b23e0e01ff7174e1d7a2668d6e12dd635_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_7fdb7a7bd97d7cf94b1d4db6352640a2db35d5738f4045ea644c46c38d267e77 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7fdb7a7bd97d7cf94b1d4db6352640a2db35d5738f4045ea644c46c38d267e77->enter($__internal_7fdb7a7bd97d7cf94b1d4db6352640a2db35d5738f4045ea644c46c38d267e77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/show_content.html.twig", "FOSUserBundle:Group:show.html.twig", 4)->display($context);
        
        $__internal_7fdb7a7bd97d7cf94b1d4db6352640a2db35d5738f4045ea644c46c38d267e77->leave($__internal_7fdb7a7bd97d7cf94b1d4db6352640a2db35d5738f4045ea644c46c38d267e77_prof);

        
        $__internal_94d35e2a759a7f148739a7b21de6604b23e0e01ff7174e1d7a2668d6e12dd635->leave($__internal_94d35e2a759a7f148739a7b21de6604b23e0e01ff7174e1d7a2668d6e12dd635_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:show.html.twig", "/home/timur/http/hw/hw64/vendor/friendsofsymfony/user-bundle/Resources/views/Group/show.html.twig");
    }
}
