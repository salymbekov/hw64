<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_deeaa895f01219c1b578602d0a66146613080408a34e0a6af5b40ef928fe4621 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4d84d4abd43d1a53328a16cff048ce224564eaabcc57377e22f6c9e020ed343a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4d84d4abd43d1a53328a16cff048ce224564eaabcc57377e22f6c9e020ed343a->enter($__internal_4d84d4abd43d1a53328a16cff048ce224564eaabcc57377e22f6c9e020ed343a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        $__internal_a697b0f8f355b3855e4ac757da69eeba50fdcc32cd03c23571d21cf700c28408 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a697b0f8f355b3855e4ac757da69eeba50fdcc32cd03c23571d21cf700c28408->enter($__internal_a697b0f8f355b3855e4ac757da69eeba50fdcc32cd03c23571d21cf700c28408_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_4d84d4abd43d1a53328a16cff048ce224564eaabcc57377e22f6c9e020ed343a->leave($__internal_4d84d4abd43d1a53328a16cff048ce224564eaabcc57377e22f6c9e020ed343a_prof);

        
        $__internal_a697b0f8f355b3855e4ac757da69eeba50fdcc32cd03c23571d21cf700c28408->leave($__internal_a697b0f8f355b3855e4ac757da69eeba50fdcc32cd03c23571d21cf700c28408_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
", "@Framework/Form/form_widget.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget.html.php");
    }
}
