<?php

/* WebProfilerBundle:Collector:exception.html.twig */
class __TwigTemplate_2447dff499eb1e5fcecdd3fa14eaf8f46dd5eebe2fddf9a6a3c784e9a89474c6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_592bf031c0b69e4eb82d6332dd2d93499516d089ef1fc4bea60de86b89e73b33 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_592bf031c0b69e4eb82d6332dd2d93499516d089ef1fc4bea60de86b89e73b33->enter($__internal_592bf031c0b69e4eb82d6332dd2d93499516d089ef1fc4bea60de86b89e73b33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $__internal_97594c73db295de6564be54364be6ce117ca10eaef9e42e80d1ed073d317128e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_97594c73db295de6564be54364be6ce117ca10eaef9e42e80d1ed073d317128e->enter($__internal_97594c73db295de6564be54364be6ce117ca10eaef9e42e80d1ed073d317128e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_592bf031c0b69e4eb82d6332dd2d93499516d089ef1fc4bea60de86b89e73b33->leave($__internal_592bf031c0b69e4eb82d6332dd2d93499516d089ef1fc4bea60de86b89e73b33_prof);

        
        $__internal_97594c73db295de6564be54364be6ce117ca10eaef9e42e80d1ed073d317128e->leave($__internal_97594c73db295de6564be54364be6ce117ca10eaef9e42e80d1ed073d317128e_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_59a08d9d40a98f08ad3f2915f7196d0587e21975afed926aa98c383a53e7b489 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_59a08d9d40a98f08ad3f2915f7196d0587e21975afed926aa98c383a53e7b489->enter($__internal_59a08d9d40a98f08ad3f2915f7196d0587e21975afed926aa98c383a53e7b489_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_b1b63258e905dc0f9befe993ed8498df7028628da1856e948a227dc29e222c6a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b1b63258e905dc0f9befe993ed8498df7028628da1856e948a227dc29e222c6a->enter($__internal_b1b63258e905dc0f9befe993ed8498df7028628da1856e948a227dc29e222c6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_b1b63258e905dc0f9befe993ed8498df7028628da1856e948a227dc29e222c6a->leave($__internal_b1b63258e905dc0f9befe993ed8498df7028628da1856e948a227dc29e222c6a_prof);

        
        $__internal_59a08d9d40a98f08ad3f2915f7196d0587e21975afed926aa98c383a53e7b489->leave($__internal_59a08d9d40a98f08ad3f2915f7196d0587e21975afed926aa98c383a53e7b489_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_dbea72ad1b20612896be3233dedf186ddaec7ee545c46caaf494e40ef6fbd6ff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dbea72ad1b20612896be3233dedf186ddaec7ee545c46caaf494e40ef6fbd6ff->enter($__internal_dbea72ad1b20612896be3233dedf186ddaec7ee545c46caaf494e40ef6fbd6ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_9b535441961028af8794717e3369dc3f728dca3869d9305e869120f68462ecb9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9b535441961028af8794717e3369dc3f728dca3869d9305e869120f68462ecb9->enter($__internal_9b535441961028af8794717e3369dc3f728dca3869d9305e869120f68462ecb9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_9b535441961028af8794717e3369dc3f728dca3869d9305e869120f68462ecb9->leave($__internal_9b535441961028af8794717e3369dc3f728dca3869d9305e869120f68462ecb9_prof);

        
        $__internal_dbea72ad1b20612896be3233dedf186ddaec7ee545c46caaf494e40ef6fbd6ff->leave($__internal_dbea72ad1b20612896be3233dedf186ddaec7ee545c46caaf494e40ef6fbd6ff_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_12f855c8fc030bd23dc6eb964e4bd6b0f08427cf893029117f00e32dea762e96 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_12f855c8fc030bd23dc6eb964e4bd6b0f08427cf893029117f00e32dea762e96->enter($__internal_12f855c8fc030bd23dc6eb964e4bd6b0f08427cf893029117f00e32dea762e96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_0960429ecbd883a5d06bf900ef92312306b4ac05c615bb19895333174e4da145 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0960429ecbd883a5d06bf900ef92312306b4ac05c615bb19895333174e4da145->enter($__internal_0960429ecbd883a5d06bf900ef92312306b4ac05c615bb19895333174e4da145_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_0960429ecbd883a5d06bf900ef92312306b4ac05c615bb19895333174e4da145->leave($__internal_0960429ecbd883a5d06bf900ef92312306b4ac05c615bb19895333174e4da145_prof);

        
        $__internal_12f855c8fc030bd23dc6eb964e4bd6b0f08427cf893029117f00e32dea762e96->leave($__internal_12f855c8fc030bd23dc6eb964e4bd6b0f08427cf893029117f00e32dea762e96_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "WebProfilerBundle:Collector:exception.html.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
