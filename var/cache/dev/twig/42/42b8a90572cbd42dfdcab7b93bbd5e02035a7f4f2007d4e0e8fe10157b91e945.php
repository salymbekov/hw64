<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_42ea571de9951e7cf09f018675f20e4b1509a97bf628678e9df353431c024aa5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_19941a7d8682a77ca8b1c111864024cede015ce73c29cb4abefc8a5638aa0c47 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_19941a7d8682a77ca8b1c111864024cede015ce73c29cb4abefc8a5638aa0c47->enter($__internal_19941a7d8682a77ca8b1c111864024cede015ce73c29cb4abefc8a5638aa0c47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        $__internal_1a5247a7dd27e2806c2a759c43926f6a032bf8f96f9739672533fbf7473dd023 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1a5247a7dd27e2806c2a759c43926f6a032bf8f96f9739672533fbf7473dd023->enter($__internal_1a5247a7dd27e2806c2a759c43926f6a032bf8f96f9739672533fbf7473dd023_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_19941a7d8682a77ca8b1c111864024cede015ce73c29cb4abefc8a5638aa0c47->leave($__internal_19941a7d8682a77ca8b1c111864024cede015ce73c29cb4abefc8a5638aa0c47_prof);

        
        $__internal_1a5247a7dd27e2806c2a759c43926f6a032bf8f96f9739672533fbf7473dd023->leave($__internal_1a5247a7dd27e2806c2a759c43926f6a032bf8f96f9739672533fbf7473dd023_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
", "@Framework/Form/form_widget_compound.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget_compound.html.php");
    }
}
