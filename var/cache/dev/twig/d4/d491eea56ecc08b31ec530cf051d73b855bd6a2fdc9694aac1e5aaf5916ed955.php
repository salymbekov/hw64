<?php

/* @Framework/Form/color_widget.html.php */
class __TwigTemplate_0274aa7377c05e1722bde0bed71c16a32dcd591ade06a47efc4a544281f9cd43 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bea28e54ad0ef97aa172819223a3c2e5525f16f0004f01da7c58ef9ae5e31860 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bea28e54ad0ef97aa172819223a3c2e5525f16f0004f01da7c58ef9ae5e31860->enter($__internal_bea28e54ad0ef97aa172819223a3c2e5525f16f0004f01da7c58ef9ae5e31860_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/color_widget.html.php"));

        $__internal_afbf97fb5738b72ed9f6eb8f55e26224f6640342813f96850ab4eae555139a21 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_afbf97fb5738b72ed9f6eb8f55e26224f6640342813f96850ab4eae555139a21->enter($__internal_afbf97fb5738b72ed9f6eb8f55e26224f6640342813f96850ab4eae555139a21_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/color_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'color'));
";
        
        $__internal_bea28e54ad0ef97aa172819223a3c2e5525f16f0004f01da7c58ef9ae5e31860->leave($__internal_bea28e54ad0ef97aa172819223a3c2e5525f16f0004f01da7c58ef9ae5e31860_prof);

        
        $__internal_afbf97fb5738b72ed9f6eb8f55e26224f6640342813f96850ab4eae555139a21->leave($__internal_afbf97fb5738b72ed9f6eb8f55e26224f6640342813f96850ab4eae555139a21_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/color_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'color'));
", "@Framework/Form/color_widget.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/color_widget.html.php");
    }
}
