<?php

/* bootstrap_3_layout.html.twig */
class __TwigTemplate_47b31eab8fa5282682f358cd13df98c82de525a45edb22c151d5203ad5acd291 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("bootstrap_base_layout.html.twig", "bootstrap_3_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."bootstrap_base_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_widget_simple' => array($this, 'block_form_widget_simple'),
                'button_widget' => array($this, 'block_button_widget'),
                'money_widget' => array($this, 'block_money_widget'),
                'checkbox_widget' => array($this, 'block_checkbox_widget'),
                'radio_widget' => array($this, 'block_radio_widget'),
                'form_label' => array($this, 'block_form_label'),
                'choice_label' => array($this, 'block_choice_label'),
                'checkbox_label' => array($this, 'block_checkbox_label'),
                'radio_label' => array($this, 'block_radio_label'),
                'checkbox_radio_label' => array($this, 'block_checkbox_radio_label'),
                'form_row' => array($this, 'block_form_row'),
                'button_row' => array($this, 'block_button_row'),
                'choice_row' => array($this, 'block_choice_row'),
                'date_row' => array($this, 'block_date_row'),
                'time_row' => array($this, 'block_time_row'),
                'datetime_row' => array($this, 'block_datetime_row'),
                'checkbox_row' => array($this, 'block_checkbox_row'),
                'radio_row' => array($this, 'block_radio_row'),
                'form_errors' => array($this, 'block_form_errors'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ef5bbbb2e0d02cee4f8002dd1ee948c760875ba602b67877f7bbe37eb0fceb3b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ef5bbbb2e0d02cee4f8002dd1ee948c760875ba602b67877f7bbe37eb0fceb3b->enter($__internal_ef5bbbb2e0d02cee4f8002dd1ee948c760875ba602b67877f7bbe37eb0fceb3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_3_layout.html.twig"));

        $__internal_056e50c13ac4b7a103a0cd7853d07b2302e839f38f12787c16f175ae3925ac3b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_056e50c13ac4b7a103a0cd7853d07b2302e839f38f12787c16f175ae3925ac3b->enter($__internal_056e50c13ac4b7a103a0cd7853d07b2302e839f38f12787c16f175ae3925ac3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_3_layout.html.twig"));

        // line 2
        echo "
";
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('form_widget_simple', $context, $blocks);
        // line 11
        echo "
";
        // line 12
        $this->displayBlock('button_widget', $context, $blocks);
        // line 16
        echo "
";
        // line 17
        $this->displayBlock('money_widget', $context, $blocks);
        // line 34
        echo "
";
        // line 35
        $this->displayBlock('checkbox_widget', $context, $blocks);
        // line 45
        echo "
";
        // line 46
        $this->displayBlock('radio_widget', $context, $blocks);
        // line 56
        echo "
";
        // line 58
        echo "
";
        // line 59
        $this->displayBlock('form_label', $context, $blocks);
        // line 63
        echo "
";
        // line 64
        $this->displayBlock('choice_label', $context, $blocks);
        // line 69
        echo "
";
        // line 70
        $this->displayBlock('checkbox_label', $context, $blocks);
        // line 75
        echo "
";
        // line 76
        $this->displayBlock('radio_label', $context, $blocks);
        // line 81
        echo "
";
        // line 82
        $this->displayBlock('checkbox_radio_label', $context, $blocks);
        // line 106
        echo "
";
        // line 108
        echo "
";
        // line 109
        $this->displayBlock('form_row', $context, $blocks);
        // line 116
        echo "
";
        // line 117
        $this->displayBlock('button_row', $context, $blocks);
        // line 122
        echo "
";
        // line 123
        $this->displayBlock('choice_row', $context, $blocks);
        // line 127
        echo "
";
        // line 128
        $this->displayBlock('date_row', $context, $blocks);
        // line 132
        echo "
";
        // line 133
        $this->displayBlock('time_row', $context, $blocks);
        // line 137
        echo "
";
        // line 138
        $this->displayBlock('datetime_row', $context, $blocks);
        // line 142
        echo "
";
        // line 143
        $this->displayBlock('checkbox_row', $context, $blocks);
        // line 149
        echo "
";
        // line 150
        $this->displayBlock('radio_row', $context, $blocks);
        // line 156
        echo "
";
        // line 158
        echo "
";
        // line 159
        $this->displayBlock('form_errors', $context, $blocks);
        
        $__internal_ef5bbbb2e0d02cee4f8002dd1ee948c760875ba602b67877f7bbe37eb0fceb3b->leave($__internal_ef5bbbb2e0d02cee4f8002dd1ee948c760875ba602b67877f7bbe37eb0fceb3b_prof);

        
        $__internal_056e50c13ac4b7a103a0cd7853d07b2302e839f38f12787c16f175ae3925ac3b->leave($__internal_056e50c13ac4b7a103a0cd7853d07b2302e839f38f12787c16f175ae3925ac3b_prof);

    }

    // line 5
    public function block_form_widget_simple($context, array $blocks = array())
    {
        $__internal_5afe6d5f766c6dcf6b5b5a87cbe1f0353d2d268d936e1423aa3179c16fe39ff6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5afe6d5f766c6dcf6b5b5a87cbe1f0353d2d268d936e1423aa3179c16fe39ff6->enter($__internal_5afe6d5f766c6dcf6b5b5a87cbe1f0353d2d268d936e1423aa3179c16fe39ff6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        $__internal_dac155dc95085becb9be4a7f2ada1a709cd2a1be1b65660503601db913b92849 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dac155dc95085becb9be4a7f2ada1a709cd2a1be1b65660503601db913b92849->enter($__internal_dac155dc95085becb9be4a7f2ada1a709cd2a1be1b65660503601db913b92849_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        // line 6
        if (( !array_key_exists("type", $context) || !twig_in_filter(($context["type"] ?? $this->getContext($context, "type")), array(0 => "file", 1 => "hidden")))) {
            // line 7
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control"))));
        }
        // line 9
        $this->displayParentBlock("form_widget_simple", $context, $blocks);
        
        $__internal_dac155dc95085becb9be4a7f2ada1a709cd2a1be1b65660503601db913b92849->leave($__internal_dac155dc95085becb9be4a7f2ada1a709cd2a1be1b65660503601db913b92849_prof);

        
        $__internal_5afe6d5f766c6dcf6b5b5a87cbe1f0353d2d268d936e1423aa3179c16fe39ff6->leave($__internal_5afe6d5f766c6dcf6b5b5a87cbe1f0353d2d268d936e1423aa3179c16fe39ff6_prof);

    }

    // line 12
    public function block_button_widget($context, array $blocks = array())
    {
        $__internal_5fe238218c575dc6acfc11c66fe90834e64d043d7f1f25cdc0f9fd4b2402b23f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5fe238218c575dc6acfc11c66fe90834e64d043d7f1f25cdc0f9fd4b2402b23f->enter($__internal_5fe238218c575dc6acfc11c66fe90834e64d043d7f1f25cdc0f9fd4b2402b23f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        $__internal_b989773bbd55d31e7bdf762b095b17f21e98fbfb2bd3f1c3e1f6a5a6451c2b87 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b989773bbd55d31e7bdf762b095b17f21e98fbfb2bd3f1c3e1f6a5a6451c2b87->enter($__internal_b989773bbd55d31e7bdf762b095b17f21e98fbfb2bd3f1c3e1f6a5a6451c2b87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        // line 13
        $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "btn-default")) : ("btn-default")) . " btn"))));
        // line 14
        $this->displayParentBlock("button_widget", $context, $blocks);
        
        $__internal_b989773bbd55d31e7bdf762b095b17f21e98fbfb2bd3f1c3e1f6a5a6451c2b87->leave($__internal_b989773bbd55d31e7bdf762b095b17f21e98fbfb2bd3f1c3e1f6a5a6451c2b87_prof);

        
        $__internal_5fe238218c575dc6acfc11c66fe90834e64d043d7f1f25cdc0f9fd4b2402b23f->leave($__internal_5fe238218c575dc6acfc11c66fe90834e64d043d7f1f25cdc0f9fd4b2402b23f_prof);

    }

    // line 17
    public function block_money_widget($context, array $blocks = array())
    {
        $__internal_e2fccc48d0bfd699435effd6c797904f0b8fb86d3bb03cb04ccfa9f7bae3e6d3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e2fccc48d0bfd699435effd6c797904f0b8fb86d3bb03cb04ccfa9f7bae3e6d3->enter($__internal_e2fccc48d0bfd699435effd6c797904f0b8fb86d3bb03cb04ccfa9f7bae3e6d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        $__internal_69e41e0a93d640a125656cf8013748442e837c588566a5eb8a57d414fa12bdff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_69e41e0a93d640a125656cf8013748442e837c588566a5eb8a57d414fa12bdff->enter($__internal_69e41e0a93d640a125656cf8013748442e837c588566a5eb8a57d414fa12bdff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        // line 18
        $context["prepend"] =  !(is_string($__internal_70e701ba5f4403f9072a615dff3487a31e367e22a936a040ec490273f921fae5 = ($context["money_pattern"] ?? $this->getContext($context, "money_pattern"))) && is_string($__internal_9cd218535b1b32c0ffcc5a250b8d23350732b88ef6abd815fac1f3fb13d6e2ff = "{{") && ('' === $__internal_9cd218535b1b32c0ffcc5a250b8d23350732b88ef6abd815fac1f3fb13d6e2ff || 0 === strpos($__internal_70e701ba5f4403f9072a615dff3487a31e367e22a936a040ec490273f921fae5, $__internal_9cd218535b1b32c0ffcc5a250b8d23350732b88ef6abd815fac1f3fb13d6e2ff)));
        // line 19
        echo "    ";
        $context["append"] =  !(is_string($__internal_e5a5694a82968c884a75a89e86c4041f60ba2a47fd9ac09ef220920f0b7c4c2c = ($context["money_pattern"] ?? $this->getContext($context, "money_pattern"))) && is_string($__internal_0009bb1874976a0d9bf0010837298e8c1ef2630d2d76b2b7c5f6506d2ffb5e52 = "}}") && ('' === $__internal_0009bb1874976a0d9bf0010837298e8c1ef2630d2d76b2b7c5f6506d2ffb5e52 || $__internal_0009bb1874976a0d9bf0010837298e8c1ef2630d2d76b2b7c5f6506d2ffb5e52 === substr($__internal_e5a5694a82968c884a75a89e86c4041f60ba2a47fd9ac09ef220920f0b7c4c2c, -strlen($__internal_0009bb1874976a0d9bf0010837298e8c1ef2630d2d76b2b7c5f6506d2ffb5e52))));
        // line 20
        echo "    ";
        if ((($context["prepend"] ?? $this->getContext($context, "prepend")) || ($context["append"] ?? $this->getContext($context, "append")))) {
            // line 21
            echo "        <div class=\"input-group\">
            ";
            // line 22
            if (($context["prepend"] ?? $this->getContext($context, "prepend"))) {
                // line 23
                echo "                <span class=\"input-group-addon\">";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->encodeCurrency($this->env, ($context["money_pattern"] ?? $this->getContext($context, "money_pattern")));
                echo "</span>
            ";
            }
            // line 25
            $this->displayBlock("form_widget_simple", $context, $blocks);
            // line 26
            if (($context["append"] ?? $this->getContext($context, "append"))) {
                // line 27
                echo "                <span class=\"input-group-addon\">";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->encodeCurrency($this->env, ($context["money_pattern"] ?? $this->getContext($context, "money_pattern")));
                echo "</span>
            ";
            }
            // line 29
            echo "        </div>
    ";
        } else {
            // line 31
            $this->displayBlock("form_widget_simple", $context, $blocks);
        }
        
        $__internal_69e41e0a93d640a125656cf8013748442e837c588566a5eb8a57d414fa12bdff->leave($__internal_69e41e0a93d640a125656cf8013748442e837c588566a5eb8a57d414fa12bdff_prof);

        
        $__internal_e2fccc48d0bfd699435effd6c797904f0b8fb86d3bb03cb04ccfa9f7bae3e6d3->leave($__internal_e2fccc48d0bfd699435effd6c797904f0b8fb86d3bb03cb04ccfa9f7bae3e6d3_prof);

    }

    // line 35
    public function block_checkbox_widget($context, array $blocks = array())
    {
        $__internal_397f958b78135a97b9588bf5073a0700fc084d88042592030cd4fa8c72d27a4f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_397f958b78135a97b9588bf5073a0700fc084d88042592030cd4fa8c72d27a4f->enter($__internal_397f958b78135a97b9588bf5073a0700fc084d88042592030cd4fa8c72d27a4f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        $__internal_3b1bda953f10cdc31b78a5c506ba05c32a0d16823dfad21accabb6213135b97e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3b1bda953f10cdc31b78a5c506ba05c32a0d16823dfad21accabb6213135b97e->enter($__internal_3b1bda953f10cdc31b78a5c506ba05c32a0d16823dfad21accabb6213135b97e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        // line 36
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter(($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")), (($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")))) : ((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : (""))));
        // line 37
        if (twig_in_filter("checkbox-inline", ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")))) {
            // line 38
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
        } else {
            // line 40
            echo "<div class=\"checkbox\">";
            // line 41
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
            // line 42
            echo "</div>";
        }
        
        $__internal_3b1bda953f10cdc31b78a5c506ba05c32a0d16823dfad21accabb6213135b97e->leave($__internal_3b1bda953f10cdc31b78a5c506ba05c32a0d16823dfad21accabb6213135b97e_prof);

        
        $__internal_397f958b78135a97b9588bf5073a0700fc084d88042592030cd4fa8c72d27a4f->leave($__internal_397f958b78135a97b9588bf5073a0700fc084d88042592030cd4fa8c72d27a4f_prof);

    }

    // line 46
    public function block_radio_widget($context, array $blocks = array())
    {
        $__internal_447ff62744463b5cb374c93c16db2a910ffd752f06200f91ef3720977c04f4ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_447ff62744463b5cb374c93c16db2a910ffd752f06200f91ef3720977c04f4ef->enter($__internal_447ff62744463b5cb374c93c16db2a910ffd752f06200f91ef3720977c04f4ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        $__internal_fe8d40ae33f86cce1d16d6503cb2f35af8fb8fb564b5304bb834431d19dfdcea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fe8d40ae33f86cce1d16d6503cb2f35af8fb8fb564b5304bb834431d19dfdcea->enter($__internal_fe8d40ae33f86cce1d16d6503cb2f35af8fb8fb564b5304bb834431d19dfdcea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        // line 47
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter(($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")), (($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")))) : ((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : (""))));
        // line 48
        if (twig_in_filter("radio-inline", ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")))) {
            // line 49
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
        } else {
            // line 51
            echo "<div class=\"radio\">";
            // line 52
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
            // line 53
            echo "</div>";
        }
        
        $__internal_fe8d40ae33f86cce1d16d6503cb2f35af8fb8fb564b5304bb834431d19dfdcea->leave($__internal_fe8d40ae33f86cce1d16d6503cb2f35af8fb8fb564b5304bb834431d19dfdcea_prof);

        
        $__internal_447ff62744463b5cb374c93c16db2a910ffd752f06200f91ef3720977c04f4ef->leave($__internal_447ff62744463b5cb374c93c16db2a910ffd752f06200f91ef3720977c04f4ef_prof);

    }

    // line 59
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_bb13f21d40a450578322b1c99dbf5c7faece442b499340aad639e4383f0e8e77 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bb13f21d40a450578322b1c99dbf5c7faece442b499340aad639e4383f0e8e77->enter($__internal_bb13f21d40a450578322b1c99dbf5c7faece442b499340aad639e4383f0e8e77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_5df949407e036e48a5b47dad94161b6d1a75ddaa0be4a48d5a94da82be6260ce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5df949407e036e48a5b47dad94161b6d1a75ddaa0be4a48d5a94da82be6260ce->enter($__internal_5df949407e036e48a5b47dad94161b6d1a75ddaa0be4a48d5a94da82be6260ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 60
        $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " control-label"))));
        // line 61
        $this->displayParentBlock("form_label", $context, $blocks);
        
        $__internal_5df949407e036e48a5b47dad94161b6d1a75ddaa0be4a48d5a94da82be6260ce->leave($__internal_5df949407e036e48a5b47dad94161b6d1a75ddaa0be4a48d5a94da82be6260ce_prof);

        
        $__internal_bb13f21d40a450578322b1c99dbf5c7faece442b499340aad639e4383f0e8e77->leave($__internal_bb13f21d40a450578322b1c99dbf5c7faece442b499340aad639e4383f0e8e77_prof);

    }

    // line 64
    public function block_choice_label($context, array $blocks = array())
    {
        $__internal_52a5c0c8a7dc5a8f725a423b213eb083832567c37e14f0a2430dffdde2ff2e10 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_52a5c0c8a7dc5a8f725a423b213eb083832567c37e14f0a2430dffdde2ff2e10->enter($__internal_52a5c0c8a7dc5a8f725a423b213eb083832567c37e14f0a2430dffdde2ff2e10_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_label"));

        $__internal_78d7cd1bf6719bda1f7c8202b98f5c10d2e2113c0a6ccd122a939aa46f297fbd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_78d7cd1bf6719bda1f7c8202b98f5c10d2e2113c0a6ccd122a939aa46f297fbd->enter($__internal_78d7cd1bf6719bda1f7c8202b98f5c10d2e2113c0a6ccd122a939aa46f297fbd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_label"));

        // line 66
        $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(twig_replace_filter((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")), array("checkbox-inline" => "", "radio-inline" => "")))));
        // line 67
        $this->displayBlock("form_label", $context, $blocks);
        
        $__internal_78d7cd1bf6719bda1f7c8202b98f5c10d2e2113c0a6ccd122a939aa46f297fbd->leave($__internal_78d7cd1bf6719bda1f7c8202b98f5c10d2e2113c0a6ccd122a939aa46f297fbd_prof);

        
        $__internal_52a5c0c8a7dc5a8f725a423b213eb083832567c37e14f0a2430dffdde2ff2e10->leave($__internal_52a5c0c8a7dc5a8f725a423b213eb083832567c37e14f0a2430dffdde2ff2e10_prof);

    }

    // line 70
    public function block_checkbox_label($context, array $blocks = array())
    {
        $__internal_f09f3999333387ee25a2a802db7361362b81a9727370dc93a02d18dead896be0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f09f3999333387ee25a2a802db7361362b81a9727370dc93a02d18dead896be0->enter($__internal_f09f3999333387ee25a2a802db7361362b81a9727370dc93a02d18dead896be0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_label"));

        $__internal_d7cc3078d98069f89b5ddd0f051875dccc367a90ab6202f98f4d96dfcef14834 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d7cc3078d98069f89b5ddd0f051875dccc367a90ab6202f98f4d96dfcef14834->enter($__internal_d7cc3078d98069f89b5ddd0f051875dccc367a90ab6202f98f4d96dfcef14834_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_label"));

        // line 71
        $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("for" => ($context["id"] ?? $this->getContext($context, "id"))));
        // line 73
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_d7cc3078d98069f89b5ddd0f051875dccc367a90ab6202f98f4d96dfcef14834->leave($__internal_d7cc3078d98069f89b5ddd0f051875dccc367a90ab6202f98f4d96dfcef14834_prof);

        
        $__internal_f09f3999333387ee25a2a802db7361362b81a9727370dc93a02d18dead896be0->leave($__internal_f09f3999333387ee25a2a802db7361362b81a9727370dc93a02d18dead896be0_prof);

    }

    // line 76
    public function block_radio_label($context, array $blocks = array())
    {
        $__internal_86f533349204d71fbee624a3946cc74d283cf5c9022b88289493eaaea302d616 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_86f533349204d71fbee624a3946cc74d283cf5c9022b88289493eaaea302d616->enter($__internal_86f533349204d71fbee624a3946cc74d283cf5c9022b88289493eaaea302d616_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_label"));

        $__internal_2eac020977d44d60263f6ff90dcb3fb05da308e05f0d235a473084276eefdfaa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2eac020977d44d60263f6ff90dcb3fb05da308e05f0d235a473084276eefdfaa->enter($__internal_2eac020977d44d60263f6ff90dcb3fb05da308e05f0d235a473084276eefdfaa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_label"));

        // line 77
        $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("for" => ($context["id"] ?? $this->getContext($context, "id"))));
        // line 79
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_2eac020977d44d60263f6ff90dcb3fb05da308e05f0d235a473084276eefdfaa->leave($__internal_2eac020977d44d60263f6ff90dcb3fb05da308e05f0d235a473084276eefdfaa_prof);

        
        $__internal_86f533349204d71fbee624a3946cc74d283cf5c9022b88289493eaaea302d616->leave($__internal_86f533349204d71fbee624a3946cc74d283cf5c9022b88289493eaaea302d616_prof);

    }

    // line 82
    public function block_checkbox_radio_label($context, array $blocks = array())
    {
        $__internal_fa4e1de700b7260979fdb913e252534dba79af42c3e022ce0041f8479dd4f10b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fa4e1de700b7260979fdb913e252534dba79af42c3e022ce0041f8479dd4f10b->enter($__internal_fa4e1de700b7260979fdb913e252534dba79af42c3e022ce0041f8479dd4f10b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        $__internal_79dfa3cc9928f38f2dc084a253fdcb5b126d75cbbe37a4ff9f150456babc2196 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_79dfa3cc9928f38f2dc084a253fdcb5b126d75cbbe37a4ff9f150456babc2196->enter($__internal_79dfa3cc9928f38f2dc084a253fdcb5b126d75cbbe37a4ff9f150456babc2196_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        // line 84
        if (array_key_exists("widget", $context)) {
            // line 85
            if (($context["required"] ?? $this->getContext($context, "required"))) {
                // line 86
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " required"))));
            }
            // line 88
            if (array_key_exists("parent_label_class", $context)) {
                // line 89
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter((((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " ") . ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class"))))));
            }
            // line 91
            if (( !(($context["label"] ?? $this->getContext($context, "label")) === false) && twig_test_empty(($context["label"] ?? $this->getContext($context, "label"))))) {
                // line 92
                if ( !twig_test_empty(($context["label_format"] ?? $this->getContext($context, "label_format")))) {
                    // line 93
                    $context["label"] = twig_replace_filter(($context["label_format"] ?? $this->getContext($context, "label_format")), array("%name%" =>                     // line 94
($context["name"] ?? $this->getContext($context, "name")), "%id%" =>                     // line 95
($context["id"] ?? $this->getContext($context, "id"))));
                } else {
                    // line 98
                    $context["label"] = $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->humanize(($context["name"] ?? $this->getContext($context, "name")));
                }
            }
            // line 101
            echo "<label";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["label_attr"] ?? $this->getContext($context, "label_attr")));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">";
            // line 102
            echo ($context["widget"] ?? $this->getContext($context, "widget"));
            echo " ";
            echo twig_escape_filter($this->env, (( !(($context["label"] ?? $this->getContext($context, "label")) === false)) ? ((((($context["translation_domain"] ?? $this->getContext($context, "translation_domain")) === false)) ? (($context["label"] ?? $this->getContext($context, "label"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(($context["label"] ?? $this->getContext($context, "label")), array(), ($context["translation_domain"] ?? $this->getContext($context, "translation_domain")))))) : ("")), "html", null, true);
            // line 103
            echo "</label>";
        }
        
        $__internal_79dfa3cc9928f38f2dc084a253fdcb5b126d75cbbe37a4ff9f150456babc2196->leave($__internal_79dfa3cc9928f38f2dc084a253fdcb5b126d75cbbe37a4ff9f150456babc2196_prof);

        
        $__internal_fa4e1de700b7260979fdb913e252534dba79af42c3e022ce0041f8479dd4f10b->leave($__internal_fa4e1de700b7260979fdb913e252534dba79af42c3e022ce0041f8479dd4f10b_prof);

    }

    // line 109
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_ee042b5269c281eda8a0f21adb1443b9e5d4dc0f1b65907b4e059a55d8655c2f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ee042b5269c281eda8a0f21adb1443b9e5d4dc0f1b65907b4e059a55d8655c2f->enter($__internal_ee042b5269c281eda8a0f21adb1443b9e5d4dc0f1b65907b4e059a55d8655c2f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_62ed96798facaae5fa973168fccae19510f87f708724a076985fdfe266fc68d7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_62ed96798facaae5fa973168fccae19510f87f708724a076985fdfe266fc68d7->enter($__internal_62ed96798facaae5fa973168fccae19510f87f708724a076985fdfe266fc68d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 110
        echo "<div class=\"form-group";
        if ((( !($context["compound"] ?? $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter(($context["force_error"] ?? $this->getContext($context, "force_error")), false)) : (false))) &&  !($context["valid"] ?? $this->getContext($context, "valid")))) {
            echo " has-error";
        }
        echo "\">";
        // line 111
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label');
        // line 112
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 113
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 114
        echo "</div>";
        
        $__internal_62ed96798facaae5fa973168fccae19510f87f708724a076985fdfe266fc68d7->leave($__internal_62ed96798facaae5fa973168fccae19510f87f708724a076985fdfe266fc68d7_prof);

        
        $__internal_ee042b5269c281eda8a0f21adb1443b9e5d4dc0f1b65907b4e059a55d8655c2f->leave($__internal_ee042b5269c281eda8a0f21adb1443b9e5d4dc0f1b65907b4e059a55d8655c2f_prof);

    }

    // line 117
    public function block_button_row($context, array $blocks = array())
    {
        $__internal_7c6c0702fbd19ee7aee74df6926f18ee0913e4cbbc8c00c77c224420edc9a285 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7c6c0702fbd19ee7aee74df6926f18ee0913e4cbbc8c00c77c224420edc9a285->enter($__internal_7c6c0702fbd19ee7aee74df6926f18ee0913e4cbbc8c00c77c224420edc9a285_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        $__internal_17db0afba168c3e040386dec7b12b175e4d08ce8d8e0f422a0aabfacf3182548 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_17db0afba168c3e040386dec7b12b175e4d08ce8d8e0f422a0aabfacf3182548->enter($__internal_17db0afba168c3e040386dec7b12b175e4d08ce8d8e0f422a0aabfacf3182548_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        // line 118
        echo "<div class=\"form-group\">";
        // line 119
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 120
        echo "</div>";
        
        $__internal_17db0afba168c3e040386dec7b12b175e4d08ce8d8e0f422a0aabfacf3182548->leave($__internal_17db0afba168c3e040386dec7b12b175e4d08ce8d8e0f422a0aabfacf3182548_prof);

        
        $__internal_7c6c0702fbd19ee7aee74df6926f18ee0913e4cbbc8c00c77c224420edc9a285->leave($__internal_7c6c0702fbd19ee7aee74df6926f18ee0913e4cbbc8c00c77c224420edc9a285_prof);

    }

    // line 123
    public function block_choice_row($context, array $blocks = array())
    {
        $__internal_d47ea4efaa2331f37d0642816894186a828938a397fddbe208a3077565e4788f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d47ea4efaa2331f37d0642816894186a828938a397fddbe208a3077565e4788f->enter($__internal_d47ea4efaa2331f37d0642816894186a828938a397fddbe208a3077565e4788f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_row"));

        $__internal_6666c5aeb15a7a69b9520d9dee5030c3c813b330408a0f663bdadc31407f345c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6666c5aeb15a7a69b9520d9dee5030c3c813b330408a0f663bdadc31407f345c->enter($__internal_6666c5aeb15a7a69b9520d9dee5030c3c813b330408a0f663bdadc31407f345c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_row"));

        // line 124
        $context["force_error"] = true;
        // line 125
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_6666c5aeb15a7a69b9520d9dee5030c3c813b330408a0f663bdadc31407f345c->leave($__internal_6666c5aeb15a7a69b9520d9dee5030c3c813b330408a0f663bdadc31407f345c_prof);

        
        $__internal_d47ea4efaa2331f37d0642816894186a828938a397fddbe208a3077565e4788f->leave($__internal_d47ea4efaa2331f37d0642816894186a828938a397fddbe208a3077565e4788f_prof);

    }

    // line 128
    public function block_date_row($context, array $blocks = array())
    {
        $__internal_70095c503154070629ebf795472e986e18b0171537d7859907706d85288db053 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_70095c503154070629ebf795472e986e18b0171537d7859907706d85288db053->enter($__internal_70095c503154070629ebf795472e986e18b0171537d7859907706d85288db053_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_row"));

        $__internal_22b094bfac868bd40232a6d30c858cf83125efdd3dee5c72647fc18d897d6151 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_22b094bfac868bd40232a6d30c858cf83125efdd3dee5c72647fc18d897d6151->enter($__internal_22b094bfac868bd40232a6d30c858cf83125efdd3dee5c72647fc18d897d6151_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_row"));

        // line 129
        $context["force_error"] = true;
        // line 130
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_22b094bfac868bd40232a6d30c858cf83125efdd3dee5c72647fc18d897d6151->leave($__internal_22b094bfac868bd40232a6d30c858cf83125efdd3dee5c72647fc18d897d6151_prof);

        
        $__internal_70095c503154070629ebf795472e986e18b0171537d7859907706d85288db053->leave($__internal_70095c503154070629ebf795472e986e18b0171537d7859907706d85288db053_prof);

    }

    // line 133
    public function block_time_row($context, array $blocks = array())
    {
        $__internal_18982b78ae0fe74486964ebe8829042fe90fa03bc6e70d98e149b7fb9b662f08 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_18982b78ae0fe74486964ebe8829042fe90fa03bc6e70d98e149b7fb9b662f08->enter($__internal_18982b78ae0fe74486964ebe8829042fe90fa03bc6e70d98e149b7fb9b662f08_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_row"));

        $__internal_1d56632214c583e27f3984c2fea00576fcdd77e8a08c09cd1afedce25389ba60 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1d56632214c583e27f3984c2fea00576fcdd77e8a08c09cd1afedce25389ba60->enter($__internal_1d56632214c583e27f3984c2fea00576fcdd77e8a08c09cd1afedce25389ba60_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_row"));

        // line 134
        $context["force_error"] = true;
        // line 135
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_1d56632214c583e27f3984c2fea00576fcdd77e8a08c09cd1afedce25389ba60->leave($__internal_1d56632214c583e27f3984c2fea00576fcdd77e8a08c09cd1afedce25389ba60_prof);

        
        $__internal_18982b78ae0fe74486964ebe8829042fe90fa03bc6e70d98e149b7fb9b662f08->leave($__internal_18982b78ae0fe74486964ebe8829042fe90fa03bc6e70d98e149b7fb9b662f08_prof);

    }

    // line 138
    public function block_datetime_row($context, array $blocks = array())
    {
        $__internal_1f1ac933f0c9eb46c3cd46998785008c558f46e99876d1553d1d1613d4cb1daa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1f1ac933f0c9eb46c3cd46998785008c558f46e99876d1553d1d1613d4cb1daa->enter($__internal_1f1ac933f0c9eb46c3cd46998785008c558f46e99876d1553d1d1613d4cb1daa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_row"));

        $__internal_dd3874665cb75346d0ddf6157b398e6d12bf50006a17cef4ef15e5beb444de5d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dd3874665cb75346d0ddf6157b398e6d12bf50006a17cef4ef15e5beb444de5d->enter($__internal_dd3874665cb75346d0ddf6157b398e6d12bf50006a17cef4ef15e5beb444de5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_row"));

        // line 139
        $context["force_error"] = true;
        // line 140
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_dd3874665cb75346d0ddf6157b398e6d12bf50006a17cef4ef15e5beb444de5d->leave($__internal_dd3874665cb75346d0ddf6157b398e6d12bf50006a17cef4ef15e5beb444de5d_prof);

        
        $__internal_1f1ac933f0c9eb46c3cd46998785008c558f46e99876d1553d1d1613d4cb1daa->leave($__internal_1f1ac933f0c9eb46c3cd46998785008c558f46e99876d1553d1d1613d4cb1daa_prof);

    }

    // line 143
    public function block_checkbox_row($context, array $blocks = array())
    {
        $__internal_0ef9d2708cc929eb9da2f6ccd2a974adcf95e235d64ae1535b4a84d5ef659efe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0ef9d2708cc929eb9da2f6ccd2a974adcf95e235d64ae1535b4a84d5ef659efe->enter($__internal_0ef9d2708cc929eb9da2f6ccd2a974adcf95e235d64ae1535b4a84d5ef659efe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        $__internal_4e0a3344b4aca53d6cc469b0bac6fd9baed5ad11fe234b11383360b65b089174 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4e0a3344b4aca53d6cc469b0bac6fd9baed5ad11fe234b11383360b65b089174->enter($__internal_4e0a3344b4aca53d6cc469b0bac6fd9baed5ad11fe234b11383360b65b089174_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        // line 144
        echo "<div class=\"form-group";
        if ( !($context["valid"] ?? $this->getContext($context, "valid"))) {
            echo " has-error";
        }
        echo "\">";
        // line 145
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 146
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 147
        echo "</div>";
        
        $__internal_4e0a3344b4aca53d6cc469b0bac6fd9baed5ad11fe234b11383360b65b089174->leave($__internal_4e0a3344b4aca53d6cc469b0bac6fd9baed5ad11fe234b11383360b65b089174_prof);

        
        $__internal_0ef9d2708cc929eb9da2f6ccd2a974adcf95e235d64ae1535b4a84d5ef659efe->leave($__internal_0ef9d2708cc929eb9da2f6ccd2a974adcf95e235d64ae1535b4a84d5ef659efe_prof);

    }

    // line 150
    public function block_radio_row($context, array $blocks = array())
    {
        $__internal_c8afd778e424f353e32f2f57c4838b3c7305e88ae83b49b78d99f94c2a40a3e6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c8afd778e424f353e32f2f57c4838b3c7305e88ae83b49b78d99f94c2a40a3e6->enter($__internal_c8afd778e424f353e32f2f57c4838b3c7305e88ae83b49b78d99f94c2a40a3e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        $__internal_4ae50aadaa27f290c4595b83bb04d6b4087a07c65387d2e8503ac35c386d57f7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4ae50aadaa27f290c4595b83bb04d6b4087a07c65387d2e8503ac35c386d57f7->enter($__internal_4ae50aadaa27f290c4595b83bb04d6b4087a07c65387d2e8503ac35c386d57f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        // line 151
        echo "<div class=\"form-group";
        if ( !($context["valid"] ?? $this->getContext($context, "valid"))) {
            echo " has-error";
        }
        echo "\">";
        // line 152
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 153
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 154
        echo "</div>";
        
        $__internal_4ae50aadaa27f290c4595b83bb04d6b4087a07c65387d2e8503ac35c386d57f7->leave($__internal_4ae50aadaa27f290c4595b83bb04d6b4087a07c65387d2e8503ac35c386d57f7_prof);

        
        $__internal_c8afd778e424f353e32f2f57c4838b3c7305e88ae83b49b78d99f94c2a40a3e6->leave($__internal_c8afd778e424f353e32f2f57c4838b3c7305e88ae83b49b78d99f94c2a40a3e6_prof);

    }

    // line 159
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_bd325484f69672eb305a220721484bbfe75fba4e1de86892c672972cf9a370a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bd325484f69672eb305a220721484bbfe75fba4e1de86892c672972cf9a370a5->enter($__internal_bd325484f69672eb305a220721484bbfe75fba4e1de86892c672972cf9a370a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        $__internal_9f27e71c4b936097744fda647c6615996d8792867b78849ffbd3bbc8191b0a94 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f27e71c4b936097744fda647c6615996d8792867b78849ffbd3bbc8191b0a94->enter($__internal_9f27e71c4b936097744fda647c6615996d8792867b78849ffbd3bbc8191b0a94_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 160
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 161
            if ( !Symfony\Bridge\Twig\Extension\twig_is_root_form(($context["form"] ?? $this->getContext($context, "form")))) {
                echo "<span class=\"help-block\">";
            } else {
                echo "<div class=\"alert alert-danger\">";
            }
            // line 162
            echo "    <ul class=\"list-unstyled\">";
            // line 163
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? $this->getContext($context, "errors")));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 164
                echo "<li><span class=\"glyphicon glyphicon-exclamation-sign\"></span> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "</li>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 166
            echo "</ul>
    ";
            // line 167
            if ( !Symfony\Bridge\Twig\Extension\twig_is_root_form(($context["form"] ?? $this->getContext($context, "form")))) {
                echo "</span>";
            } else {
                echo "</div>";
            }
        }
        
        $__internal_9f27e71c4b936097744fda647c6615996d8792867b78849ffbd3bbc8191b0a94->leave($__internal_9f27e71c4b936097744fda647c6615996d8792867b78849ffbd3bbc8191b0a94_prof);

        
        $__internal_bd325484f69672eb305a220721484bbfe75fba4e1de86892c672972cf9a370a5->leave($__internal_bd325484f69672eb305a220721484bbfe75fba4e1de86892c672972cf9a370a5_prof);

    }

    public function getTemplateName()
    {
        return "bootstrap_3_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  709 => 167,  706 => 166,  698 => 164,  694 => 163,  692 => 162,  686 => 161,  684 => 160,  675 => 159,  665 => 154,  663 => 153,  661 => 152,  655 => 151,  646 => 150,  636 => 147,  634 => 146,  632 => 145,  626 => 144,  617 => 143,  607 => 140,  605 => 139,  596 => 138,  586 => 135,  584 => 134,  575 => 133,  565 => 130,  563 => 129,  554 => 128,  544 => 125,  542 => 124,  533 => 123,  523 => 120,  521 => 119,  519 => 118,  510 => 117,  500 => 114,  498 => 113,  496 => 112,  494 => 111,  488 => 110,  479 => 109,  468 => 103,  464 => 102,  449 => 101,  445 => 98,  442 => 95,  441 => 94,  440 => 93,  438 => 92,  436 => 91,  433 => 89,  431 => 88,  428 => 86,  426 => 85,  424 => 84,  415 => 82,  405 => 79,  403 => 77,  394 => 76,  384 => 73,  382 => 71,  373 => 70,  363 => 67,  361 => 66,  352 => 64,  342 => 61,  340 => 60,  331 => 59,  320 => 53,  318 => 52,  316 => 51,  313 => 49,  311 => 48,  309 => 47,  300 => 46,  289 => 42,  287 => 41,  285 => 40,  282 => 38,  280 => 37,  278 => 36,  269 => 35,  258 => 31,  254 => 29,  248 => 27,  246 => 26,  244 => 25,  238 => 23,  236 => 22,  233 => 21,  230 => 20,  227 => 19,  225 => 18,  216 => 17,  206 => 14,  204 => 13,  195 => 12,  185 => 9,  182 => 7,  180 => 6,  171 => 5,  161 => 159,  158 => 158,  155 => 156,  153 => 150,  150 => 149,  148 => 143,  145 => 142,  143 => 138,  140 => 137,  138 => 133,  135 => 132,  133 => 128,  130 => 127,  128 => 123,  125 => 122,  123 => 117,  120 => 116,  118 => 109,  115 => 108,  112 => 106,  110 => 82,  107 => 81,  105 => 76,  102 => 75,  100 => 70,  97 => 69,  95 => 64,  92 => 63,  90 => 59,  87 => 58,  84 => 56,  82 => 46,  79 => 45,  77 => 35,  74 => 34,  72 => 17,  69 => 16,  67 => 12,  64 => 11,  62 => 5,  59 => 4,  56 => 2,  14 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"bootstrap_base_layout.html.twig\" %}

{# Widgets #}

{% block form_widget_simple -%}
    {% if type is not defined or type not in ['file', 'hidden'] %}
        {%- set attr = attr|merge({class: (attr.class|default('') ~ ' form-control')|trim}) -%}
    {% endif %}
    {{- parent() -}}
{%- endblock form_widget_simple %}

{% block button_widget -%}
    {%- set attr = attr|merge({class: (attr.class|default('btn-default') ~ ' btn')|trim}) -%}
    {{- parent() -}}
{%- endblock button_widget %}

{% block money_widget -%}
    {% set prepend = not (money_pattern starts with '{{') %}
    {% set append = not (money_pattern ends with '}}') %}
    {% if prepend or append %}
        <div class=\"input-group\">
            {% if prepend %}
                <span class=\"input-group-addon\">{{ money_pattern|form_encode_currency }}</span>
            {% endif %}
            {{- block('form_widget_simple') -}}
            {% if append %}
                <span class=\"input-group-addon\">{{ money_pattern|form_encode_currency }}</span>
            {% endif %}
        </div>
    {% else %}
        {{- block('form_widget_simple') -}}
    {% endif %}
{%- endblock money_widget %}

{% block checkbox_widget -%}
    {%- set parent_label_class = parent_label_class|default(label_attr.class|default('')) -%}
    {% if 'checkbox-inline' in parent_label_class %}
        {{- form_label(form, null, { widget: parent() }) -}}
    {% else -%}
        <div class=\"checkbox\">
            {{- form_label(form, null, { widget: parent() }) -}}
        </div>
    {%- endif -%}
{%- endblock checkbox_widget %}

{% block radio_widget -%}
    {%- set parent_label_class = parent_label_class|default(label_attr.class|default('')) -%}
    {%- if 'radio-inline' in parent_label_class -%}
        {{- form_label(form, null, { widget: parent() }) -}}
    {%- else -%}
        <div class=\"radio\">
            {{- form_label(form, null, { widget: parent() }) -}}
        </div>
    {%- endif -%}
{%- endblock radio_widget %}

{# Labels #}

{% block form_label -%}
    {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' control-label')|trim}) -%}
    {{- parent() -}}
{%- endblock form_label %}

{% block choice_label -%}
    {# remove the checkbox-inline and radio-inline class, it's only useful for embed labels #}
    {%- set label_attr = label_attr|merge({class: label_attr.class|default('')|replace({'checkbox-inline': '', 'radio-inline': ''})|trim}) -%}
    {{- block('form_label') -}}
{% endblock %}

{% block checkbox_label -%}
    {%- set label_attr = label_attr|merge({'for': id}) -%}

    {{- block('checkbox_radio_label') -}}
{%- endblock checkbox_label %}

{% block radio_label -%}
    {%- set label_attr = label_attr|merge({'for': id}) -%}

    {{- block('checkbox_radio_label') -}}
{%- endblock radio_label %}

{% block checkbox_radio_label -%}
    {# Do not display the label if widget is not defined in order to prevent double label rendering #}
    {%- if widget is defined -%}
        {%- if required -%}
            {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' required')|trim}) -%}
        {%- endif -%}
        {%- if parent_label_class is defined -%}
            {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' ' ~ parent_label_class)|trim}) -%}
        {%- endif -%}
        {%- if label is not same as(false) and label is empty -%}
            {%- if label_format is not empty -%}
                {%- set label = label_format|replace({
                    '%name%': name,
                    '%id%': id,
                }) -%}
            {%- else -%}
                {% set label = name|humanize %}
            {%- endif -%}
        {%- endif -%}
        <label{% for attrname, attrvalue in label_attr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}>
            {{- widget|raw }} {{ label is not same as(false) ? (translation_domain is same as(false) ? label : label|trans({}, translation_domain)) -}}
        </label>
    {%- endif -%}
{%- endblock checkbox_radio_label %}

{# Rows #}

{% block form_row -%}
    <div class=\"form-group{% if (not compound or force_error|default(false)) and not valid %} has-error{% endif %}\">
        {{- form_label(form) -}}
        {{- form_widget(form) -}}
        {{- form_errors(form) -}}
    </div>
{%- endblock form_row %}

{% block button_row -%}
    <div class=\"form-group\">
        {{- form_widget(form) -}}
    </div>
{%- endblock button_row %}

{% block choice_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock choice_row %}

{% block date_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock date_row %}

{% block time_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock time_row %}

{% block datetime_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock datetime_row %}

{% block checkbox_row -%}
    <div class=\"form-group{% if not valid %} has-error{% endif %}\">
        {{- form_widget(form) -}}
        {{- form_errors(form) -}}
    </div>
{%- endblock checkbox_row %}

{% block radio_row -%}
    <div class=\"form-group{% if not valid %} has-error{% endif %}\">
        {{- form_widget(form) -}}
        {{- form_errors(form) -}}
    </div>
{%- endblock radio_row %}

{# Errors #}

{% block form_errors -%}
    {% if errors|length > 0 -%}
    {% if form is not rootform %}<span class=\"help-block\">{% else %}<div class=\"alert alert-danger\">{% endif %}
    <ul class=\"list-unstyled\">
        {%- for error in errors -%}
            <li><span class=\"glyphicon glyphicon-exclamation-sign\"></span> {{ error.message }}</li>
        {%- endfor -%}
    </ul>
    {% if form is not rootform %}</span>{% else %}</div>{% endif %}
    {%- endif %}
{%- endblock form_errors %}
", "bootstrap_3_layout.html.twig", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bridge/Twig/Resources/views/Form/bootstrap_3_layout.html.twig");
    }
}
