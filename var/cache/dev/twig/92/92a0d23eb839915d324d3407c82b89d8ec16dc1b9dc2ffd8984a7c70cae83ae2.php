<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_83e1523de5d68690dbc25bca4c316bc48fb8d627a17464445f4064341076c15a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d0ee8e83ba966f750c7391a684c55d7702676924c271caefc7025f76a78f6ce3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d0ee8e83ba966f750c7391a684c55d7702676924c271caefc7025f76a78f6ce3->enter($__internal_d0ee8e83ba966f750c7391a684c55d7702676924c271caefc7025f76a78f6ce3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        $__internal_52804b465fdf0e009da593772ce24a0c15eb750634f5d2142d329ef760c523ad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_52804b465fdf0e009da593772ce24a0c15eb750634f5d2142d329ef760c523ad->enter($__internal_52804b465fdf0e009da593772ce24a0c15eb750634f5d2142d329ef760c523ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_d0ee8e83ba966f750c7391a684c55d7702676924c271caefc7025f76a78f6ce3->leave($__internal_d0ee8e83ba966f750c7391a684c55d7702676924c271caefc7025f76a78f6ce3_prof);

        
        $__internal_52804b465fdf0e009da593772ce24a0c15eb750634f5d2142d329ef760c523ad->leave($__internal_52804b465fdf0e009da593772ce24a0c15eb750634f5d2142d329ef760c523ad_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/checkbox_widget.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/checkbox_widget.html.php");
    }
}
