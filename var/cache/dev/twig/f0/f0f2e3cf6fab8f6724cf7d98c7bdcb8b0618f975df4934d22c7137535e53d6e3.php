<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_5e4ef152f60af0b077e6c1164200dcd2853c7dd552ad0c724aa65a6c8a74aabb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_66466bec0aec67db826d610416ebae07a7c4090f7b511e7df569386a771eeb68 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_66466bec0aec67db826d610416ebae07a7c4090f7b511e7df569386a771eeb68->enter($__internal_66466bec0aec67db826d610416ebae07a7c4090f7b511e7df569386a771eeb68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        $__internal_b6bc3a1070e456ed5144e3896038f41f5c6ff6456a62344f690cddebaf8dc36e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b6bc3a1070e456ed5144e3896038f41f5c6ff6456a62344f690cddebaf8dc36e->enter($__internal_b6bc3a1070e456ed5144e3896038f41f5c6ff6456a62344f690cddebaf8dc36e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_66466bec0aec67db826d610416ebae07a7c4090f7b511e7df569386a771eeb68->leave($__internal_66466bec0aec67db826d610416ebae07a7c4090f7b511e7df569386a771eeb68_prof);

        
        $__internal_b6bc3a1070e456ed5144e3896038f41f5c6ff6456a62344f690cddebaf8dc36e->leave($__internal_b6bc3a1070e456ed5144e3896038f41f5c6ff6456a62344f690cddebaf8dc36e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
", "@Framework/Form/form_enctype.html.php", "/home/timur/http/hw/hw64/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_enctype.html.php");
    }
}
